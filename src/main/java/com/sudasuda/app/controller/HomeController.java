package com.sudasuda.app.controller;


import com.google.api.client.auth.openidconnect.IdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.gson.Gson;
import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import com.sudasuda.app.domain.*;
import com.sudasuda.app.service.*;
import com.sudasuda.app.utils.CookieUtil;
import com.sudasuda.app.utils.ImageExtractor;
import com.sudasuda.app.vo.NameCountVO;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.view.RedirectView;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.net.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Handles requests for the application home page.
 */
@Controller
@Component("HomeController")
public class HomeController {

    private static final Logger logger = LoggerFactory
            .getLogger(HomeController.class);
    public static int timer = 3600;
    private LinkService linkService;
    public final static Map<String, String> chudachudaHash = new HashMap<String, String>();
    private static final String[] IP_HEADER_CANDIDATES = {
            "X-Forwarded-For",
            "Proxy-Client-IP",
            "WL-Proxy-Client-IP",
            "HTTP_X_FORWARDED_FOR",
            "HTTP_X_FORWARDED",
            "HTTP_X_CLUSTER_CLIENT_IP",
            "HTTP_CLIENT_IP",
            "HTTP_FORWARDED_FOR",
            "HTTP_FORWARDED",
            "HTTP_VIA",
            "REMOTE_ADDR" };
    // public final static HazelcastInstance hazelcastInstance = Hazelcast.newHazelcastInstance();

    /**
     * Simply selects the home view to render by returning its name.
     */
    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String home(
            @RequestParam(value = "name", required = false) String name,
            Locale locale, Model model) {
        logger.info("Welcome home! The client locale is {}.", locale);

        Date date = new Date();
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG,
                DateFormat.LONG, locale);

        String formattedDate = dateFormat.format(date);

        model.addAttribute("serverTime", formattedDate);

        return "home";
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public RedirectView welcome(
            @RequestParam(value = "name", required = false) String name,
            HttpServletRequest request, Model model) {

        // LinkService linkService = new LinkService();

        // Randomly chose a type of graph

        double randomNo = Math.random();
        randomNo = randomNo * 10;
        randomNo = randomNo % 5;

        // randomNo = randomNo * -1;

        request.getSession().setAttribute("stylefile", "style_" + ((int) randomNo) + ".css");

		/*int rNo = (int) randomNo;

		if (rNo == 0) rNo = -1;
		
		logger.info("Random No = " + (rNo));
		List<NameCountVO> counts = linkService.getCategoryLinkCount(rNo, -1, "");
		
		request.setAttribute("counts", counts);*/

        request.setAttribute("tab", "");
        request.setAttribute("page", "signin");
        request.setAttribute("next", (request.getParameter("next") != null ? request.getParameter("next") : request.getContextPath() + "/GetLinks?tab=new"));

        RedirectView rd = new RedirectView(request.getContextPath() + "/GetLinks?tab=new");

        return rd;

        // return "welcome";
    }

    @RequestMapping(value = "/SignIn", method = RequestMethod.GET)
    public String signIn(
            HttpServletRequest request,
            HttpServletResponse response,
            HttpSession session,
            @RequestParam(value = "page", required = false, defaultValue = "signin") String page) {


        double randomNo = Math.random();
        randomNo = randomNo * 10;
        randomNo = randomNo % 5;

        // randomNo = randomNo * -1;

        request.getSession().setAttribute("stylefile", "style_" + ((int) randomNo) + ".css");
        request.setAttribute("tab", "");

        request.setAttribute("page", page);
    //    session.setAttribute("isPopup",false);
        String email = CookieUtil.getCookieValue(request, "rememberMe");
        System.out.println("email is"+email);
        UserService userService = new UserService();
        User user = null;

        System.out.println("checkCookie: finding cookie");

        if (email != null) {
            String decodedEmail = new String(org.apache.commons.codec.binary.Base64.decodeBase64(email));
            logger.info("uuid=" + decodedEmail);

            user = userService.getUserByEmail(decodedEmail);
            System.out.println("cookie user"+user);

            if (user != null) {
                request.setAttribute("email",user.getEmail());
                String decodedPassword = new String(org.apache.commons.codec.binary.Base64.decodeBase64(user.getPassword()));


                request.setAttribute("password",decodedPassword);

            } else {
                CookieUtil.removeCookie(response, "rememberMe");
                request.setAttribute("email","");

                request.setAttribute("password","");

            }

        }
        else
        {
            System.out.println("Not Login *******ah");
            request.setAttribute("email","");

            request.setAttribute("password","");
        }

        return "signin-new";
    }

    @RequestMapping(value = "/AboutUS",method = RequestMethod.GET)
    public String aboutus(HttpServletRequest request,HttpServletResponse response){


        return "aboutus";

        }

     /*   @RequestMapping(value = "/RssFeed",method = RequestMethod.GET)
    public String feedRss(HttpServletRequest request,HttpServletResponse response){
        FeedService feedService=new FeedService();
        String timestamp=request.getParameter("t");
           StringBuffer feeds= feedService.getNewLinks(timestamp);

        request.setAttribute("feeds",feeds);
        return "feedRss";
    }*/

 /*   @RequestMapping(value = "/JSONFeed",method = RequestMethod.GET)
    public String feedRss(HttpServletRequest request,HttpServletResponse response) {

        FeedService feedService = new FeedService();
        String timestamp = request.getParameter("t");


        request.setAttribute("feeds", feedService.getNewJsonLinks(timestamp));
        return "feedRss";

    }
*/

    @RequestMapping(value = "/Reg", method = RequestMethod.GET)
    public String signUp(
            HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam(value = "page", required = false, defaultValue = "signup") String page) {
        request.setAttribute("tab", "");
        request.setAttribute("page", page);

        return "signup";
    }

    @RequestMapping(value="/SendNotification", method=RequestMethod.GET)
    public String sendNotification(HttpServletRequest request, HttpServletResponse response)
    {   // https://github.com/notnoop/java-apns
        ApnsService service = APNS.newService().withCert("/","").withSandboxDestination().build();
        String headline = request.getParameter("headline");
        if ( headline == null ) return "test";
        String payload = APNS.newPayload().alertBody(headline).build();
        service.push("token",payload);
        return "test";
    }

    @RequestMapping(value = "/SignIn", method = RequestMethod.POST)
    public String signIn(HttpServletRequest request,
                         HttpServletResponse response) throws IOException {



        UserService userService = new UserService();

        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String action = request.getParameter("action");
        String email = request.getParameter("email");
        System.out.println("rem000000"+request.getParameter("remember"));
        boolean remember = "on".equals(request.getParameter("remember"));

        logger.info("REMEMBER ME --->" + request.getParameter("remember"));

        boolean skip = false;
        request.setAttribute("tab", "");

        request.setAttribute("page", "signin");
        System.out.println("username"+username);
        System.out.println("email"+email);
        System.out.println("username"+action);

        if (username != null && username.trim().length() == 0) {
            System.out.println("user name not empty");
            skip = true;
        }
        System.out.println("skip"+skip);
        if (!skip && action != null && action.equalsIgnoreCase("login")) {
            // String emailreg =
            // "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";

            // Boolean isValidEmail = email.matches(emailreg);
            logger.info("action = login for username = " + email);

           // User user = userService.getLoginUser(email,"CC");
            User user = userService.getUserByEmail(email);
            if (userService.authenticate(email, encryptPassword(password))) {
                String encodeEmail= new String(java.util.Base64.getEncoder().encode(email.getBytes()));

                Cookie ck=new Cookie("cc",encodeEmail);//creating cookie object
                ck.setMaxAge(60*60*24*365);
                //   ck.setDomain("https://lightuptheweb.net");
                response.addCookie(ck);//adding cookie in the response
                request.getSession().setAttribute("userInfo", user);
                request.getSession().setMaxInactiveInterval(60 * 60);
                if (remember) {
                    System.out.println("remember is on..");
                    String uuid = UUID.randomUUID().toString();
                    userService.saveUUID(uuid, user.getUserName());
                    CookieUtil.addCookie(response, "rememberMe",
                            encodeEmail, 2592000);
                } else {
                    System.out.println("remember is off....");
                    userService.deleteUUID(user.getUserName());
                    CookieUtil.removeCookie(response, "rememberMe");
                }
                if (request.getParameter("next") == null || request.getParameter("next").trim().length() == 0)
                    response.sendRedirect(request.getContextPath()
                            + "/GetLinks?tab=new");
                else
                    response.sendRedirect(request.getParameter("next"));
            } else {
                request.getSession().removeAttribute("userInfo");
                CookieUtil.removeCookie(response, "rememberMe");
                request.setAttribute("email","");
                request.setAttribute("password","");
                request.setAttribute("error", "Invalid username or password.  Please retry.");

                request.setAttribute("page", "signin");
                return "signin-new";
            }
        } else if (!skip) {
            System.out.println("its SignIn page...");
            String emailreg = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
            String usernameRegExp = "^[a-zA-Z0-9_]*$";

           // Boolean isValidUserName = username.matches(usernameRegExp);
            Boolean isValidEmail = email.matches(emailreg);
            request.setAttribute("page", "register");
           System.out.println("length"+username.length());
            if (username.length()<6) {
                System.out.println("user name incorrect");
                request.setAttribute("email","");
                request.setAttribute("password","");
                request.setAttribute("error", "Username " + username + " is not valid");
                return "signin-new";
            }
            if (password.length()<6) {
                System.out.println("user name incorrect");
                request.setAttribute("email","");
                request.setAttribute("password","");
                request.setAttribute("error", "Password Length is Minimum 6 Charactor");
                return "signin-new";
            }


            if (!isValidEmail) {
                request.setAttribute("email","");
                request.setAttribute("password","");
                request.setAttribute("error", "Email: " + email + " is invalid");
                return "signin-new";
            }

            /*User user = userService.getUser(email);
            if (isValidUserName && user != null && user.getUserName() != null) {
                request.setAttribute("error", "Username " + username + " is already taken");
                request.setAttribute("page", "register");
                return "signin";

            }*/

            User user = userService.getUserByEmail(email);
            if (isValidEmail && user != null && user.getEmail() != null) {
                request.setAttribute("email","");
                request.setAttribute("password","");
                request.setAttribute("error", "Email address Alreday Exist");
                request.setAttribute("page", "register");
                return "signin-new";
            }

            if (user == null || user.getUserName() == null
                    || username.trim().length() != 0) {

                if (isValidEmail&&username!=""&&password!="") {
                    System.out.println("not empty....");
                    userService.addUser(username, email,
                            encryptPassword(password), "CC","");
                    String encodeEmail= new String(java.util.Base64.getEncoder().encode(email.getBytes()));

                    System.out.println("encodeEmail"+encodeEmail);
                    Cookie ck=new Cookie("cc",encodeEmail);//creating cookie object
                    ck.setMaxAge(60*60*24*365);
                    //   ck.setDomain("https://lightuptheweb.net");
                    response.addCookie(ck);//adding cookie in the response

                    request.getSession().setAttribute("userInfo",
                            userService.getUser(username));
                }
                if (request.getParameter("next") == null || request.getParameter("next").trim().length() == 0)
                    response.sendRedirect(request.getContextPath()
                            + "/GetLinks?tab=new");
                else
                    response.sendRedirect(request.getParameter("next"));
                // return "signin";
            } else if (!skip) {
                if (request.getAttribute("error") == null)
                    request.setAttribute("error", "Username " + username + " is already taken");

                return "signin-new";
            }
        }

        if (skip) {
            if (request.getParameter("next") == null)
                response.sendRedirect(request.getContextPath()
                        + "/GetLinks?tab=new");
            else
                response.sendRedirect(request.getParameter("next"));

        }

        return "getlinks";

    }

    @RequestMapping(value = "/SocialLogin", method = RequestMethod.GET)
    public @ResponseBody String socialLogin(HttpServletRequest request,
                                    HttpServletResponse response) throws IOException {

        UserService userService = new UserService();

        HttpSession session = request.getSession(true);

        String email = request.getParameter("email");
        String network = request.getParameter("network");
        String picture= request.getParameter("picture");
        String accessToken = request.getParameter("access_token");

        session.setAttribute("accessToken", accessToken);

        User user = userService.getUserByEmail(email);

        logger.info("access token ------->" + accessToken);
        logger.info("email  -------->" + email);
        logger.info("user --------->" + request.getAttribute("userName"));
        logger.info("user -------->" + user.getUserName());
        logger.info("email req------->" + user.getEmail());

        System.out.println("user size"+user.getUserId());
        if(user.getEmail()!=null) {
           User user2=userService.getLoginUser(email,network);
            if(user2.getEmail()!=null)
            {
                System.out.println("user size"+user.getUserId());
                System.out.println("credentials are correct");

                session.setAttribute("userInfo", user);
            }
            else
            {
                System.out.println("user exist");
                CookieUtil.removeCookie(response, "cc");
                request.setAttribute("error", " email is already taken");
                return "signin-new";
            }

        }
        else
        {
            System.out.println("its new user");
            userService.addUser(request.getParameter("userName"), request.getParameter("email"), "", network,picture);
            user = userService.getUserByEmail(email);
            session.setAttribute("userInfo", user);
        }
       /* if (user.getUserName() != null) {
            session.setAttribute("userInfo", user);
        } else {
            userService.addUser(request.getParameter("userName"), request.getParameter("email"), "", network,picture);
            user = userService.getUserByEmail(email);
            session.setAttribute("userInfo", user);
        }*/

        String next = request.getContextPath() + "/GetLinks?tab=current";

        if (request.getParameter("next") != null) next = request.getParameter("next");

        logger.info("next == " + next);



        return "success";
    }

    @RequestMapping(value="/Forgot",method = RequestMethod.GET)
    public String forgetPassword(HttpServletRequest request, HttpServletResponse response)
    {
        double randomNo = Math.random();
        randomNo = randomNo * 10;
        randomNo = randomNo % 5;

        // randomNo = randomNo * -1;

        request.getSession().setAttribute("stylefile", "style_" + ((int) randomNo) + ".css");

        return "forget-new";
    }
    @RequestMapping(value="/OneTapLogin",method = RequestMethod.POST)
    public @ResponseBody String oneTapLogin(HttpServletRequest request, HttpServletResponse response)
    {

        System.out.println("one tab="+request.getParameter("credential"));
        String data=request.getParameter("credential");
        String givenName="";
        String status="";
        Properties prop = new Properties();
        try {
            File file = ResourceUtils.getFile("classpath:application.properties");
            InputStream in = new FileInputStream(file);
            prop.load(in);
            System.out.println("api key"+prop.getProperty("client_id"));


            HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
            JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();

            GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(httpTransport, jsonFactory)
                    // Specify the CLIENT_ID of the app that accesses the backend:
                    .setAudience(Collections.singletonList(prop.getProperty("client_id")))
                    // Or, if multiple clients access the backend:
                    //.setAudience(Arrays.asList(CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3))
                    .build();

            // (Receive idTokenString by HTTPS POST)

            GoogleIdToken idToken = verifier.verify(data);
            System.out.println("idToken"+idToken);
            if (idToken != null) {
                IdToken.Payload payload = idToken.getPayload();

                // Print user identifier
                String userId = payload.getSubject();
                System.out.println("User ID: " + userId);

                // Get profile information from payload

                //   boolean emailVerified = Boolean.valueOf(payload.getEmailVerified());
                String name = (String) payload.get("name");
                String pictureUrl = (String) payload.get("picture");
                String email = (String) payload.get("email");
                String familyName = (String) payload.get("family_name");
                givenName = (String) payload.get("given_name");
                System.out.println("Name is"+name);
                System.out.println("picture is"+pictureUrl);
                System.out.println("given name is"+givenName);
                System.out.println("email-->"+email);

                String encodeEmail= new String(java.util.Base64.getEncoder().encode(email.getBytes()));

                System.out.println("encodeEmail"+encodeEmail);



                String userDeatil=socialLoginCredential(givenName,email,"GL",pictureUrl,request,response);
                System.out.println("user Detail-->"+userDeatil);
                status= userDeatil;
                if(status.equals("success"))
                {
                    Cookie ck=new Cookie("cc",encodeEmail);//creating cookie object
                    ck.setMaxAge(60*60*24*365);
                    response.addCookie(ck);
                }


            }
        }
        catch (Exception e)
        {

        }

        // Use or store profile information
        // ...



       return status;

    }

    public @ResponseBody String socialLoginCredential(String name,String email,String network,String picture,HttpServletRequest request,
                                           HttpServletResponse response) throws IOException {

        UserService userService = new UserService();

        HttpSession session = request.getSession(true);


        User user = userService.getUserByEmail(email);


        logger.info("email  -------->" + email);
        logger.info("user --------->" + request.getAttribute("userName"));
        logger.info("user -------->" + user.getUserName());
        logger.info("email req------->" + user.getEmail());

        System.out.println("user size"+user.getUserId());
        if(user.getEmail()!=null) {
            User user2=userService.getLoginUser(email,network);
            if(user2.getEmail()!=null)
            {
                System.out.println("user size"+user.getUserId());
                System.out.println("credentials are correct");

                session.setAttribute("userInfo", user);
            }
            else
            {
                CookieUtil.removeCookie(response, "cc");
                request.setAttribute("error", " email is already taken");
                return "signin-new";
            }

        }
        else
        {
            System.out.println("its new user");
            userService.addUser(name, email, "", network,picture);
            user = userService.getUserByEmail(email);
            session.setAttribute("userInfo", user);
        }
       /* if (user.getUserName() != null) {
            session.setAttribute("userInfo", user);
        } else {
            userService.addUser(request.getParameter("userName"), request.getParameter("email"), "", network,picture);
            user = userService.getUserByEmail(email);
            session.setAttribute("userInfo", user);
        }*/

        String next = request.getContextPath() + "/GetLinks?tab=current";

        if (request.getParameter("next") != null) next = request.getParameter("next");

        logger.info("next == " + next);



        return "success";
    }
    private String encryptPassword(String password) {
        String passwordToHash = password;
        String encodePassword= new String(java.util.Base64.getEncoder().encode(passwordToHash.getBytes()));

        String generatedPassword = null;
        try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            // Add password bytes to digest
            md.update(passwordToHash.getBytes());
            // Get the hash's bytes
            byte[] bytes = md.digest();
            // This bytes[] has bytes in decimal format;
            // Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16)
                        .substring(1));
            }
            // Get complete hashed password in hex format
            generatedPassword = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
       // return generatedPassword;
        return encodePassword;
    }
    @RequestMapping(value = "/error", method = RequestMethod.GET)
    public String error(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        System.out.println("Error");

        return "invalidUser";
    }
    @RequestMapping(value = "/SignOut", method = RequestMethod.GET)
    public String signOut(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        System.out.println("inside of logout");

        request.getSession().removeAttribute("userInfo");

        CookieUtil.removeCookie(response, "cc");
       // CookieUtil.removeCookie(response, "popup");
        Cookie ck=new Cookie("popup","closed");//creating cookie object
        ck.setMaxAge(60*60*24*365);
        //   ck.setDomain("https://lightuptheweb.net");
        response.addCookie(ck);//adding cookie in the response

           request.getSession().invalidate();


       // response.sendRedirect(request.getContextPath() + "/");
        String redirectUrl="http://www.garamgaram.news";
        return  "redirect:"+redirectUrl;
    }

    @RequestMapping(value = "/VoteUp", method = RequestMethod.GET)
    public void voteUp(
            HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam(value = "linkId", required = false) int linkId,
            @RequestParam(value = "tab", required = false, defaultValue = "current") String tab)
            throws IOException {

        if (request.getSession().getAttribute("userInfo") == null)
            response.sendRedirect(request.getContextPath() + "/GetLinks?tab=" + tab);

        User user = (User) request.getSession().getAttribute("userInfo");

        LinkService linkService = new LinkService();

        if (!linkService.isVotedAlready(user.getUserId(), linkId))
            linkService.voteUpLink(linkId, user.getUserId());
        //  response.sendRedirect(request.getContextPath() + "/GetLinks?tab=" + tab);
        response.sendRedirect((request.getParameter("commentpage") != null ? request.getContextPath() + "/AddComment?linkId=" + linkId : request.getContextPath() + "/GetLinks?tab=" + tab));
    }


    @RequestMapping(value = "/BookMark", method = RequestMethod.GET)
    public void bookMark(
            HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam(value = "linkId", required = false) int linkId,
            @RequestParam(value = "tab", required = false, defaultValue = "current") String tab)
            throws IOException {

        if (request.getSession().getAttribute("userInfo") == null)
            response.sendRedirect(request.getContextPath() + "/GetLinks?tab=" + tab);

        User user = (User) request.getSession().getAttribute("userInfo");

        LinkService linkService = new LinkService();

        if (!linkService.isBookmarkedAlready(user.getUserId(), linkId))

            linkService.addBookmark(linkId, user.getUserId());

        else

            linkService.removeBookmark(user.getUserId(), linkId);
        //  response.sendRedirect(request.getContextPath() + "/GetLinks?tab=" + tab);
        response.sendRedirect((request.getParameter("commentpage") != null ? request.getContextPath() + "/AddComment?linkId=" + linkId : request.getContextPath() + "/GetLinks?tab=" + tab));
    }


    @RequestMapping(value = "/CommentVoteUp", method = RequestMethod.GET)
    public void commentVoteUp(
            HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam(value = "commentId", required = true) int commentId,
            @RequestParam(value = "linkId", required = true) int linkId,
            @RequestParam(value = "tab", required = false, defaultValue = "current") String tab)
            throws IOException {
        User user = (User) request.getSession().getAttribute("userInfo");
        CommentService commentService = new CommentService();

        commentService.voteUpComment(commentId, user.getUserId());

        response.sendRedirect(request.getContextPath() + "/AddComment?linkId=" + linkId);
    }

    @RequestMapping(value = "/AddLink", method = RequestMethod.GET)
    public String addLink(HttpServletRequest request,
                          HttpServletResponse response) throws UnsupportedEncodingException {
        request.setCharacterEncoding("utf-8");
        request.setAttribute("tab", "new");
        return "addlink";
    }


    @RequestMapping(value = "/EditItem", method = RequestMethod.GET)
    public String editLink(HttpServletRequest request,
                           HttpServletResponse response,
                           @RequestParam(value = "linkId", required = true) int linkId) throws UnsupportedEncodingException {
        request.setCharacterEncoding("utf-8");
        LinkService linkService = new LinkService();
        Link link = linkService.getLink(linkId);
        request.setAttribute("link", link);

        request.setAttribute("tab", "edit");
        return "updatelink";
    }

    @RequestMapping(value = "/UpdateLink", method = RequestMethod.POST)
    public RedirectView updateLink(HttpServletRequest request,
                                   HttpServletResponse response,
                                   @RequestParam(value = "linkId", required = true) int linkId) throws UnsupportedEncodingException {

        LinkService linkService = new LinkService();
        request.setCharacterEncoding("UTF-8");


        Link link = new Link();
        link.setLinkId(linkId);
        String title = URLDecoder
                .decode(request.getParameter("title"), "UTF-8");

        link.setTitle(title);
        link.setUrl(request.getParameter("url"));
        link.setCategory(request.getParameter("category"));
        link.setLanguage(request.getParameter("language"));
        link.setCountry(request.getParameter("country"));
        link.setImageUrl(request.getParameter("image_url"));
        link.setDesc(request.getParameter("desc"));

        // TODO: tags
        linkService.updateLink(link);
        link = linkService.getLink(linkId);

        request.setAttribute("link", link);
        request.setAttribute("link", link);
        request.setAttribute("tab", "new");

        RedirectView view = new RedirectView(request.getContextPath()
                + "/GetLinks?tab=current");


        return view;

    }

    @RequestMapping(value = "/Spam", method = RequestMethod.GET)
    public RedirectView spam(HttpServletRequest request,
                             HttpServletResponse response, @RequestParam(value = "tab", required = false, defaultValue = "Current") String tab) {

        LinkService linkService = new LinkService();
        User user = (User) request.getSession().getAttribute("userInfo");
        String linkId = request.getParameter("linkId");

        logger.info("Link id=" + linkId + " being reported as spam by "
                + user.getUserName());

        if (user != null && linkId != null) {
            linkService.addSpam(Integer.valueOf(linkId), user);
        }

        RedirectView view = new RedirectView(request.getContextPath()
                + "/GetLinks?tab=" + tab);

        return view;
    }

    @RequestMapping(value = "/AddLink", method = RequestMethod.POST)
    public String processAddLink(HttpServletRequest request,
                                 HttpServletResponse response, HttpSession session)throws URISyntaxException,IOException {
        LinkService linkService = new LinkService();
        request.setCharacterEncoding("UTF-8");

        String url = request.getParameter("url");

        String title = URLDecoder
                .decode(request.getParameter("title"), "UTF-8");
        String media_type = "Text/HTML";
        String tags[] ={"News"};


        String lang = "Tamil";
        String country="India";
        logger.info("Title ->" + title);
        logger.info("URL -> " + url);
        logger.info("Tags ->" + tags);
        logger.info("desc ->" + request.getParameter("desc"));

        User user = (User) request.getSession().getAttribute("userInfo");
        boolean skip = false;

        if (user != null && url != null && url.trim().length() > 0
                && title != null && title.trim().length() > 0) {

            if (!linkService.isLinkPresent(url)) {
                linkService.addLink(url, title, "" + user.getUserId(),
                        lang,
                        request.getParameter("category"), country, media_type, tags, request.getParameter("image_url"), request.getParameter("desc"));
            } else {
                request.setAttribute("error", "\"" + title.trim() + "\" - is already submitted");
                skip = true;
                return "addLink";
            }

        }


        if (!skip)
            response.sendRedirect(request.getContextPath() + "/GetLinks?tab=New");

        return "addlink";
    }

    @RequestMapping(value = "/FollowUserManager", method = RequestMethod.GET)
    public String followUsers(HttpServletRequest request,
                              HttpServletResponse response) throws IOException {
        LinkService linkService = new LinkService();
        UserService userService = new UserService();
        FollowUserService followUserService = new FollowUserService();

        String op = request.getParameter("op");
        String followUserName = request.getParameter("submittedBy");
        List<Link> links = null;

        User user = (User) request.getSession().getAttribute("userInfo");
        request.setAttribute("tab", "");

        if (op != null && op.equalsIgnoreCase("add") && user != null) {
            followUserService.addFollowUser(user.getUserId(), followUserName);
        }

        if (op != null && op.equalsIgnoreCase("remove") && user != null) {
            followUserService.removeFollowUser(user.getUserId(), followUserName);
        }

        User submittedBy = userService.getUser(request.getParameter("submittedBy") + "");
        links = linkService.getLinksSubmittedBy((submittedBy != null ? submittedBy.getUserId() : 0));
        if (user != null) {
            request.setAttribute("isFollowAllowed", followUserService.isFollowAllowed(user.getUserId(), followUserName));
            request.setAttribute("followUsers", followUserService.getFollowUsers(user.getUserId()));
        }
        request.setAttribute("links", links);

        return "linksSubmittedBy";

    }

    @RequestMapping(value = "/FollowManager", method = RequestMethod.GET)
    public String followers(HttpServletRequest request,
                            HttpServletResponse response) throws IOException {
        LinkService linkService = new LinkService();
        FollowDomainService followDomainService = new FollowDomainService();
        UserService userService = new UserService();
        String op = request.getParameter("op");
        String domain = request.getParameter("domain");
        List<Link> links = null;
        List<FollowDomain> domains = null;
        User user = (User) request.getSession().getAttribute("userInfo");
        request.setAttribute("tab", "current");

        if (op != null && op.equalsIgnoreCase("add") && user != null) {
            followDomainService.addFollowDomain(domain, user.getUserId());
        }

        if (op != null && op.equalsIgnoreCase("remove") && user != null) {
            followDomainService.removeFollowDomain(domain, user.getUserId());
        }

        links = linkService.getDomainLinks(request.getParameter("domain"));
        if (user != null)
            domains = followDomainService.getFollowDomains(user.getUserId());

        request.setAttribute("topPosters", userService
                .getTopPostersForDomain(request.getParameter("domain")));

        request.setAttribute("domains", domains);
        request.setAttribute("links", links);
        if (user != null)
            request.setAttribute(
                    "isFollowAllowed",
                    followDomainService.isFollowAllowed(domain,
                            user.getUserId()));

        return "domainLinks";
    }

    @RequestMapping(value = "/AddComment", method = RequestMethod.GET)
    public String getComment(HttpServletRequest request,
                             HttpServletResponse response,
                             @RequestParam(value = "linkId", required = false) String linkId) {
        CommentService commentService = new CommentService();
        LinkService linkService = new LinkService();

        User user = (User) request.getSession().getAttribute("userInfo");

        if (request.getSession().getAttribute("stylefile") == null) {
            double randomNo = Math.random();
            randomNo = randomNo * 10;
            randomNo = randomNo % 5;
            request.getSession().setAttribute("stylefile", "style_" + ((int) randomNo) + ".css");
        }

        request.setAttribute("comments",
                commentService.getComments(Integer.valueOf(linkId), user));
        Link link = linkService.getLink(Integer.valueOf(linkId));

        if (user != null && user.getUserId() > 0)
            link.setVoted(linkService.isVotedAlready(user.getUserId(), Integer.valueOf(linkId)));
        else
            link.setVoted(false);

        logger.info("Link ID=" + linkId + ", isVoted:" + link.isVoted() + " Title:" + link.getTitle());


        request.setAttribute("text", "");
        request.setAttribute("linkId", linkId);
        request.setAttribute("link", link);

        return "comment";
    }

    @RequestMapping(value = "/Analytics", method = RequestMethod.GET)
    public String getAnalytics(HttpServletRequest request,
                               HttpServletResponse response) {

        LinkService linkService = new LinkService();

        User user = (User) request.getSession().getAttribute("userInfo");
        List<NameCountVO> categoryVOs = null;

        int type = 0;

        if (request.getParameter("type") != null) type = Integer.valueOf(request.getParameter("type"));

        if (user != null) {
            String cond = "";
            if (type == 3) cond = request.getParameter("source");
            categoryVOs = linkService.getCategoryLinkCount(user.getUserId(), type, cond);

        }

        if (type == 0) {
            request.setAttribute("header", " item likes by category");
            request.setAttribute("type", "category");
        }

        if (type == 1) {
            request.setAttribute("header", " item likes by source");
            request.setAttribute("type", "source");
        }

        if (type == 3) {
            request.setAttribute("header", " item likes by category for source " + request.getParameter("source"));
            request.setAttribute("type", "category");
        }

        request.setAttribute("text", "");
        request.setAttribute("counts", categoryVOs);

        return "analytics";
    }


    @RequestMapping(value = "/AddComment", method = RequestMethod.POST)
    public String addComment(HttpServletRequest request,
                             HttpServletResponse response,
                             @RequestParam(value = "linkId", required = false) String linkId) throws UnsupportedEncodingException {
        CommentService commentService = new CommentService();
        LinkService linkService = new LinkService();

        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        if (request.getSession().getAttribute("stylefile") == null) {
            double randomNo = Math.random();
            randomNo = randomNo * 10;
            randomNo = randomNo % 5;
            request.getSession().setAttribute("stylefile", "style_" + ((int) randomNo) + ".css");
        }

        User user = (User) request.getSession().getAttribute("userInfo");
        if (user != null && request.getParameter("comment") != null
                && request.getParameter("comment").trim().length() > 0) {
            commentService.addComment(user.getUserId(),
                    Integer.valueOf(linkId), request.getParameter("comment"));
        }

        Link link = linkService.getLink(Integer.valueOf(linkId));
        request.setAttribute("link", link);
        request.setAttribute("comments",
                commentService.getComments(Integer.valueOf(linkId), user));
        request.setAttribute("linkId", linkId);

        return "comment";
    }

    @RequestMapping(value = "/SetImageOnLinks", method = RequestMethod.GET)
    public void setImageOnLinks(HttpServletRequest request, HttpServletResponse response) {
        logger.info("SetImageOnLinks invoked");
        LinkService linkService = new LinkService();
        List<Link> links = null;

        links = linkService.getLinks(0, "All", "All", "All");

        links.addAll(linkService.getNewLinks(0, "All", "All", "All","0","30"));


        for (Link link : links) {

            //	if (link.getImageUrl() != null && !link.getImageUrl().equalsIgnoreCase("null") && link.getImageUrl().length() != 0) continue;

            if (link.getImageUrl() == null || link.getImageUrl().equalsIgnoreCase("null")) {
                link.setImageUrl("http://img.s-msn.com/tenant/amp/entityid/AA8mDjt.img?h=442&w=948&m=6&q=60&o=f&l=f&x=455&y=570");
                linkService.updateImageUrl(link.getLinkId(), link.getImageUrl());
                logger.info("Processing image for link Id:" + link.getLinkId() + ", imageUrl:" + link.getImageUrl());
                continue;
            }

            try {
                String imgUrl = ImageExtractor.extractImage(link.getUrl());

                if (imgUrl != null && !imgUrl.equalsIgnoreCase("null") && imgUrl.startsWith("http"))
                    link.setImageUrl(imgUrl);
                else
                    link.setImageUrl("http://img.s-msn.com/tenant/amp/entityid/AA8mDjt.img?h=442&w=948&m=6&q=60&o=f&l=f&x=455&y=570");

                linkService.updateImageUrl(link.getLinkId(), link.getImageUrl());

                logger.info("Processing image for link Id:" + link.getLinkId() + ", imageUrl:" + link.getImageUrl());

            } catch (Exception e) {

                e.printStackTrace();
            }

        }


    }

    @RequestMapping(value = "/Notify", method= RequestMethod.GET )
    public String notification(HttpServletRequest request) throws UnsupportedEncodingException {
          request.setCharacterEncoding("UTF-8");
          return "notify";

    }


        @RequestMapping(value = "/Notify", method= RequestMethod.POST )
    public String notify(HttpServletRequest request){

            try {
                request.setCharacterEncoding("UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            String password = request.getParameter("password");

        if ( password != null && password.equalsIgnoreCase("notify123"))
        {
            try {
                sendPushNotification(request);
            } catch (Exception e) {
                e.printStackTrace();
            }

            request.setAttribute("result", "Success!");
        }
        else
        {
            request.setAttribute("result", "Cannot Authenticate!");
        }


        return "notify";
    }

    @RequestMapping(value = "/ClearCache", method= RequestMethod.GET )
    public RedirectView clearCache(HttpServletRequest request)
    {
      //  HazelcastCacheService.getInstance().put("newStories", null);
        //HazelcastCacheService.getInstance().put("xmlFeed", null);
        logger.debug("newStories and xmlFeed - Cache Cleared");

        chudachudaHash.clear();
        System.out.println("dinakarsn"+chudachudaHash.get("dinakaran.com"));
        for(String key : chudachudaHash.keySet())
        {
            System.out.println(key);
        }

        RedirectView view = new RedirectView(request.getContextPath()
                + "/GetLinks?tab=new");
        return view;


    }

    public String createHttpClient_AcceptsUntrustedCerts(String deviceId, String title, String body) throws Exception {

        String res="";
        HttpClientBuilder b = HttpClientBuilder.create();

        // setup a Trust Strategy that allows all certificates.
        //
        SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(null, new TrustStrategy() {
            public boolean isTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
                return true;
            }
        }).build();
        b.setSslcontext(sslContext);

        // don't check Hostnames, either.
        //      -- use SSLConnectionSocketFactory.getDefaultHostnameVerifier(), if you don't want to weaken
        HostnameVerifier hostnameVerifier = SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;

        // here's the special part:
        //      -- need to create an SSL Socket Factory, to use our weakened "trust strategy";
        //      -- and create a Registry, to register it.
        //
        SSLConnectionSocketFactory sslSocketFactory = new SSLConnectionSocketFactory(sslContext, (X509HostnameVerifier) hostnameVerifier);
        Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                .register("http", PlainConnectionSocketFactory.getSocketFactory())
                .register("https", sslSocketFactory)
                .build();

        // now, we create connection-manager using our Registry.
        //      -- allows multi-threaded use
        PoolingHttpClientConnectionManager connMgr = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
        b.setConnectionManager(connMgr);

        // finally, build the HttpClient;
        //      -- done!
        CloseableHttpClient client =  b.build();

        HttpGet httpget = new HttpGet("https://fcm.googleapis.com/fcm/send");
        HttpPost httppost =new HttpPost("https://fcm.googleapis.com/fcm/send");
        httppost.addHeader("Authorization", "key=AIzaSyCRwAgzpVkK_l6VSNGVafyxMZRpS86xHBw");
        httppost.addHeader("Content-Type","application/json;charset=UTF-8");
        httppost.addHeader("charset", "UTF-8");
        //httppost.
        JSONObject json = new JSONObject();
        json.put("to",deviceId);
        json.put("priority","high");
        JSONObject info = new JSONObject();
        // http://stackoverflow.com/questions/22861828/java-string-getbytesutf-8-javascript-equivalent
        info.put("title", URLDecoder.decode(title,"UTF-8"));   // Notification title
        info.put("body", URLDecoder.decode(body,"UTF-8")); // Notification body
        json.put("notification", info);
        StringEntity entity = new StringEntity(json.toString(),"UTF-8");

        httppost.setEntity(entity);
        CloseableHttpResponse response = client.execute(httppost);

        res = "Device Token:<b>"+deviceId+"</b>, Response Status Code: <b>"+response.getStatusLine().getStatusCode()+"</b>,response->"+response.toString()+" <br />";

        return res;
    }

    @RequestMapping(value="/sendFCMNotification", method=RequestMethod.GET)
    public void sendPushNotification(HttpServletRequest request) throws Exception
    {
        NotificationService notificationService = new NotificationService();

        String userDeviceIdKey=request.getParameter("deviceid");
        request.setCharacterEncoding("UTF-8");

        String title = URLDecoder
                .decode(request.getParameter("title"), "UTF-8");

        String body = request.getParameter("body");

        // URL url = new URL(FMCurl);

        if (userDeviceIdKey != null && userDeviceIdKey.trim().length() > 0)
            request.setAttribute("output", createHttpClient_AcceptsUntrustedCerts(userDeviceIdKey, title, body));
        else
        {
            List<String> deviceIds = notificationService.getMobileDevices();

            StringBuilder outputList = new StringBuilder();

            for (String deviceId: deviceIds)
            {
                outputList.append(createHttpClient_AcceptsUntrustedCerts(deviceId, title, body));
            }

            request.setAttribute("output",outputList.toString());

        }

    }

    @RequestMapping(value = "/RegisterMobile", method=RequestMethod.GET)
    public void registerMobile(HttpServletRequest request, HttpServletResponse response,
                               @RequestParam(value="token", required = false) String token
                               )
    {
        NotificationService notificationService = new NotificationService();

        if (token!=null)
        {
            notificationService.registerDevice(token);
            System.out.println("Mobile token="+token);

        }
    }

    @RequestMapping(value = "/GetLinks", method = RequestMethod.GET)
    public String getLinks(
            HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam(value = "tab", required = false, defaultValue = "Current") String tab,
            @RequestParam(value = "lang", required = false) String lang,
            @RequestParam(value = "category", required = false) String category,
            @RequestParam(value = "country", required = false) String country,
            HttpSession session)throws FileNotFoundException,IOException {
        logger.info("GetLinks Invoked");

        UserService userService = new UserService();
        LinkService linkService = new LinkService();
        FollowDomainService followDomainService = new FollowDomainService();
        FollowUserService followUserService = new FollowUserService();
        List<Link> tag = null;
        List<Link> links = null;
        List<Link> domainList = new ArrayList<Link>();
        List<Link> newList = new ArrayList<Link>();

        List<Link> joinedLink=new ArrayList<Link>();
        List<FollowDomain> domains = null;
        List<NameCountVO> tags ;
        User user = null;

        // Just in case if someone tries to crash the page

        if (tab != null && !(tab.equalsIgnoreCase("Current")
                || tab.equalsIgnoreCase("New")
                || tab.equalsIgnoreCase("Expired")
                || tab.equalsIgnoreCase("MyLinks")
                || tab.equalsIgnoreCase("trending")
                || tab.equalsIgnoreCase("Recommendation")
                || tab.equalsIgnoreCase("MyBookmarks")))
            tab = "Current";

        // request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        request.setAttribute("tab", tab);

        if (lang != null)
            session.setAttribute("language", lang);
        if (category != null)
            session.setAttribute("category", category);
        if (country != null)
            session.setAttribute("country", country);

        if (lang == null && session.getAttribute("language") == null)
            session.setAttribute("language", "All");
        if (category == null && session.getAttribute("category") == null)
            session.setAttribute("category", "All");
        if (country == null && session.getAttribute("country") == null)
            session.setAttribute("country", "All");

        lang = session.getAttribute("language").toString();
        category = session.getAttribute("category").toString();
        country = session.getAttribute("country").toString();
        // session.setAttribute("language", lang);
        // session.setAttribute("category", category);

        if (session.getAttribute("userInfo") == null) {
            System.out.println("session is empty");
            checkCookie2(request, response);
           // setCookieForPopup(request,response);

        }
        String popup = CookieUtil.getCookieValue(request, "popup");
        System.out.println("popup cookie--->"+popup);
        if (session.getAttribute("isPopup") == null) {
            System.out.println("ispopup open first time");
            if (popup != null) {
                if (popup.equals("opened")) {
                    session.setAttribute("isPopup", false);
                } else {
                    session.setAttribute("isPopup", true);
                }
            }
            else
            {
                session.setAttribute("isPopup", true);
            }


        }
        else
        {
            System.out.println("is popup closed");
            if (popup != null) {
                if (popup.equals("opened")) {
                    session.setAttribute("isPopup", false);
                    Cookie ck=new Cookie("popup","opened");//creating cookie object
                    ck.setMaxAge(60*60*24*365);
                    //   ck.setDomain("https://lightuptheweb.net");
                    response.addCookie(ck);//adding cookie in the response
                    session.setAttribute("cookieValue","opened");
                } else {
                    session.setAttribute("isPopup", true);
                }
            }

        }
        System.out.println("test------>"+session.getAttribute("isPopup"));
		
		/*List<String> recentSubmitters = linkService.getRecentSubmitters();
		StringBuilder submittersStr = new StringBuilder();

		for(String submitter : recentSubmitters) {
			if ( submittersStr.length() > 0)
			submittersStr.append(", ");
			submittersStr.append(submitter);
		}

		request.setAttribute("recentSubmitters", "Recent Submitters: "+submittersStr);*/

        System.out.println("0----->"+session.getAttribute("userInfo"));
        session.removeAttribute("domainCategory");
        session.setAttribute("save","All" );
        //	session.setAttribute(SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME, new Locale("ta"));
        session.setAttribute("category", "All");
        session.setAttribute("domain", "All");
        if (session.getAttribute("userInfo") != null) {
            user = (User) request.getSession().getAttribute("userInfo");


        }

        if (request.getParameter("domain") != null) {
            domains = followDomainService.getFollowDomains(user.getUserId());
        }

        if (request.getParameter("submittedBy") == null
                && request.getParameter("domain") == null) {

            if (tab.equalsIgnoreCase("current")) {


                links = linkService.getLinks((user != null ? user.getUserId()
                        : 0), lang, category, country);


            }

            if (tab.equalsIgnoreCase("trending")) {



                //links= (List<Link>) HazelcastCacheService.getInstance().get("newStories");
                if ( links == null )
                {

// this getting links for articles

                    links = linkService.getTrendingLinks(
                            (user != null ? user.getUserId() : 0), lang, category, country);

                    joinedLink=links;
                    request.setAttribute("count",links.size());
                    // HazelcastCacheService.getInstance().put("newStories",links);
                }

            }

            Properties prop = new Properties();

            if (tab.equalsIgnoreCase("new")) {



                //links= (List<Link>) HazelcastCacheService.getInstance().get("newStories");
                if ( links == null ) {

                    System.out.println("landing");
                    //request.setAttribute("popup",true);
                    File file = ResourceUtils.getFile("classpath:application.properties");
                    InputStream in = new FileInputStream(file);
                    prop.load(in);
                    System.out.println("servername"+prop.getProperty("JsonLimit"));


                    String endLimit=prop.getProperty("JsonLimit");
                    Integer endValue=Integer.parseInt(endLimit);
                    String startLimit="0";

                  //  links = linkService.getNewLinks(
                      //      (user != null ? user.getUserId() : 0), lang, category, country,startLimit,endLimit);
                    // HazelcastCacheService.getInstance().put("newStories",links);

                    links=linkService.getFrontPageNews((user != null ? user.getUserId() : 0),startLimit,endLimit);
                    System.out.println("size" + links.size());
                     if(links.size()>0) {

                        System.out.println("end value"+endValue);
                         for (int i = 0; i < endValue; i++) {


                                if ((links.get(i).getDomain2().contains("tamil.thehindu.com") && links.get(i).getCategory().contains("இந்தியா")) || (links.get(i).getDomain2().contains("Dinakaran.com") && links.get(i).getCategory().contains("இந்தியா")) ||
                                        (links.get(i).getDomain2().contains("tamil.thehindu.com") && links.get(i).getCategory().contains("தமிழகம்")) || (links.get(i).getDomain2().contains("dinamani") && links.get(i).getCategory().contains("இந்தியா"))
                                        || (links.get(i).getDomain2().contains("vikatan") && links.get(i).getCategory().contains("இந்தியா")) || (links.get(i).getDomain2().contains("vikatan") && links.get(i).getCategory().contains("தமிழகம்"))
                                        || (links.get(i).getDomain2().contains("Dinakaran.com") && links.get(i).getCategory().contains("உலகம்")) || (links.get(i).getDomain2().contains("tamil.oneindia.com") && links.get(i).getCategory().contains("இந்தியா"))
                                         || (links.get(i).getDomain2().contains("cinema.dinakaran.com") && links.get(i).getCategory().contains("சினிமா"))|| (links.get(i).getDomain2().contains("filmibeat") && links.get(i).getCategory().contains("சினிமா"))) {
                                       domainList.add(links.get(i));

                                } else {

                                    newList.add(links.get(i));
                                }
                            }

                        }


                    joinedLink.addAll(domainList);
                    joinedLink.addAll(newList);

                    System.out.println("joined Link---->" + joinedLink.size());
                   // session.setAttribute("isPopup",true);


                }

            }


        }

        //request.setAttribute("links", links);
        request.setAttribute("links", joinedLink);
        session.setAttribute("moreLink",links);
     //   session.setAttribute("isPopup",true);

        if (request.getSession().getAttribute("stylefile") == null) {
            double randomNo = Math.random();
            randomNo = randomNo * 10;
            randomNo = randomNo % 5;
            request.getSession().setAttribute("stylefile", "style_" + ((int) randomNo) + ".css");
        }

        if (request.getParameter("submittedBy") == null
                && request.getParameter("domain") == null) {

            return "getlinks";
        }

        return "getlinks";
    }


    @RequestMapping(value = "/DomainLinks", method = RequestMethod.GET)
    public String getDomainLinks(
            HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam(value = "tab", required = false, defaultValue = "Current") String tab,
            @RequestParam(value = "lang", required = false) String lang,
            @RequestParam(value = "category", required = false) String category,
            @RequestParam(value = "country", required = false) String country,
            HttpSession session) {
        logger.info("DomainLinks Invoked");

        logger.info("URI: " + request.getRequestURI() + ", URL:" + request.getRequestURL() + "?" + request.getQueryString());

        UserService userService = new UserService();
        LinkService linkService = new LinkService();
        FollowDomainService followDomainService = new FollowDomainService();
        FollowUserService followUserService = new FollowUserService();

        List<Link> links = null;
        List<FollowDomain> domains = null;
        List<NameCountVO> tags = null;
        User user = null;

        if (request.getSession().getAttribute("stylefile") == null) {
            double randomNo = Math.random();
            randomNo = randomNo * 10;
            randomNo = randomNo % 5;
            request.getSession().setAttribute("stylefile", "style_" + ((int) randomNo) + ".css");
        }

        // Just in case if someone tries to crash the page

        if (tab != null && !(tab.equalsIgnoreCase("Current")
                || tab.equalsIgnoreCase("New")
                || tab.equalsIgnoreCase("Expired")
                || tab.equalsIgnoreCase("MyLinks")
                || tab.equalsIgnoreCase("MyBookmarks")))
            tab = "Current";

        // request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        request.setAttribute("tab", tab);

        if (lang != null)
            session.setAttribute("language", lang);
        if (category != null)
            session.setAttribute("category", category);
        if (country != null)
            session.setAttribute("country", country);

        if (lang == null && session.getAttribute("language") == null)
            session.setAttribute("language", "All");
        if (category == null && session.getAttribute("category") == null)
            session.setAttribute("category", "All");
        if (country == null && session.getAttribute("country") == null)
            session.setAttribute("country", "All");

        lang = session.getAttribute("language").toString();
        category = session.getAttribute("category").toString();
        country = session.getAttribute("country").toString();
        // session.setAttribute("language", lang);
        // session.setAttribute("category", category);

        if (session.getAttribute("userInfo") == null) {
            checkCookie2(request, response);
        }

		/*List<String> recentSubmitters = linkService.getRecentSubmitters();
		StringBuilder submittersStr = new StringBuilder();

		for(String submitter : recentSubmitters) {
			if ( submittersStr.length() > 0)
			submittersStr.append(", ");
			submittersStr.append(submitter);
		}

		request.setAttribute("recentSubmitters", "Recent Submitters: "+submittersStr);*/


        //	session.setAttribute(SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME, new Locale("ta"));

        if (session.getAttribute("userInfo") != null) {
            user = (User) request.getSession().getAttribute("userInfo");
        }

        if (request.getParameter("domain") != null && user != null) {
            domains = followDomainService.getFollowDomains(user.getUserId());
        }
        String startLimit="0";
        if (request.getParameter("submittedBy") == null
                && request.getParameter("domain") == null) {

            if (tab.equalsIgnoreCase("current"))
                links = linkService.getLinks((user != null ? user.getUserId()
                        : 0), lang, category, country);
            if (tab.equalsIgnoreCase("new"))
                links = linkService.getNewLinks(
                        (user != null ? user.getUserId() : 0), lang, category, country,startLimit,"30");
            if (tab.equalsIgnoreCase("expired"))
                links = linkService.getExpiredLinks(
                        (user != null ? user.getUserId() : 0), lang, category, country);
            if (tab.equalsIgnoreCase("mylinks") && user != null) {
                links = linkService.getMyLinks(user.getUserId(), lang, category, country);
                tags = linkService.getMyTags(user.getUserId());
                request.setAttribute("tags", tags);
            }
            if (tab.equalsIgnoreCase("mybookmarks") && user != null) {
                links = linkService.getMyBookmarks(user.getUserId(), lang, category, country);
            }
        } else if (request.getParameter("submittedBy") != null) {
            User submittedBy = userService.getUser(request
                    .getParameter("submittedBy") + "");
            links = linkService
                    .getLinksSubmittedBy((submittedBy != null ? submittedBy
                            .getUserId() : 0));
            request.setAttribute(
                    "isFollowAllowed",
                    followUserService.isFollowAllowed(
                            (user != null ? user.getUserId() : 0),
                            submittedBy.getUserName()));
            request.setAttribute("followUsers", followUserService
                    .getFollowUsers((user != null ? user.getUserId() : 0)));
            request.setAttribute("tab", "");
        }

        if (request.getParameter("domain") != null) {
            links = linkService.getDomainLinks(request.getParameter("domain"));
            if (user != null)
                request.setAttribute(
                        "isFollowAllowed",
                        followDomainService.isFollowAllowed(
                                request.getParameter("domain"),
                                user.getUserId()));
            request.setAttribute("topPosters", userService
                    .getTopPostersForDomain(request.getParameter("domain")));
            request.setAttribute("tab", "");
        }

        request.setAttribute("domains", domains);
        request.setAttribute("links", links);


        if (request.getParameter("submittedBy") == null
                && request.getParameter("domain") == null) {

            return "getlinks";
        }
        if (request.getParameter("submittedBy") != null) {
            return "linksSubmittedBy";
        }
        if (request.getParameter("domain") != null) {
            return "domainLinks";
        }

        return "getlinks";
    }

    private void checkCookie(HttpServletRequest req, HttpServletResponse res) {
        String uuid = CookieUtil.getCookieValue(req, CookieUtil.COOKIE_NAME);
        logger.info("uuid=" + uuid);

        UserService userService = new UserService();
        User user = null;

        logger.info("checkCookie: finding cookie");

        if (uuid != null) {
            user = userService.findUUID(uuid);

            if (user != null) {
                req.getSession().setAttribute("userInfo", user);
                CookieUtil
                        .addCookie(res, CookieUtil.COOKIE_NAME, uuid, 2592000);
            } else {
                CookieUtil.removeCookie(res, CookieUtil.COOKIE_NAME);
            }

        }
    }
    private boolean popUpCheck(String value) {
        if(value.equals("new"))
        {
            return true;
        }
        else
        {
            return false;
        }

    }
    private void checkCookie2(HttpServletRequest req, HttpServletResponse res) {
        System.out.println("inside check cookie");
        String email = CookieUtil.getCookieValue(req, "cc");
        System.out.println("email is"+email);
         UserService userService = new UserService();
        User user = null;

        System.out.println("checkCookie: finding cookie");

        if (email != null) {
            String decodedEmail = new String(org.apache.commons.codec.binary.Base64.decodeBase64(email));
            logger.info("uuid=" + decodedEmail);

            user = userService.getUserByEmail(decodedEmail);
            System.out.println("cookie user"+user);

            if (user != null) {
                req.getSession().setAttribute("userInfo", user);
                req.getSession().setAttribute("isPopup", false);

            } else {
                CookieUtil.removeCookie(res, "cc");
            }

        }
        else
        {
            System.out.println("Not Login");

          /*  Cookie ck=new Cookie("showPopup","true");//creating cookie object
            ck.setMaxAge(60*60*24*365);
            //   ck.setDomain("https://lightuptheweb.net");
            res.addCookie(ck);//adding cookie in the response

            String popUp = CookieUtil.getCookieValue(req, "showPopup");
            System.out.println("popup vaalue cookie"+popUp);
            if ( req.getSession().getAttribute("isPopup") == null) {
                System.out.println("ispopup open first time");

                req.getSession().setAttribute("isPopup", true);
            }
            else
            {
                System.out.println("ispopup ");

                req.getSession().setAttribute("isPopup",false);
            }*/

        }
    }
    private void setCookieForPopup(HttpServletRequest req, HttpServletResponse res) {
        System.out.println("inside check cookie");

        Cookie ck=new Cookie("popUp","true");//creating cookie object
        ck.setMaxAge(60*60*24*365);
        //   ck.setDomain("https://lightuptheweb.net");
        res.addCookie(ck);//adding cookie in the response

       /* String popUp = CookieUtil.getCookieValue(req, "popUp");
        System.out.println("email is"+popUp);
        UserService userService = new UserService();
        User user = null;

        System.out.println("checkCookie: finding cookie");

        if (popUp != null) {


            req.getSession().setAttribute("popUp", true);
        }
        else
        {
            System.out.println("Not Login");
        }*/
    }

    @RequestMapping(value = "/GetRssFeed ")
    public RedirectView getRssFeed(HttpServletRequest request, HttpServletResponse response) {
        RssFeedService rssFeedService = new RssFeedService();
        try {
            //rssFeedService.addRssFeed();
            //request.setAttribute("timer", timer);
        } catch (Exception e) {
            e.printStackTrace();
        }
        RedirectView view = new RedirectView(request.getContextPath()
                + "/GetLinks?tab=new");
        return view;
    }



    @RequestMapping(value = "/GetCategoryNews",method = RequestMethod.GET)
    public String getCategoryNews(HttpServletRequest request, HttpServletResponse response) {

        String category=request.getParameter("domain");

        System.out.println("domain"+category);


        LinkService linkService = new LinkService();

        boolean isEmpty=chudachudaHash.isEmpty();


          if (isEmpty) {

              //linkService.getCategoryNewsForMobile(category);
              chudachudaHash.put(category, linkService.getCategoryNewsForMobile(category));
              System.out.println("chuda" + chudachudaHash.get(category));
              request.setAttribute("categoryfeeds", chudachudaHash.get(category));
          } else {


              String value = chudachudaHash.get(category);
              if (value != null) {
                  System.out.println("category is not empty");
                  request.setAttribute("categoryfeeds", chudachudaHash.get(category));

              } else {
                  System.out.println("category is empty");
                  chudachudaHash.put(category, linkService.getCategoryNewsForMobile(category));

                  request.setAttribute("categoryfeeds", chudachudaHash.get(category));
              }


          }

        return "categoryRss";

    }
    @RequestMapping(value = "/GetCategoryNewsForWeb",method = RequestMethod.GET)
    public String getCategory(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws IOException,FileNotFoundException{



        double randomNo = Math.random();
        randomNo = randomNo * 10;
        randomNo = randomNo % 5;

        // randomNo = randomNo * -1;

        User user=null;
        if (session.getAttribute("userInfo") == null) {
            //checkCookie(request, response);
            checkCookie2(request, response);
        }
        if (session.getAttribute("userInfo") != null) {
            user = (User) request.getSession().getAttribute("userInfo");
        }
        request.getSession().setAttribute("stylefile", "style_" + ((int) randomNo) + ".css");


        String timestamp = request.getParameter("t");

        String category=request.getParameter("category");

        request.setAttribute("category",category);

        session.setAttribute("category", category);
        session.setAttribute("domain", "All");
        session.setAttribute("domainCategory","suc");
        session.setAttribute("save","All" );

       // String category="tamil.samayam.com";

        LinkService linkService = new LinkService();
        List<Link>links;
        Properties prop = new Properties();
        File file = ResourceUtils.getFile("classpath:application.properties");
        InputStream in = new FileInputStream(file);
        prop.load(in);
        System.out.println("servername"+prop.getProperty("JsonLimit"));


        String endLimit=prop.getProperty("JsonLimit");
        Integer endValue=Integer.parseInt(endLimit);
        String startLimit="0";
        links=linkService.getCategoryNewsForWeb(category,user != null ? user.getUserId() : 0,startLimit,endLimit);
        session.removeAttribute("moreLink");

        session.setAttribute("moreLink",links);

        request.setAttribute("links", links);

        return "getlinks";
    }
    @RequestMapping(value = "/GetNews",method = RequestMethod.GET)
    public String getNews(HttpServletRequest request, HttpServletResponse response,HttpSession session)throws IOException,FileNotFoundException {

        System.out.println("inside of categfory page");

        double randomNo = Math.random();
        randomNo = randomNo * 10;
        randomNo = randomNo % 5;

        // randomNo = randomNo * -1;


        User user=null;
        if (session.getAttribute("userInfo") == null) {
            checkCookie2(request, response);
        }
        if (session.getAttribute("userInfo") != null) {
               user = (User) request.getSession().getAttribute("userInfo");
        }
        request.getSession().setAttribute("stylefile", "style_" + ((int) randomNo) + ".css");


        String timestamp = request.getParameter("t");

        String category=request.getParameter("category");
        System.out.println("--->"+category);

        session.setAttribute("domain",category);
        session.setAttribute("category", "All");
        session.removeAttribute("domainCategory");
        session.setAttribute("save","All" );
        request.setAttribute("category",category);

        if(category.equals("india")){
            category="इंडिया";
        }if(category.equals("world")){category="दुनिया";}
        if(category.equals("sports")){category="खेल";}
        if(category.equals("general")){category="सामान्य समाचार";}
        if(category.equals("cinema")){category="मनोरंजन";}


        System.out.println("categoty-->"+category);
        // String category="tamil.samayam.com";

        LinkService linkService = new LinkService();
        List<Link>links;
        Properties prop = new Properties();
        File file = ResourceUtils.getFile("classpath:application.properties");
        InputStream in = new FileInputStream(file);
        prop.load(in);
        System.out.println("servername"+prop.getProperty("JsonLimit"));


        String endLimit=prop.getProperty("JsonLimit");

        String startLimit="0";

        links=linkService.getNews(category,user != null ? user.getUserId() : 0,startLimit,endLimit);
        session.removeAttribute("moreLink");

        request.setAttribute("links", links);
        session.setAttribute("moreLink",links);

        return "getlinks";
    }
    //get news based on domain and category
    @RequestMapping(value = "/GetCategoryForNews",method = RequestMethod.GET)
    public String getCategoryForNews(HttpServletRequest request, HttpServletResponse response,HttpSession session)throws IOException,FileNotFoundException {



        double randomNo = Math.random();
        randomNo = randomNo * 10;
        randomNo = randomNo % 5;

        // randomNo = randomNo * -1;

        User user=null;
        if (session.getAttribute("userInfo") == null) {
            checkCookie2(request, response);
        }
        if (session.getAttribute("userInfo") != null) {
              user = (User) request.getSession().getAttribute("userInfo");
        }
        request.getSession().setAttribute("stylefile", "style_" + ((int) randomNo) + ".css");


        String timestamp = request.getParameter("t");

        String category=request.getParameter("category");
        session.setAttribute("domain", category);
        String domain=request.getParameter("domain");
        session.setAttribute("category", domain);
        session.setAttribute("save","All" );

        request.setAttribute("category",category);
        request.setAttribute("domain",domain);

        System.out.println("category"+category);
        System.out.println("domain"+domain);


        if(category.equals("india")){
            category="इंडिया";
        }if(category.equals("world")){category="दुनिया";}
        if(category.equals("sports")){category="खेल";}
        if(category.equals("general")){category="सामान्य समाचार";}
        if(category.equals("cinema")){category="मनोरंजन";}





        // String category="tamil.samayam.com";

        LinkService linkService = new LinkService();
        List<Link>links;
        Properties prop = new Properties();
        File file = ResourceUtils.getFile("classpath:application.properties");
        InputStream in = new FileInputStream(file);
        prop.load(in);
        System.out.println("servername"+prop.getProperty("JsonLimit"));


        String endLimit=prop.getProperty("JsonLimit");

        String startLimit="0";
        links=linkService.getCategoryForNews(category,domain,user != null ? user.getUserId() : 0,startLimit,endLimit);

        session.removeAttribute("moreLink");

        session.setAttribute("moreLink",links);

        request.setAttribute("links", links);

        return "getlinks";
    }
    @RequestMapping(value = "/GetTopNews",method = RequestMethod.GET)
    public String getTopNews(HttpServletRequest request, HttpServletResponse response) {


        LinkService linkService = new LinkService();

        request.setAttribute("categoryfeeds", linkService.getTopNews());


        return "categoryRss";

    }

    @RequestMapping(value = "/UpdateLikes",method = RequestMethod.GET)
    public @ResponseBody int updateLikes(HttpServletRequest request, HttpServletResponse response,HttpSession session,@RequestParam(value = "id",required = true)int id) {


        String mobile_id = request.getParameter("mobile_id");
        String category = request.getParameter("category");

        LinkService linkService = new LinkService();
        int likes = linkService.updateLikes(id, mobile_id);


        InputStream input = null;
        Properties prop = new Properties();


        try {

          //  input = new FileInputStream("/var/lib/tomcat7/webapps/ROOT/WEB-INF/db.properties");
            //input = new FileInputStream("/usr/share/tomcat/webapps/ROOT/WEB-INF/db.properties");
           // input = new FileInputStream("/Users/upspring/Desktop/chudachuda-web/new/chudchuda-trending-algo/src/main/webapp/WEB-INF/db.properties" );

           // prop.load(input);
            File file = ResourceUtils.getFile("classpath:application.properties");
            InputStream in = new FileInputStream(file);
            prop.load(in);
            System.out.println("servername"+prop.getProperty("server"));


            String listOfServer=prop.getProperty("server");

            String[] server=listOfServer.split(",");//Separating the word using delimiter Comma and stored in an array

            String concate=category+"&id="+id+"&likes="+likes;

            for(int k=0;k<server.length;k++) {


                System.out.println(server[k]);
                String server_ip=server[k].concat(concate);
                System.out.println("After....."+server_ip);

                String inputs=server_ip;
                CloseableHttpClient client = HttpClients.createDefault();
                URIBuilder builder = new URIBuilder(inputs);

                HttpGet get = new HttpGet(builder.build());

                CloseableHttpResponse response1 = client.execute(get);

                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        response1.getEntity().getContent()));

                String inputLine;
                StringBuffer HttpResponse = new StringBuffer();


                System.out.println("Updating Hash Successfully.........");
                reader.close();


            }

        }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }
            catch (URISyntaxException ex)
            {
                ex.printStackTrace();
            }


        // request.setAttribute("links", links);
        System.out.println("controller like"+likes);
        return likes;
    }



    @RequestMapping(value = "/ChudachudaHashUpdate",method = RequestMethod.GET)
    public void UpdatLikes(HttpServletRequest request, HttpServletResponse response) {

        System.out.println("inside of updating");
                String category =request.getParameter("category");
                String  ids= request.getParameter("id");
                String  likes=request.getParameter("likes");
                Integer id=Integer.parseInt(ids);

        String java = chudachudaHash.get(category);
        if(java==null)
        {
            chudachudaHash.put(category, linkService.getCategoryNewsForMobile(category));
        }

        if(java!=null) {

            //System.out.println("java---->" + java);
            JSONObject jsnobject = new JSONObject(java);


            JSONArray jsonArray = jsnobject.getJSONArray("feed");
            JSONArray ja = new JSONArray();
            JSONObject mainObj = new JSONObject();


            for (int i = 0; i < jsonArray.length(); i++) {
                if (id == jsonArray.getJSONObject(i).getInt("id")) {

                    jsonArray.getJSONObject(i).put("likes", likes);
                    jsonArray.getJSONObject(i).put("liked", 1);

                    //System.out.println("console " + jsonArray.getJSONObject(i));
                    ja.put(jsonArray.getJSONObject(i));
                } else {
                    ja.put(jsonArray.getJSONObject(i));
                }


            }

            mainObj.put("feed", ja);
          //  System.out.println(mainObj);
            chudachudaHash.put(category, mainObj.toString());
        }

    }


    @RequestMapping(value = "/IsLiked",method = RequestMethod.GET)
    public @ResponseBody ArrayList isLiked(HttpServletRequest request, HttpServletResponse response,HttpSession session) {

        String deviceId=request.getParameter("deviceId");
        String domain=request.getParameter("domain");

        LinkService linkService = new LinkService();

        ArrayList likes;
        likes= linkService.isLiked(deviceId,domain);

        return likes;

    }
    @RequestMapping(value = "/IsLikedForKathambam",method = RequestMethod.GET)
    public @ResponseBody ArrayList isLikedForKathambam(HttpServletRequest request, HttpServletResponse response,HttpSession session) {

        String deviceId=request.getParameter("deviceId");

        LinkService linkService = new LinkService();

        ArrayList likes;
        likes= linkService.isLikedForKathambam(deviceId);

        return likes;

    }

    @RequestMapping(value = "/GetVideoFeed",method = RequestMethod.GET)
    public String getVideoFeed(HttpServletRequest request, HttpServletResponse response) {
        LinkService linkService = new LinkService();
        request.setAttribute("categoryfeeds", linkService.getVideoFeed());

         return "categoryRss";
    }

    @RequestMapping(value = "/UpdateBookmark",method = RequestMethod.GET)
    public @ResponseBody int updateBookmarks(HttpServletRequest request, HttpServletResponse response,HttpSession session,@RequestParam(value = "id",required = true)int id) {


        System.out.println("INSIDE OF BOOKMARK UPDATING..");
        String mobile_id = request.getParameter("mobile_id");
        String category = request.getParameter("category");

        LinkService linkService = new LinkService();
        int bookmarks = linkService.updateBookmark(id, mobile_id);


        InputStream input = null;
        Properties prop = new Properties();


        try {

            File file = ResourceUtils.getFile("classpath:application.properties");
            InputStream in = new FileInputStream(file);
            prop.load(in);

            String listOfServer=prop.getProperty("bookmarkServer");

            String[] server=listOfServer.split(",");//Separating the word using delimiter Comma and stored in an array

            String concate=category+"&id="+id+"&bookmark="+bookmarks;

            for(int k=0;k<server.length;k++) {


                System.out.println(server[k]);
                String server_ip=server[k].concat(concate);
                System.out.println("After....."+server_ip);

                String inputs=server_ip;
                CloseableHttpClient client = HttpClients.createDefault();
                URIBuilder builder = new URIBuilder(inputs);

                HttpGet get = new HttpGet(builder.build());

                CloseableHttpResponse response1 = client.execute(get);

                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        response1.getEntity().getContent()));

                String inputLine;
                StringBuffer HttpResponse = new StringBuffer();


                System.out.println("Updating Hash Successfully.........");
                reader.close();


            }

        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
        catch (URISyntaxException ex)
        {
            ex.printStackTrace();
        }


        // request.setAttribute("links", links);
        System.out.println("controller bookmarks result"+bookmarks);
        return bookmarks;
    }


    @RequestMapping(value = "/ChudachudaHashBookmarkUpdate",method = RequestMethod.GET)
    public void bookmarkUpdate(HttpServletRequest request, HttpServletResponse response) {

        System.out.println("inside of updating");
        String category =request.getParameter("category");
        String  ids= request.getParameter("id");
        String  bookmark=request.getParameter("bookmark");
        Integer id=Integer.parseInt(ids);

        String java = chudachudaHash.get(category);
        if(java==null)
        {
            chudachudaHash.put(category, linkService.getCategoryNewsForMobile(category));
        }

        if(java!=null) {

            //System.out.println("java---->" + java);
            JSONObject jsnobject = new JSONObject(java);


            JSONArray jsonArray = jsnobject.getJSONArray("feed");
            JSONArray ja = new JSONArray();
            JSONObject mainObj = new JSONObject();


            for (int i = 0; i < jsonArray.length(); i++) {
                if (id == jsonArray.getJSONObject(i).getInt("id")) {

                    jsonArray.getJSONObject(i).put("bookmark", bookmark);
                  //  jsonArray.getJSONObject(i).put("liked", 1);

                    //System.out.println("console " + jsonArray.getJSONObject(i));
                    ja.put(jsonArray.getJSONObject(i));
                } else {
                    ja.put(jsonArray.getJSONObject(i));
                }


            }

            mainObj.put("feed", ja);
            //  System.out.println(mainObj);
            chudachudaHash.put(category, mainObj.toString());
        }

    }
    @RequestMapping(value = "/IsBookmarked",method = RequestMethod.GET)
    public @ResponseBody ArrayList isBookmarked(HttpServletRequest request, HttpServletResponse response,HttpSession session) {

        String deviceId=request.getParameter("deviceId");
        String domain=request.getParameter("domain");

        LinkService linkService = new LinkService();

        ArrayList likes;
        likes= linkService.isBookmarked(deviceId,domain);

        return likes;

    }

    @RequestMapping(value = {"/GetBookmarks"}, method = {RequestMethod.GET})
    public String getBookmarks(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        String deviceId = request.getParameter("deviceId");
        LinkService linkService = new LinkService();
        request.setAttribute("categoryfeeds", linkService.getBookmarks(deviceId));
        return "categoryRss";
    }

    @RequestMapping(value = {"/CheckUserExists"}, method = {RequestMethod.GET})
    public @ResponseBody User checkUserExists(HttpServletRequest request, HttpServletResponse response, HttpSession session) {

        String email=request.getParameter("email");
        String network=request.getParameter("network");
        UserService userService=new UserService();
        User user=userService.getLoginUser(email,network);
        return user;
    }

    @RequestMapping(value = {"/UpdatePassword"}, method = {RequestMethod.GET})
    public @ResponseBody int updatePassword(HttpServletRequest request, HttpServletResponse response,HttpSession session) {


        System.out.println("inside of update password.....");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        System.out.println("password"+password);
        String pass=encryptPassword(password);
        System.out.println("Encoded password"+password);
        UserService userService=new UserService();
        int likes = userService.updatePassword(email,pass);



        return likes;
    }
    @RequestMapping(value = {"/Share"}, method = {RequestMethod.GET})
    public String share(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
          return "share";
    }

    @RequestMapping(value = {"/UsersListOf"}, method = {RequestMethod.GET})
    public String usersListOf(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        UserService userService=new UserService();

       List<User> user2=null;

      user2= userService.getAllUsers();
      System.out.println("---->"+user2.get(0).getUserName());
       request.setAttribute("users",user2);
        return "feedRss";
    }

    @RequestMapping(value = "/News_Detail",method = RequestMethod.GET)
    public String newsDetail(HttpServletRequest request, HttpServletResponse response,HttpSession session) {

        System.out.println("news source.....");


        double randomNo = Math.random();
        randomNo = randomNo * 10;
        randomNo = randomNo % 5;

        // randomNo = randomNo * -1;


        User user=null;
        if (session.getAttribute("userInfo") == null) {
            checkCookie2(request, response);
        }
        if (session.getAttribute("userInfo") != null) {
               user = (User) request.getSession().getAttribute("userInfo");
        }
        request.getSession().setAttribute("stylefile", "style_" + ((int) randomNo) + ".css");


        String timestamp = request.getParameter("t");

        String id=request.getParameter("id");
        session.setAttribute("domain","All");
        session.setAttribute("category", "All");
        session.removeAttribute("domainCategory");

        LinkService linkService = new LinkService();
        List<Link>links=null;

       links=linkService.getNews_Detail(id,(user != null ? user.getUserId() : 0));
       request.setAttribute("url",links.get(0).getUrl());
        request.setAttribute("title",links.get(0).getTitle());
        request.setAttribute("description",links.get(0).getDesc());
        request.setAttribute("imageUrl",links.get(0).getImageUrl());
        request.setAttribute("id",links.get(0).getLinkId());


        request.setAttribute("links", links);

        return "newsView";
    }




    @RequestMapping(value={"/ShareEmail"},method={RequestMethod.GET})
    public  @ResponseBody void ShareEmail(HttpServletRequest request, HttpServletResponse response)
    {

        String email = request.getParameter("emailId");
        String title= request.getParameter("title");
        String image_url= request.getParameter("image_url");
        String url= request.getParameter("url");
        String domain= request.getParameter("domain");
        System.out.println(email);
        System.out.println(title);
        System.out.println(image_url);
        System.out.println(url);
        LinkService linkService=new LinkService();
        linkService.sendEmail(email,title,image_url,url,domain);

    }
    @RequestMapping(value = {"/ReadLater"}, method = {RequestMethod.GET})
    public @ResponseBody String ReadLater(HttpServletRequest request, HttpServletResponse response, HttpSession session) {

        System.out.println("Read Later-->");
        String email=request.getParameter("emailId");
        System.out.println("Read Later-->"+email);

        String linkId=request.getParameter("linkId");
        System.out.println("Read Later-->"+linkId);
        LinkService linkService=new LinkService();
        linkService.saveBookmark(linkId,email);
        return "success";
    }
    @RequestMapping(value = "/ReadLaterArticle",method = RequestMethod.GET)
    public String readLaterArticle(HttpServletRequest request, HttpServletResponse response,HttpSession session) {



        double randomNo = Math.random();
        randomNo = randomNo * 10;
        randomNo = randomNo % 5;

        // randomNo = randomNo * -1;

        User user=null;

        request.getSession().setAttribute("stylefile", "style_" + ((int) randomNo) + ".css");


        String timestamp = request.getParameter("t");

        String emailId=request.getParameter("userId");

        if (session.getAttribute("userInfo") == null) {
            System.out.println("session is empty");
            checkCookie2(request, response);
        }
        System.out.println("read later cookie----->"+session.getAttribute("userInfo"));
        if (session.getAttribute("userInfo") != null) {


            user = (User) request.getSession().getAttribute("userInfo");
            System.out.println("userId----->"+user.getUserId());
        }

        session.setAttribute("save","READ LATER" );
        session.setAttribute("category","All");
        session.setAttribute("domain","All");
        session.removeAttribute("domainCategory");
        session.setAttribute("save","READ LATER" );
        // String category="tamil.samayam.com";

        LinkService linkService = new LinkService();
        List<Link>links;
        links=linkService.getReadLaterArticle(emailId,user != null ? user.getUserId() : 0);


        request.setAttribute("links", links);

        return "readLater";
    }
    @RequestMapping(value = "/InfiniteScrollAjax", method = RequestMethod.GET)
    public @ResponseBody  List<Link> infiniteAjax(HttpServletRequest request, HttpServletResponse response, HttpSession session)throws IOException {

        System.out.println("inisde of infinate ");
        LinkService linkService = new LinkService();
        List<Link> link=null;
        List<Link> newList = new ArrayList<Link>();
        User user=null;
        if (session.getAttribute("userInfo") == null) {
            System.out.println("session is empty");
            checkCookie2(request, response);
        }
        System.out.println("read later cookie----->"+session.getAttribute("userInfo"));
        if (session.getAttribute("userInfo") != null) {


            user = (User) request.getSession().getAttribute("userInfo");
            System.out.println("userId----->"+user.getUserId());
        }

        String start=request.getParameter("start");
        Integer startValue=Integer.parseInt(start);
        String end=request.getParameter("end");
        Integer endValue=Integer.parseInt(end);
        String category=request.getParameter("category");
        String domain=request.getParameter("domain");

        String url=request.getParameter("url");
        System.out.println("start"+startValue);
        System.out.println("end"+endValue);
        System.out.println(" category------>"+category);
        System.out.println("domain"+domain);
        System.out.println("url is"+url);

        if(category.equals("india")){
            category="इंडिया";
        }if(category.equals("world")){category="दुनिया";}
        if(category.equals("sports")){category="खेल";}
        if(category.equals("general")){category="सामान्य समाचार";}
        if(category.equals("cinema")){category="मनोरंजन";}

        System.out.println("---->new category"+category);
        if(url.contains("GetNews?category"))
        {
            System.out.println("its category page");
            link=linkService.getNews(category,user != null ? user.getUserId() : 0,start,end);
        }
        if(url.contains("GetCategoryNewsForWeb?category"))
        {
            System.out.println("its News source page");
            link=linkService.getCategoryNewsForWeb(category,user != null ? user.getUserId() : 0,start,end);
        }
        if(url.contains("GetCategoryForNews?category"))
        {
            System.out.println("its News source and domain");
            link=linkService.getCategoryForNews(category,domain,user != null ? user.getUserId() : 0,start,end);

        }
        if(url.contains("GetLinks?tab=trending"))
        {
            System.out.println("its trending page");
            link = linkService.getTrendingLinks(
                    (user != null ? user.getUserId() : 0), "All", "All", "All");

        }
        if(url.contains("?tab=new"))
        {
            System.out.println("its landing page");
            link = linkService.getFrontPageNews(
                    (user != null ? user.getUserId() : 0), start,end);

        }
        //  link= (List<Link>)request.getSession().getAttribute("moreLink");
        System.out.println("--->"+link.size());

        for (int i = startValue; i < endValue; i++) {
            boolean inBounds = (i >= startValue) && (i <link.size());

            System.out.println("inBounds"+inBounds);
            System.out.println("i value" + i);
            if(inBounds) {
                newList.add(link.get(i));
            }
            else {
                break;
            }



        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TimeZone istTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
        java.util.Date d = new java.util.Date();
        sdf.setTimeZone(istTimeZone);
        String date1 = sdf.format(d);
        System.out.println("query end date is"+date1);

        return newList;
    }

    @RequestMapping(value = {"/isPopupActive"}, method = {RequestMethod.GET})
    public @ResponseBody  String popupActive(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        session.setAttribute("isPopup", false);
       /* String status=request.getParameter("status");
        if(status.equals("false")) {
            session.setAttribute("isPopup", false);
        }
        else
        {
            session.setAttribute("isPopup", true);
        }*/

        return "success";
    }

    @RequestMapping(value="/Recommendation",method = RequestMethod.GET)
    public String recommendation(HttpServletRequest request,HttpServletResponse response,HttpSession session)
    {

        session.setAttribute("save","RECOMMENDATION" );
        UserService userService = new UserService();
        LinkService linkService = new LinkService();

        List<Link> tag = null;
        List<Link> links = null;
        List<Link> domainList = new ArrayList<Link>();
        List<Link> newList = new ArrayList<Link>();

        List<Link> joinedLink=new ArrayList<Link>();
        List<FollowDomain> domains = null;
        List<NameCountVO> tags ;


        double randomNo = Math.random();
        randomNo = randomNo * 10;
        randomNo = randomNo % 5;

        // randomNo = randomNo * -1;

        request.getSession().setAttribute("stylefile", "style_" + ((int) randomNo) + ".css");

        User user=null;

        response.setCharacterEncoding("UTF-8");

        if (session.getAttribute("userInfo") == null) {
            System.out.println("session is empty");
            checkCookie2(request, response);
        }

        if (session.getAttribute("userInfo") != null) {
            user = (User) request.getSession().getAttribute("userInfo");


        }

        links = linkService.getRecommendationLinks((user != null ? user.getUserId() : 0));


        System.out.println("length"+links.size());
        joinedLink=links;

        request.setAttribute("links", joinedLink);
        session.setAttribute("moreLink",links);




        return "recommendation";
    }
    @RequestMapping(value = "/chudachudaMap",method = RequestMethod.GET)
    public String gettingLocation(HttpServletRequest request)throws UnknownHostException {


        String linkid=request.getParameter("id");
        System.out.println("link id--->"+linkid);

        LinkService linkService=new LinkService();
        List<HashMap<String,String>> t= linkService.getMapDetailsForArticle(linkid);
        System.out.println("hashmap--->"+t);
        Gson gson = new Gson();
        String json = gson.toJson(t);
        request.setAttribute("mapDetails",json);

        return "location";

    }
    @RequestMapping(value={"/LikeArticles"},method={RequestMethod.GET})
    public  @ResponseBody HashMap<String, String> likeArticles(HttpServletRequest request, HttpServletResponse response)
    {
        System.out.println("like artyicles");
        String email = request.getParameter("emailId");
        String linkId = request.getParameter("id");

        String remoteAddr = "";
        GeoLocation location=null;

        if (request != null) {
            System.out.println("remote --->" + request.getRemoteAddr());
            remoteAddr = request.getRemoteAddr();
            //System.out.println("By String IP address: \n" +
            //      GeoIPv4.getLocation(request.getRemoteAddr()));
            System.out.println("request remoteAddr" + remoteAddr);
            for (String header : IP_HEADER_CANDIDATES) {
                String ip = request.getHeader(header);
                if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {
                    remoteAddr = ip;
                }
            }

        }
     //remoteAddr="103.197.113.220";

        location=getIPDetails(remoteAddr);

        System.out.println(email+linkId);
        LinkService linkService = new LinkService();

        HashMap<String,String> votes=linkService.likeArticles(email,linkId,location.getLongitude(),location.getLatitude(),location.getCountryName());

        System.out.println(votes.get("vote_up"));
        System.out.println(votes.get("vote_down"));
        return votes;

    }
    public GeoLocation getIPDetails(String ip)
    {
        System.out.println("ip--"+ip);

        GeoLocation geoDAta=GeoIPv4.getLocation(ip);

        System.out.println(geoDAta);
        System.out.println(geoDAta.getCountryName());
        System.out.println(geoDAta.getCity());
        System.out.println(geoDAta.getLatitude());
        System.out.println(geoDAta.getLongitude());
        return  geoDAta;

    }

    @RequestMapping(value={"/DisLikeArticles"},method={RequestMethod.GET})
    public  @ResponseBody HashMap<String, String> dislikeArticles(HttpServletRequest request, HttpServletResponse response)
    {
        System.out.println("like artyicles");
        String email = request.getParameter("emailId");
        String linkId = request.getParameter("id");

        String remoteAddr="";
        GeoLocation location=null;

        if (request != null) {
            System.out.println("remote --->" + request.getRemoteAddr());
            remoteAddr = request.getRemoteAddr();
            //System.out.println("By String IP address: \n" +
            //      GeoIPv4.getLocation(request.getRemoteAddr()));
            System.out.println("request remoteAddr" + remoteAddr);
            for (String header : IP_HEADER_CANDIDATES) {
                String ip = request.getHeader(header);
                if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {
                    remoteAddr = ip;
                }
            }

        }
        //remoteAddr="103.197.113.220";
        location=getIPDetails(remoteAddr);

        System.out.println(email+linkId);
        LinkService linkService = new LinkService();
        HashMap<String,String> votes=linkService.disLikeArticles(email,linkId,location.getLongitude(),location.getLatitude(),location.getCountryName());
        System.out.println(votes.get("vote_up"));
        System.out.println(votes.get("vote_down"));
        return votes;
    }

}
