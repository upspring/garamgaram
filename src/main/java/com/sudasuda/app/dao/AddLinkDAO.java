package com.sudasuda.app.dao;

import com.sudasuda.app.domain.AddLink;

public interface AddLinkDAO {

    public boolean addNewLink(AddLink link);
    public boolean isLinkPresent(String url);
}

