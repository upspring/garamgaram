package com.sudasuda.app.dao;

import org.springframework.beans.factory.annotation.Autowired;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import  com.sudasuda.app.domain.AddLink;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class AddLinkDAOImpl implements AddLinkDAO {


    @Autowired(required = false)
    private JdbcTemplate jdbcTemplateObj;

    @Autowired(required = false)
    private NamedParameterJdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    public void setJdbcTemplateObj(JdbcTemplate jdbcTemplateObj){this.jdbcTemplateObj=jdbcTemplateObj;}

    public boolean addNewLink(AddLink link) {

        int result = 0;
        try {

            java.sql.Timestamp date1=new java.sql.Timestamp(new java.util.Date().getTime());

            System.out.println("Timestamp value is"+date1);


            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            TimeZone istTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
            java.util.Date d = new java.util.Date();
            sdf.setTimeZone(istTimeZone);
            String date2 = sdf.format(d);

            System.out.println("Asia/Kolkatta time is-->"+date2);



            String addLinkSql = "INSERT INTO link(userId,idlink,title,url,date_created,votes,comments,domain,spam,language,category,country,media_type,image_url,description,format,ads_publish_date)" +
                    " VALUES(:userId,:linkId,:title,:url,:createdBy,:votes,:comments,:domain,:spam,:lang,:category,:country,:media_type,:imageUrl,:desc,:format,:ads_publish_date)";
            //  "  VALUES('"+link.getUserId()+",'"+link.getTitle()+",'"+link.getUrl()+",'"+link.getDateCreated()+",domain='"+domain+"','"+link.getDomain()+",'"+link.getLanguage()+",'"+link.getCategory()+",'"+link.getCountry()+",'"+link.getImageUrl()+",'"+link.getDesc()+"')";


            HashMap<String, Object> parameters = new HashMap<String, Object>();
            //Link link=new Link();
            parameters.put("linkId", link.getLinkId());
            parameters.put("userId", link.getUserId());
            parameters.put("title", link.getTitle());
            parameters.put("url", link.getUrl());
            parameters.put("desc", link.getDesc());
            parameters.put("createdBy", date2);
            parameters.put("category", link.getCategory());
            // System.out.println("date"+link.getDateCreated());
            parameters.put("votes",link.getVotes());
            parameters.put("comments",link.getNoOfComments());
            parameters.put("spam",link.getSpam());
            parameters.put("imageUrl", link.getImageUrl());
            parameters.put("domain", link.getDomain());
            parameters.put("lang", link.getLanguage());
            parameters.put("country", link.getCountry());
            parameters.put("media_type",link.getMedia_type());
            parameters.put("format",link.getVideo_format());
            parameters.put("ads_publish_date",link.getAds_published_date());

            System.out.println("dao ads ");
            result = jdbcTemplate.update(addLinkSql, parameters);


            System.out.println("inserted successfully");
        } catch (Exception e) {
            e.printStackTrace();
        }


        return (result > 0) ? true : false;
    }

    /*  public void cleanUpDuplicates() {

          try {

              String sql = "delete from link  where url in ( select x.url from (select b.url from link b group by b.url having count(b.url)  > 1) as x )";

              HashMap<String, Object> params = new HashMap<String, Object>();

              int rows = jdbcTemplate.update(sql, params);


          } catch (Exception ex) {
              ex.printStackTrace();

          }

      }

  */
    /*public List<AddLink> getFeedLinks(int currUser, String language,
                                   String category, String country, String timestamp) {

        List<Link> links = new ArrayList<Link>();


        try {

            // String SQL =
            // "select *,b.username from link a, user b where a.userId=b.idUser order by a.votes desc, a.date_created";
            String SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted from link a left join link_user c on a.idlink=c.linkId and c.userId="
                    + currUser + " order by a.votes desc, a.date_created";
            HashMap<String, Object> params = new HashMap<String, Object>();

            if (language == null || category == null || country == null)
                return links;

            // No filter scenario

            if (language.equalsIgnoreCase("all")
                    && category.equalsIgnoreCase("all")
                    && country.equalsIgnoreCase("all"))
                SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v,timestampdiff(HOUR, a.date_created, now()) as hours,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId=" + currUser + " ) as bookmark from link a left join link_user c on a.idlink=c.linkId and c.userId=" + currUser + " where spam < 4  and timestampdiff(HOUR, a.date_created, now()) <= 100 order by a.date_created desc limit 0,20;";

            // Language only filter

            if (!language.equalsIgnoreCase("all")
                    && category.equalsIgnoreCase("all")
                    && country.equalsIgnoreCase("all"))
                SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v,timestampdiff(HOUR, a.date_created, now()) as hours,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId=" + currUser + " ) as bookmark from link a left join link_user c on a.idlink=c.linkId and c.userId="
                        + currUser
                        + " where spam < 4 and language='"
                        + language
                        + "'  and timestampdiff(HOUR, a.date_created, now()) <= 100 order by a.date_created desc";

            // Category filter

            if (language.equalsIgnoreCase("all")
                    && !category.equalsIgnoreCase("all")
                    && country.equalsIgnoreCase("all"))
                SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v,timestampdiff(HOUR, a.date_created, now()) as hours,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId=" + currUser + " ) as bookmark from link a left join link_user c on a.idlink=c.linkId and c.userId="
                        + currUser
                        + " where spam < 4 and category='"
                        + category
                        + "' and timestampdiff(HOUR, a.date_created, now()) <= 100 order by a.date_created desc";

            // Country filter

            if (language.equalsIgnoreCase("all")
                    && category.equalsIgnoreCase("all")
                    && !country.equalsIgnoreCase("all"))
                SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v,timestampdiff(HOUR, a.date_created, now()) as hours,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId=" + currUser + " ) as bookmark from link a left join link_user c on a.idlink=c.linkId and c.userId="
                        + currUser
                        + " where spam < 4 and country='"
                        + country
                        + "'  and timestampdiff(HOUR, a.date_created, now()) <= 100 order by a.date_created desc";

            // Language, Category and Country filter

            if (!language.equalsIgnoreCase("all")
                    && !category.equalsIgnoreCase("all")
                    && !country.equalsIgnoreCase("all"))
                SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v,timestampdiff(HOUR, a.date_created, now()) as hours,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId=" + currUser + " ) as bookmark from link a left join link_user c on a.idlink=c.linkId and c.userId="
                        + currUser
                        + " where spam < 4 and category='"
                        + category
                        + "' and language='"
                        + language
                        + "' and country='"
                        + country
                        + "' and a.date_created >= '" + timestamp + "' order by a.date_created desc limit 0,50";

            // Category and Language filter

            if (!language.equalsIgnoreCase("all")
                    && !category.equalsIgnoreCase("all")
                    && country.equalsIgnoreCase("all"))
                SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v,timestampdiff(HOUR, a.date_created, now()) as hours,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId=" + currUser + " ) as bookmark from link a left join link_user c on a.idlink=c.linkId and c.userId="
                        + currUser
                        + " where spam < 4 and category='"
                        + category
                        + "' and language='"
                        + language
                        + "' and timestampdiff(HOUR, a.date_created, now()) <= 100 order by a.date_created desc";

            // Category and country filter

            if (language.equalsIgnoreCase("all")
                    && !category.equalsIgnoreCase("all")
                    && !country.equalsIgnoreCase("all"))
                SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v,timestampdiff(HOUR, a.date_created, now()) as hours,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId=" + currUser + " ) as bookmark from link a left join link_user c on a.idlink=c.linkId and c.userId="
                        + currUser
                        + " where spam < 4 and category='"
                        + category
                        + "' and country='"
                        + country
                        + "' and timestampdiff(HOUR, a.date_created, now()) <= 100 order by a.date_created desc";

            // Language and Country

            if (!language.equalsIgnoreCase("all")
                    && category.equalsIgnoreCase("all")
                    && !country.equalsIgnoreCase("all"))
                SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v,timestampdiff(HOUR, a.date_created, now()) as hours,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId=" + currUser + " ) as bookmark from link a left join link_user c on a.idlink=c.linkId and c.userId="
                        + currUser
                        + " where spam < 4 and language='"
                        + language
                        + "' and country='"
                        + country
                        + "' and timestampdiff(HOUR, a.date_created, now()) <= 100 order by a.date_created desc limit 0.50";

            // logger.info(SQL);
            Map<String, Object> parameters = new HashMap<String, Object>();

            links = jdbcTemplate.query(SQL, parameters, new RowMapper<Link>() {
                public Link mapRow(ResultSet rs, int rowNum) throws SQLException {
                    Link link = new Link();
                    link.setUrl(rs.getString("url"));
                    link.setTitle(rs.getString("title"));
                    link.setLinkId(rs.getInt("idlink"));
                    link.setVotes(rs.getInt("votes"));
                    link.setSubmitedBy(rs.getString("username"));
                    link.setVoted((rs.getString("voted") != null));
                    link.setBookmarked((rs.getString("bookmark") != null));
                    link.setHoursElapsed((rs.getLong("hours")));
                    link.setNoOfComments(rs.getInt("comments"));
                    link.setDomain(rs.getString("domain"));

                    link.setDesc(rs.getString("description"));
                    link.setCategory(rs.getString("category"));
                    link.setLanguage(rs.getString("language"));
                    link.setCountry(rs.getString("country"));
                    link.setTags(rs.getString("tags"));
                    link.setSpam(rs.getInt("spam"));
                    link.setImageUrl(rs.getString("image_url"));
                    link.setDateCreated(rs.getTimestamp("date_created"));
                    //  System.out.println("the link is-------------->"+link);
                    return link;
                }
            });

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return links;

    }*/
    public boolean isLinkPresent(String url) {
        System.out.println("isLinkpresent"+url);

        int res1=0;
        try {
            // String link=null;
            System.out.println("before11111");
            String Sql ="select count(*) from link where url=?";

            System.out.println("before");
            //   List<Map<String, Object>> link = jdbcTemplateObj.queryForList(Sql, new Object[] {url});
//            System.out.println(jdbcTemplateObj.queryForList(Sql, new Object[] {url}));
            res1 =  (Integer) jdbcTemplateObj.queryForObject(Sql, new Object[] { url }, Integer.class);

            System.out.println("res--->"+res1);
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return (res1>0)? true : false;
    }

/*
    public List<AddLink> getNewLinks(int currUser, String language,
                                  String category, String country) {
        List<Link> links = null;


        try {

            // String SQL =
            // "select *,b.username from link a, user b where a.userId=b.idUser order by a.votes desc, a.date_created";
            String SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted from link a left join link_user c on a.idlink=c.linkId and c.userId="
                    + currUser + " order by a.votes desc, a.date_created";

            if (language == null || category == null || country == null)
                return links;

            // No filter scenario

            if (language.equalsIgnoreCase("all")
                    && category.equalsIgnoreCase("all")
                    && country.equalsIgnoreCase("all"))
                SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v,timestampdiff(HOUR, a.date_created, now()) as hours,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId=" + currUser + " ) as bookmark from link a left join link_user c on a.idlink=c.linkId and c.userId=" + currUser + " where spam < 4  and timestampdiff(HOUR, a.date_created, now()) <= 50 order by a.date_created desc limit 0,50";

            // Language only filter

            if (!language.equalsIgnoreCase("all")
                    && category.equalsIgnoreCase("all")
                    && country.equalsIgnoreCase("all"))
                SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v,timestampdiff(HOUR, a.date_created, now()) as hours,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId=" + currUser + " ) as bookmark from link a left join link_user c on a.idlink=c.linkId and c.userId="
                        + currUser
                        + " where spam < 4 and language='"
                        + language
                        + "'  and timestampdiff(HOUR, a.date_created, now()) <= 100 order by a.date_created desc";

            // Category filter

            if (language.equalsIgnoreCase("all")
                    && !category.equalsIgnoreCase("all")
                    && country.equalsIgnoreCase("all"))
                SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v,timestampdiff(HOUR, a.date_created, now()) as hours,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId=" + currUser + " ) as bookmark from link a left join link_user c on a.idlink=c.linkId and c.userId="
                        + currUser
                        + " where spam < 4 and category='"
                        + category
                        + "' and timestampdiff(HOUR, a.date_created, now()) <= 100 order by a.date_created desc";

            // Country filter

            if (language.equalsIgnoreCase("all")
                    && category.equalsIgnoreCase("all")
                    && !country.equalsIgnoreCase("all"))
                SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v,timestampdiff(HOUR, a.date_created, now()) as hours,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId=" + currUser + " ) as bookmark from link a left join link_user c on a.idlink=c.linkId and c.userId="
                        + currUser
                        + " where spam < 4 and country='"
                        + country
                        + "'  and timestampdiff(HOUR, a.date_created, now()) <= 100 order by a.date_created desc";

            // Language, Category and Country filter

            if (!language.equalsIgnoreCase("all")
                    && !category.equalsIgnoreCase("all")
                    && !country.equalsIgnoreCase("all"))
                SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v,timestampdiff(HOUR, a.date_created, now()) as hours,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId=" + currUser + " ) as bookmark from link a left join link_user c on a.idlink=c.linkId and c.userId="
                        + currUser
                        + " where spam < 4 and category='"
                        + category
                        + "' and language='"
                        + language
                        + "' and country='"
                        + country
                        + "' and timestampdiff(HOUR, a.date_created, now()) <= 50 order by a.date_created desc limit 0,50";

            // Category and Language filter

            if (!language.equalsIgnoreCase("all")
                    && !category.equalsIgnoreCase("all")
                    && country.equalsIgnoreCase("all"))
                SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v,timestampdiff(HOUR, a.date_created, now()) as hours,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId=" + currUser + " ) as bookmark from link a left join link_user c on a.idlink=c.linkId and c.userId="
                        + currUser
                        + " where spam < 4 and category='"
                        + category
                        + "' and language='"
                        + language
                        + "' and timestampdiff(HOUR, a.date_created, now()) <= 100 order by a.date_created desc";

            // Category and country filter

            if (language.equalsIgnoreCase("all")
                    && !category.equalsIgnoreCase("all")
                    && !country.equalsIgnoreCase("all"))
                SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v,timestampdiff(HOUR, a.date_created, now()) as hours,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId=" + currUser + " ) as bookmark from link a left join link_user c on a.idlink=c.linkId and c.userId="
                        + currUser
                        + " where spam < 4 and category='"
                        + category
                        + "' and country='"
                        + country
                        + "' and timestampdiff(HOUR, a.date_created, now()) <= 100 order by a.date_created desc";

            // Language and Country

            if (!language.equalsIgnoreCase("all")
                    && category.equalsIgnoreCase("all")
                    && !country.equalsIgnoreCase("all"))
                SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v,timestampdiff(HOUR, a.date_created, now()) as hours,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId=" + currUser + " ) as bookmark from link a left join link_user c on a.idlink=c.linkId and c.userId="
                        + currUser
                        + " where spam < 4 and language='"
                        + language
                        + "' and country='"
                        + country
                        + "' and timestampdiff(HOUR, a.date_created, now()) <= 100 order by a.date_created desc";

            Map<String, Object> parameters = new HashMap<String, Object>();

            links = jdbcTemplate.query(SQL, parameters, new RowMapper<Link>() {
                public Link mapRow(ResultSet rs, int rowNum) throws SQLException {
                    Link link = new Link();
                    link.setUrl(rs.getString("url"));
                    link.setTitle(rs.getString("title"));
                    link.setLinkId(rs.getInt("idlink"));
                    link.setVotes(rs.getInt("votes"));
                    link.setSubmitedBy(rs.getString("username"));
                    link.setVoted((rs.getString("voted") != null));
                    link.setBookmarked((rs.getString("bookmark") != null));
                    link.setHoursElapsed((rs.getLong("hours")));
                    link.setNoOfComments(rs.getInt("comments"));
                    link.setDomain(rs.getString("domain"));
                    link.setDesc(rs.getString("description"));
                    link.setCategory(rs.getString("category"));
                    link.setLanguage(rs.getString("language"));
                    link.setCountry(rs.getString("country"));
                    link.setTags(rs.getString("tags"));
                    link.setSpam(rs.getInt("spam"));
                    link.setImageUrl(rs.getString("image_url"));
                    link.setDateCreated(rs.getTimestamp("date_created"));
                    //System.out.println("getNew links"+link.getTitle());
                    return link;
                }
            });
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return links;


    }

*/


/*
    public void voteUpLink(int linkid, int userid) {

        try {

            String SQL1 = "update link set votes = votes+1 where idlink=:link_id";
            String SQL2 = "insert into link_user (linkId,userId,date_voted) values (:link_id,:user_id,now())";

            HashMap<String,Object> params = new HashMap<String,Object>();
            params.put("link_id",linkid);
            params.put("user_id",userid);

            if (!isVotedAlready(userid, linkid)) {
                int rows=jdbcTemplate.update(SQL1,params);


                HashMap<String,Object> parameters = new HashMap<String,Object>();
                parameters.put("user_id",userid);
                parameters.put("link_id",linkid);

                jdbcTemplate.update(SQL2, parameters);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }  }





    public boolean isVotedAlready(int userId, int linkId) {

        try {

            String SQL = "select * from link_user where userId=? and linkId=?";

            String rs=jdbcTemplateObj.queryForObject(SQL,new Object[]{userId,linkId},String.class);

            if (rs.length()>0)
                return true;

        } catch (Exception ex) {
            ex.printStackTrace();
        }


        return false;
    }


    public AddLink getLink(String url) {

        Link links=null;


        try {


            String SQL = "select * from link where url=:url";
            Map<String,Object> parameters = new HashMap<String, Object>();
            parameters.put("url",url);

            links = jdbcTemplate.queryForObject(SQL, parameters, new RowMapper<Link>() {
                public Link mapRow(ResultSet rs, int rowNum) throws SQLException {
                    Link link = new Link();
                    link.setUrl(rs.getString("url"));
                    link.setTitle(rs.getString("title"));
                    link.setLinkId(rs.getInt("idlink"));

                    link.setVotes(rs.getInt("votes"));
                    link.setNoOfComments(rs.getInt("comments"));
                    link.setNoOfComments(rs.getInt("comments"));
                    link.setDomain(rs.getString("domain"));
                    link.setLanguage(rs.getString("language"));
                    link.setCategory(rs.getString("category"));
                    link.setCountry(rs.getString("country"));
                    link.setImageUrl(rs.getString("image_url"));
                    // System.out.println("id link"+link.getLinkId());
                    return link;
                }
            });

        } catch (Exception ex) {
            ex.printStackTrace();
        }


        return links;
    }
    public List<AddLink>getData() {
        List<Link> bundleList1 = new ArrayList<Link>();
        try {

            String t = "select * from link order by date_created limit 50";



            bundleList1 = jdbcTemplate.query(t, new RowMapper<Link>() {
                public Link mapRow(ResultSet rs, int rowNum) throws SQLException {
                    Link link = new Link();
                    link.setUrl(rs.getString("url"));
                    link.setTitle(rs.getString("title"));


                    return link;
                }
            });
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return bundleList1;

    }*/

}
