package com.sudasuda.app.dao;

import com.sudasuda.app.db.DBConnection;
import com.sudasuda.app.domain.Link;
import com.sudasuda.app.domain.User;
import com.sudasuda.app.vo.NameCountVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.net.URI;
import java.sql.*;
//import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

public class LinkDAO {

    private static final Logger logger = LoggerFactory.getLogger(LinkDAO.class);

    {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    // final HazelcastInstance hz = Hazelcast.newHazelcastInstance();

    public void updateLink(Link link) {
        DataSource ds = null;
        Connection conn = null;
        Statement stmt = null;
        String url="";

        try {
            URI uri = new URI(link.getUrl());
            String domain = uri.getHost();
            domain = domain.startsWith("www.") ? domain.substring(4) : domain;

            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String sql = "update link set title='" + link.getTitle() + "',url='" + link.getUrl() + "',domain='"+domain+"',image_url='" + link.getImageUrl() + "',language='" + link.getLanguage() + "',country='" + link.getCountry() + "',"
                    + "category='" + link.getCategory() + "',description='"+link.getDesc()+"' where idLink=" + link.getLinkId();
            logger.debug("SQL:" + sql);
            stmt.executeUpdate(sql);


        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Error while updating link Id=" + link.getLinkId() + ", title=" + link.getTitle());
        }

    }

    public void cleanUpDuplicates() {
        DataSource ds = null;
        Connection conn = null;
        Statement stmt = null;

        try {
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String sql = "delete from link  where url in ( select x.url from (select b.url from link b group by b.url having count(b.url)  > 1) as x )";

            logger.debug("SQL:" + sql);
            stmt.executeUpdate(sql);


        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Error while cleaning up duplicates");
        }

    }


    public void addLink(String url, String title, String userid,
                        String language, String category, String country,
                        String media_type, String tags[], String imageUrl, String desc) {
        DataSource ds = null;
        Connection conn = null;
        Statement stmt = null;

        // final Map<String, List<Link> > stories = hz.getMap("stories");

        // if (stories != null )
        //     stories.put("newStories", null);

        try {

            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            stmt = conn.createStatement();

            // URI uri = new URI(url);
            String domain = "";
            // domain = domain.startsWith("www.") ? domain.substring(4) : domain;
            URI uri = new URI(url);

            String domainName = uri.getHost();
            System.out.println("domain"+domainName);
            if (domainName != null) {
                String domainSite = domainName.startsWith("www.") ? domainName.substring(4) : domainName;
                if (domainSite == null) {
                    domain = uri.getHost();
                    //System.out.println("domain222"+domain);

                } else {
                    domain = domainSite;
                    System.out.println("domain3333"+domain);
                    if(domain.equals("hindutamil.in"))
                    {
                        domain="tamil.thehindu.com";
                    }

                }
            }
            java.sql.Timestamp date1=new java.sql.Timestamp(new java.util.Date().getTime());


            title = title.replace("'", " ");
            String SQL = "insert into link values (0," + userid + ",'" + url
                    + "','" + title + "','"+date1+"',0,0,'" + domain + "',0,'"
                    + language + "','" + category + "','" + country + "','"
                    + media_type + "','" + imageUrl + "','" + desc + "',0,0,0,0,0,0,'',0,'',0,'')";
            logger.info("SQL:" + SQL);
            stmt.executeUpdate(SQL);

            Link link = getLink(url);

            if (link.getLinkId() > 0) {
                for (String tag : tags) {
                    applyTag(tag, link.getLinkId());
                }
                voteUpLink(link.getLinkId(), Integer.valueOf(userid));
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (stmt != null)
                    stmt.close();

            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }


    public void updateImageUrl(int linkid, String imageUrl) {
        DataSource ds = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {

            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            stmt = conn.createStatement();
            String SQL = "update link set image_url='" + imageUrl + "' where idlink=" + linkid;

            stmt.executeUpdate(SQL);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public List<Link> getCurrentLinks(int currUser, String lang,
                                      String category, String country) {
        List<Link> links = new ArrayList<Link>();

        DataSource ds = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;




        try {

            // String SQL =
            // "select *,b.username from link a, user b where a.userId=b.idUser order by a.votes desc, a.date_created";
            String SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.* from link a left join link_user c on a.idlink=c.linkId and c.userId="
                    + currUser + " order by a.votes desc, a.date_created";

            if (lang == null || category == null || country == null)
                return links;

            // No filter scenario

            if (lang.equalsIgnoreCase("all")
                    && category.equalsIgnoreCase("all")
                    && country.equalsIgnoreCase("all"))
                SQL = "select distinct * from ( select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, (select userId from link_user lu where lu.linkId=a.idlink and lu.userId = " + currUser + " ) as voted,  (a.votes-1)/pow(timestampdiff(HOUR, a.date_created, now())+2,1.8) as v,"
                        + " (select count(distinct byUserId) from comment where linkid=a.idlink) as activists,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId= " + currUser + " ) as bookmark,"
                        + "timestampdiff(HOUR, a.date_created, now()) as hours from link a left join link_user c on a.idlink=c.linkId "
                        + " where spam < 4 and deleted=0 and publish=1 and timestampdiff(HOUR, a.date_created, now()) < 100000 ) as mq order by v desc";

            // Filter by Language

            if (!lang.equalsIgnoreCase("all")
                    && category.equalsIgnoreCase("all")
                    && country.equalsIgnoreCase("all"))
                SQL = "select distinct * from ( select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, (select userId from link_user lu where lu.linkId=a.idlink and lu.userId = " + currUser + " ) as voted,  (a.votes-1)/pow(timestampdiff(HOUR, a.date_created, now())+2,1.8) as v"
                        + ",(select count(distinct byUserId) from comment where linkid=a.idlink) as activists,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId= " + currUser + " ) as bookmark,"
                        + "timestampdiff(HOUR, a.date_created, now()) as hours from link a left join link_user c on a.idlink=c.linkId"
                        + " where language='"
                        + lang.trim()
                        + "' and spam < 4 and timestampdiff(HOUR, a.date_created, now()) < 100000 ) as mq order by v desc";

            // Filter by category

            if (lang.equalsIgnoreCase("all") && country.equalsIgnoreCase("all")
                    && !category.equalsIgnoreCase("all"))
                SQL = "select distinct * from ( select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*,(select userId from link_user lu where lu.linkId=a.idlink and lu.userId = " + currUser + " ) as voted,  (a.votes-1)/pow(timestampdiff(HOUR, a.date_created, now())+2,1.8) as v, "
                        + "(select count(distinct byUserId) from comment where linkid=a.idlink) as activists,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId= " + currUser + " ) as bookmark,"
                        + "timestampdiff(HOUR, a.date_created, now()) as hours from link a left join link_user c on a.idlink=c.linkId "
                        + " where category='"
                        + category.trim()
                        + "' and spam < 4 and timestampdiff(HOUR, a.date_created, now()) < 100000 ) as mq order by v desc";

            // Filter by country

            if (lang.equalsIgnoreCase("all")
                    && !country.equalsIgnoreCase("all")
                    && category.equalsIgnoreCase("all"))
                SQL = "select distinct * from ( select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, (select userId from link_user lu where lu.linkId=a.idlink and lu.userId = " + currUser + " ) as voted,  (a.votes-1)/pow(timestampdiff(HOUR, a.date_created, now())+2,1.8) as v,"
                        + "(select count(distinct byUserId) from comment where linkid=a.idlink) as activists,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId= " + currUser + " ) as bookmark,"
                        + "timestampdiff(HOUR, a.date_created, now()) as hours from link a left join link_user c on a.idlink=c.linkId "
                        + " where country='"
                        + country.trim()
                        + "' and spam < 4 and timestampdiff(HOUR, a.date_created, now()) < 100000 ) as mq order by v desc";

            // Filter by language and category

            if (!lang.equalsIgnoreCase("all")
                    && !category.equalsIgnoreCase("all")
                    && country.equalsIgnoreCase("all"))
                SQL = "select distinct * from (  select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, (select userId from link_user lu where lu.linkId=a.idlink and lu.userId = " + currUser + " ) as voted,  (a.votes-1)/pow(timestampdiff(HOUR, a.date_created, now())+2,1.8) as v, "
                        + "(select count(distinct byUserId) from comment where linkid=a.idlink) as activists,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId= " + currUser + " ) as bookmark,"
                        + "timestampdiff(HOUR, a.date_created, now()) as hours from link a left join link_user c on a.idlink=c.linkId "
                        + " where language='"
                        + lang.trim()
                        + "' and category='"
                        + category.trim()
                        + "' and spam < 4 and timestampdiff(HOUR, a.date_created, now()) < 100000 ) as mq order by v desc";

            // Filter by language and country

            if (!lang.equalsIgnoreCase("all")
                    && category.equalsIgnoreCase("all")
                    && !country.equalsIgnoreCase("all"))
                SQL = "select distinct * from ( select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, (select userId from link_user lu where lu.linkId=a.idlink and lu.userId = " + currUser + " ) as voted,  (a.votes-1)/pow(timestampdiff(HOUR, a.date_created, now())+2,1.8) as v,"
                        + "(select count(distinct byUserId) from comment where linkid=a.idlink) as activists,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId= " + currUser + " ) as bookmark,"
                        + "timestampdiff(HOUR, a.date_created, now()) as hours from link a left join link_user c on a.idlink=c.linkId "
                        + " where language='"
                        + lang.trim()
                        + "' and country='"
                        + country.trim()
                        + "' and spam < 4 and timestampdiff(HOUR, a.date_created, now()) < 100000 ) as mq order by v desc";

            // Filter by category and country

            if (lang.equalsIgnoreCase("all")
                    && !category.equalsIgnoreCase("all")
                    && !country.equalsIgnoreCase("all"))
                SQL = "select distinct * from ( select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, (select userId from link_user lu where lu.linkId=a.idlink and lu.userId = " + currUser + " ) as voted,  (a.votes-1)/pow(timestampdiff(HOUR, a.date_created, now())+2,1.8) as v,"
                        + "(select count(distinct byUserId) from comment where linkid=a.idlink) as activists,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId= " + currUser + " ) as bookmark,"
                        + "timestampdiff(HOUR, a.date_created, now()) as hours from link a left join link_user c on a.idlink=c.linkId "
                        + " where category='"
                        + category.trim()
                        + "' and country='"
                        + country.trim()
                        + "' and spam < 4 and timestampdiff(HOUR, a.date_created, now()) < 100000 ) as mq order by v desc";

            // Filter by language, category and country

            if (!lang.equalsIgnoreCase("all")
                    && !category.equalsIgnoreCase("all")
                    && !country.equalsIgnoreCase("all"))
                SQL = "select distinct * from ( select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, (select userId from link_user lu where lu.linkId=a.idlink and lu.userId = " + currUser + " ) as voted,  (a.votes-1)/pow(timestampdiff(HOUR, a.date_created, now())+2,1.8) as v,"
                        + "(select count(distinct byUserId) from comment where linkid=a.idlink) as activists,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId= " + currUser + " ) as bookmark,"
                        + "timestampdiff(HOUR, a.date_created, now()) as hours from link a left join link_user c on a.idlink=c.linkId "
                        + " where language='"
                        + lang.trim()
                        + "' and category='"
                        + category.trim()
                        + "' and country='"
                        + country.trim()
                        + "' and spam < 4 and timestampdiff(HOUR, a.date_created, now()) < 100000 ) as mq order by v desc";

            SQL = SQL + " LIMIT 0,60";

            logger.info("SQL:" + SQL);

            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            stmt = conn.createStatement();

            rs = stmt.executeQuery(SQL);

            while (rs.next()) {
                Link link = new Link();
                link.setUrl(rs.getString("url"));
                link.setTitle(rs.getString("title"));
                link.setLinkId(rs.getInt("idlink"));
                link.setVotes(rs.getInt("votes"));
                link.setSubmitedBy(rs.getString("username"));
                link.setVoted((rs.getString("voted") != null));
                link.setBookmarked((rs.getString("bookmark") != null));
                link.setHoursElapsed((rs.getLong("hours")));
                link.setNoOfComments(rs.getInt("comments"));
                link.setDomain(rs.getString("domain"));
                link.setCategory(rs.getString("category"));
                link.setLanguage(rs.getString("language"));
                link.setCountry(rs.getString("country"));
                link.setTags(rs.getString("tags"));

                link.setSpam(rs.getInt("spam"));
                link.setActivists(rs.getInt("activists"));
                link.setImageUrl(rs.getString("image_url"));
                links.add(link);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return links;
    }

    public List<Link> getNewLinks(int currUser, String language,
                                  String category, String country,String startLimit,String limit) {
        List<Link> links = new ArrayList<Link>();

        DataSource ds = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TimeZone istTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
        java.util.Date d = new java.util.Date();
        sdf.setTimeZone(istTimeZone);
        String date1 = sdf.format(d);

        System.out.println("---->limits"+limit);
        try {


            // SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v,timestampdiff(HOUR, a.date_created, now()) as hours,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId=" + currUser + " ) as bookmark from link a left join link_user c on a.idlink=c.linkId and c.userId=" + currUser + " where spam < 4 and deleted=0 and publish=1 and timestampdiff(HOUR, a.date_created, now()) <= 50 order by a.date_created desc limit 0,20"
            //String  SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v, TIMEDIFF('"+date1+"',date_created)as hours,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId=" + currUser + " ) as bookmark from link a left join link_user c on a.idlink=c.linkId and c.userId=" + currUser + " where spam < 4 and deleted=0 order by a.date_created desc limit 0,20";
            // Language only filter

            //   String SQL="select idlink,url,title,domain,description,category,image_url,likes,date_created,(select distinct isBookmarked from web_bookmark where link_id=idlink)as bookmarked,TIMEDIFF('"+date1+"',date_created)as hours from link where media_type='Text/HTML' GROUP BY title order by date_created DESC limit 0,"+limit+"";

            //  String SQL="select idlink,url,title,domain,description,category,image_url,likes,date_created,(select distinct no_of_likes from like_user where link_id=idlink)as liked,TIMEDIFF('"+date1+"',date_created)as hours from link where media_type='Text/HTML'  order by date_created DESC limit 0,50";
            //String SQL="select idlink,url,title,domain,(select user_id from web_bookmark where link_id=idlink and user_id='"+currUser+"')as userId,description,category,likes,image_url,vote_up,vote_down,likes,date_created,(select distinct isBookmarked from web_bookmark where link_id=idlink and user_id='"+currUser+"')as bookmarked,(select voted from link_votes where link_id=idlink and user_id='"+currUser +"')as voted,(select distinct no_of_likes from like_user where link_id=idlink)as liked,TIMEDIFF('"+date1+"',date_created)as hours from link where media_type='Text/HTML' GROUP BY title order by date_created DESC limit 0,"+limit+"";
            String SQL="select idlink,url,title,domain,(select user_id from web_bookmark where link_id=idlink and user_id='"+currUser+"')as userId,description,category,likes,image_url,vote_up,vote_down,likes,date_created,(select distinct isBookmarked from web_bookmark where link_id=idlink and user_id='"+currUser+"')as bookmarked,(select voted from link_votes where link_id=idlink and user_id='"+currUser +"')as voted,(select distinct no_of_likes from like_user where link_id=idlink)as liked,TIMEDIFF('"+date1+"',date_created)as hours from link where media_type='Text/HTML' and domain not IN('ibctamil.com','puthiyathalaimurai.com','tamil.samayam.com') GROUP BY title order by date_created DESC limit "+startLimit+","+limit+"";
            logger.info(SQL);
            logger.info(date1);
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            stmt = conn.createStatement();

            rs = stmt.executeQuery(SQL);

            while (rs.next()) {
                Link link = new Link();

                   link.setUrl(rs.getString("url"));
                    link.setTitle(rs.getString("title"));
                    link.setLinkId(rs.getInt("idlink"));
                    link.setBookmark(rs.getInt("bookmarked"));
                    link.setUserId(rs.getString("userId"));

                    //For Developement
                    Date date= sdf.parse(date1);
                    Date date2 = sdf.parse(rs.getString("date_created"));
                    // Calucalte time difference in milliseconds
                    long time_difference = date.getTime() - date2.getTime();

                    long hours_difference = (time_difference / (1000*60*60)) % 24;


                    link.setHoursElapsed(hours_difference);
                    //end of hours for dev

                    link.setLikes(rs.getInt("likes"));
                    //  link.setLiked(rs.getInt("liked"));
                    link.setDomain(rs.getString("domain"));
                    link.setDesc(rs.getString("description"));
                    link.setSubmitedBy("GaramGaram");
                    link.setImageUrl(rs.getString("image_url"));
                    link.setCategory(rs.getString("category"));
                    link.setDateCreated(rs.getTimestamp("date_created"));
                    link.setVote_up(rs.getInt("vote_up"));
                    link.setVote_down(rs.getInt("vote_down"));
                    //System.out.println("voted--->"+rs.getString("voted"));
                    if(rs.getString("voted")==null)
                    {
                        link.setUserVoted("0");
                    }
                    else {
                        link.setUserVoted(rs.getString("voted"));
                    }
                    //    System.out.println("Date -->"+link.getDateCreated());
                link.setSubmitedBy("GaramGaram");
                String cat=rs.getString("category");
                if(cat.equals("इंडिया")){
                    cat="india";
                }if(cat.equals("दुनिया")){
                    System.out.println("Inside of World Category");
                    cat="world";
                }
                if(cat.equals("खेल")){cat="sports";}
                if(cat.equals("सामान्य समाचार")){cat="general";}
                if(cat.equals("मनोरंजन")){cat="cinema";}
                link.setCategory2(cat);

                String newsSource="";

                if(rs.getString("domain").equals("hindi.oneindia.com"))
                {
                    newsSource="हिंदी वनइंडिया";
                }
                if(rs.getString("domain").equals("hindi.news18.com"))
                {
                    newsSource="हिंदी न्यूज़ 18";
                }
                if(rs.getString("domain").equals("livehindustan.com"))
                {
                    newsSource="लाइव हिंदुस्तान";
                }
                if(rs.getString("domain").equals("amarujala.com")) {
                    newsSource = "अमर उजाला";
                }
                if(rs.getString("domain").equals("hindi.filmibeat.com")) {
                    newsSource = "हिंदी फिल्मीबीट";
                }
                if(rs.getString("domain").equals("zeenews.india.com")) {
                    newsSource = "जी नेवस";
                }
                if(rs.getString("domain").equals("bbc.com")) {
                    newsSource = "बीबीसी";
                }
                if(rs.getString("domain").equals("naidunia.com")) {
                    newsSource = "नईदुनिया";
                }
                if(rs.getString("domain").equals("prabhasakshi.com")) {
                    newsSource = "प्रभासाक्षी";
                }
                if(rs.getString("domain").equals("dabangdunia.co")) {
                    newsSource = "दबंगदुनिया";
                }

                link.setDomain(newsSource);
                String dom=link.getDomain();
                if(dom.equals("हिंदी वनइंडिया")){ dom="hindi.oneindia.com"; }
                if(dom.equals("हिंदी न्यूज़ 18")){dom="hindi.news18.com";}
                if(dom.equals("लाइव हिंदुस्तान")){dom="livehindustan.com";}
                if(dom.equals("अमर उजाला")){dom="amarujala.com";}
                if(dom.equals("हिंदी फिल्मीबीट")){dom="hindi.filmibeat.com";}
                if(dom.equals("जी नेवस")){dom="zeenews.india.com";}
                if(dom.equals("बीबीसी")){dom="bbc.com";}
                if(dom.equals("नईदुनिया")){dom="naidunia.com";}
                if(dom.equals("प्रभासाक्षी")){dom="prabhasakshi.com";}
                if(dom.equals("दबंगदुनिया")){dom="dabangdunia.co";}
                link.setDomain2(dom);
                links.add(link);

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("success fetched....."+links.size());

        System.out.println("query end date is"+date1);
        return links;
    }

    public List<Link> getFeedLinks(int currUser, String language,
                                   String category, String country,String timestamp) {
        List<Link> links = new ArrayList<Link>();

        DataSource ds = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {

            // String SQL =
            // "select *,b.username from link a, user b where a.userId=b.idUser order by a.votes desc, a.date_created";
            String SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted from link a left join link_user c on a.idlink=c.linkId and c.userId="
                    + currUser + " order by a.votes desc, a.date_created";

            if (language == null || category == null || country == null)
                return links;

            // No filter scenario

            if (language.equalsIgnoreCase("all")
                    && category.equalsIgnoreCase("all")
                    && country.equalsIgnoreCase("all"))
                SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v,timestampdiff(HOUR, a.date_created, now()) as hours,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId=" + currUser + " ) as bookmark from link a left join link_user c on a.idlink=c.linkId and c.userId=" + currUser + " where spam < 4  and timestampdiff(HOUR, a.date_created, now()) <= 100 order by a.date_created desc limit 0,20;";

            // Language only filter

            if (!language.equalsIgnoreCase("all")
                    && category.equalsIgnoreCase("all")
                    && country.equalsIgnoreCase("all"))
                SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v,timestampdiff(HOUR, a.date_created, now()) as hours,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId=" + currUser + " ) as bookmark from link a left join link_user c on a.idlink=c.linkId and c.userId="
                        + currUser
                        + " where spam < 4 and language='"
                        + language
                        + "'  and timestampdiff(HOUR, a.date_created, now()) <= 100 order by a.date_created desc";

            // Category filter

            if (language.equalsIgnoreCase("all")
                    && !category.equalsIgnoreCase("all")
                    && country.equalsIgnoreCase("all"))
                SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v,timestampdiff(HOUR, a.date_created, now()) as hours,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId=" + currUser + " ) as bookmark from link a left join link_user c on a.idlink=c.linkId and c.userId="
                        + currUser
                        + " where spam < 4 and category='"
                        + category
                        + "' and timestampdiff(HOUR, a.date_created, now()) <= 100 order by a.date_created desc";

            // Country filter

            if (language.equalsIgnoreCase("all")
                    && category.equalsIgnoreCase("all")
                    && !country.equalsIgnoreCase("all"))
                SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v,timestampdiff(HOUR, a.date_created, now()) as hours,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId=" + currUser + " ) as bookmark from link a left join link_user c on a.idlink=c.linkId and c.userId="
                        + currUser
                        + " where spam < 4 and country='"
                        + country
                        + "'  and timestampdiff(HOUR, a.date_created, now()) <= 100 order by a.date_created desc";

            // Language, Category and Country filter

            if (!language.equalsIgnoreCase("all")
                    && !category.equalsIgnoreCase("all")
                    && !country.equalsIgnoreCase("all"))
                SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v,timestampdiff(HOUR, a.date_created, now()) as hours,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId=" + currUser + " ) as bookmark from link a left join link_user c on a.idlink=c.linkId and c.userId="
                        + currUser
                        + " where spam < 4 and category='"
                        + category
                        + "' and language='"
                        + language
                        + "' and country='"
                        + country
                        + "' and a.date_created >= '"+timestamp+"' order by a.date_created desc limit 0,20";

            // Category and Language filter

            if (!language.equalsIgnoreCase("all")
                    && !category.equalsIgnoreCase("all")
                    && country.equalsIgnoreCase("all"))
                SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v,timestampdiff(HOUR, a.date_created, now()) as hours,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId=" + currUser + " ) as bookmark from link a left join link_user c on a.idlink=c.linkId and c.userId="
                        + currUser
                        + " where spam < 4 and category='"
                        + category
                        + "' and language='"
                        + language
                        + "' and timestampdiff(HOUR, a.date_created, now()) <= 100 order by a.date_created desc";

            // Category and country filter

            if (language.equalsIgnoreCase("all")
                    && !category.equalsIgnoreCase("all")
                    && !country.equalsIgnoreCase("all"))
                SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v,timestampdiff(HOUR, a.date_created, now()) as hours,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId=" + currUser + " ) as bookmark from link a left join link_user c on a.idlink=c.linkId and c.userId="
                        + currUser
                        + " where spam < 4 and category='"
                        + category
                        + "' and country='"
                        + country
                        + "' and timestampdiff(HOUR, a.date_created, now()) <= 100 order by a.date_created desc";

            // Language and Country

            if (!language.equalsIgnoreCase("all")
                    && category.equalsIgnoreCase("all")
                    && !country.equalsIgnoreCase("all"))
                SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v,timestampdiff(HOUR, a.date_created, now()) as hours,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId=" + currUser + " ) as bookmark from link a left join link_user c on a.idlink=c.linkId and c.userId="
                        + currUser
                        + " where spam < 4 and language='"
                        + language
                        + "' and country='"
                        + country
                        + "' and timestampdiff(HOUR, a.date_created, now()) <= 100 order by a.date_created desc limit 0,20";

            logger.info(SQL);

            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            stmt = conn.createStatement();

            rs = stmt.executeQuery(SQL);

            while (rs.next()) {
                Link link = new Link();
                link.setUrl(rs.getString("url"));
                link.setTitle(rs.getString("title"));
                link.setLinkId(rs.getInt("idlink"));
                link.setVotes(rs.getInt("votes"));
                link.setSubmitedBy(rs.getString("username"));
                link.setVoted((rs.getString("voted") != null));
                link.setBookmarked((rs.getString("bookmark") != null));
                link.setHoursElapsed((rs.getLong("hours")));
                link.setNoOfComments(rs.getInt("comments"));
                link.setDomain(rs.getString("domain"));
                link.setDesc(rs.getString("description"));
                link.setCategory(rs.getString("category"));
                link.setLanguage(rs.getString("language"));
                link.setCountry(rs.getString("country"));
                link.setTags(rs.getString("tags"));
                link.setSpam(rs.getInt("spam"));
                link.setImageUrl(rs.getString("image_url"));
                link.setDateCreated(rs.getTimestamp("date_created"));
                //    System.out.println("Date -->"+link.getDateCreated());
                links.add(link);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return links;
    }


    public List<Link> getExpiredLinks(int currUser, String language,
                                      String category, String country) {
        List<Link> links = new ArrayList<Link>();

        DataSource ds = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {

            // String SQL =
            // "select *,b.username from link a, user b where a.userId=b.idUser order by a.votes desc, a.date_created";
            String SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username, (select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted from link a left join link_user c on a.idlink=c.linkId and c.userId="
                    + currUser + " order by a.votes desc, a.date_created";

            if (language == null || category == null || country == null)
                return links;

            // No filter return everything

            if (language.equalsIgnoreCase("all")
                    && category.equalsIgnoreCase("all")
                    && country.equalsIgnoreCase("all"))
                SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v,timestampdiff(HOUR, a.date_created, now()) as hours from link a left join link_user c on a.idlink=c.linkId and c.userId="
                        + currUser
                        + " where spam < 4 and timestampdiff(HOUR, a.date_created, now()) > 100000 order by a.date_created desc limit 0,25";

            // Filter by language

            if (!language.equalsIgnoreCase("all")
                    && category.equalsIgnoreCase("all")
                    && country.equalsIgnoreCase("all"))
                SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v,timestampdiff(HOUR, a.date_created, now()) as hours from link a left join link_user c on a.idlink=c.linkId and c.userId="
                        + currUser
                        + " where language='"
                        + language
                        + "' and spam < 4 and timestampdiff(HOUR, a.date_created, now()) > 100000 order by a.date_created desc limit 0,25";

            // Filter by category

            if (language.equalsIgnoreCase("all")
                    && !category.equalsIgnoreCase("all")
                    && country.equalsIgnoreCase("all"))
                SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v,timestampdiff(HOUR, a.date_created, now()) as hours from link a left join link_user c on a.idlink=c.linkId and c.userId="
                        + currUser
                        + " where category='"
                        + category
                        + "' and spam < 4 and timestampdiff(HOUR, a.date_created, now()) > 100000 order by a.date_created desc limit 0,25";

            // Filter by country

            if (language.equalsIgnoreCase("all")
                    && category.equalsIgnoreCase("all")
                    && !country.equalsIgnoreCase("all"))
                SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v,timestampdiff(HOUR, a.date_created, now()) as hours from link a left join link_user c on a.idlink=c.linkId and c.userId="
                        + currUser
                        + " where country='"
                        + country
                        + "' and spam < 4 and timestampdiff(HOUR, a.date_created, now()) > 100000 order by a.date_created desc limit 0,25";

            // Filter by country and language

            if (!language.equalsIgnoreCase("all")
                    && category.equalsIgnoreCase("all")
                    && !country.equalsIgnoreCase("all"))
                SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v,timestampdiff(HOUR, a.date_created, now()) as hours from link a left join link_user c on a.idlink=c.linkId and c.userId="
                        + currUser
                        + " where country='"
                        + country
                        + "' and language='"
                        + language
                        + "' and spam < 4 and timestampdiff(HOUR, a.date_created, now()) > 100000 order by a.date_created desc limit 0,25";

            // Filter by country and category

            if (language.equalsIgnoreCase("all")
                    && !category.equalsIgnoreCase("all")
                    && !country.equalsIgnoreCase("all"))
                SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v,timestampdiff(HOUR, a.date_created, now()) as hours from link a left join link_user c on a.idlink=c.linkId and c.userId="
                        + currUser
                        + " where country='"
                        + country
                        + "' and category='"
                        + category
                        + "' and spam < 4 and timestampdiff(HOUR, a.date_created, now()) > 100000 order by a.date_created desc limit 0,25";

            // Filter by language and category

            if (!language.equalsIgnoreCase("all")
                    && !category.equalsIgnoreCase("all")
                    && country.equalsIgnoreCase("all"))
                SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v,timestampdiff(HOUR, a.date_created, now()) as hours from link a left join link_user c on a.idlink=c.linkId and c.userId="
                        + currUser
                        + " where language='"
                        + language
                        + "' and category='"
                        + category
                        + "' and spam < 4 and timestampdiff(HOUR, a.date_created, now()) > 100000 order by a.date_created desc limit 0,25";

            // Filter by language, category and country

            if (!language.equalsIgnoreCase("all")
                    && !category.equalsIgnoreCase("all")
                    && !category.equalsIgnoreCase("all"))
                SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v,timestampdiff(HOUR, a.date_created, now()) as hours from link a left join link_user c on a.idlink=c.linkId and c.userId="
                        + currUser
                        + " where language='"
                        + language
                        + "' and category='"
                        + category
                        + "' and country='"
                        + country
                        + "' and spam < 4 and timestampdiff(HOUR, a.date_created, now()) > 100000 order by a.date_created desc limit 0,25";

            logger.info("SQL:" + SQL);

            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            stmt = conn.createStatement();

            rs = stmt.executeQuery(SQL);

            while (rs.next()) {
                Link link = new Link();
                link.setUrl(rs.getString("url"));
                link.setTitle(rs.getString("title"));
                link.setLinkId(rs.getInt("idlink"));
                link.setVotes(rs.getInt("votes"));
                link.setSubmitedBy(rs.getString("username"));
                link.setVoted((rs.getString("voted") != null));
                link.setBookmarked((rs.getString("bookmarked") != null));
                link.setHoursElapsed((rs.getLong("hours")));
                link.setNoOfComments(rs.getInt("comments"));
                link.setDomain(rs.getString("domain"));
                link.setCategory(rs.getString("category"));
                link.setLanguage(rs.getString("language"));
                link.setCountry(rs.getString("country"));
                link.setImageUrl(rs.getString("image_url"));
                links.add(link);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return links;
    }

    public Link getLink(int linkId) {




        Link link = new Link();

        DataSource ds = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String SQL = "select (select username from user where idUser=a.userId) as submittedBy, timestampdiff(HOUR, a.date_created, now()) as hours, (select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags,a.* from link a where a.idlink="
                    + linkId;

            rs = stmt.executeQuery(SQL);

            while (rs.next()) {
                link.setUrl(rs.getString("url"));
                link.setTitle(rs.getString("title"));
                link.setLinkId(rs.getInt("idlink"));
                link.setVotes(rs.getInt("votes"));
                link.setNoOfComments(rs.getInt("comments"));
                link.setDomain(rs.getString("domain"));
                link.setCategory(rs.getString("category"));
                link.setLanguage(rs.getString("language"));
                link.setCountry(rs.getString("country"));
                link.setTags(rs.getString("tags"));
                link.setSpam(rs.getInt("spam"));
                link.setHoursElapsed(rs.getLong("hours"));
                link.setSubmitedBy(rs.getString("submittedBy"));
                link.setImageUrl(rs.getString("image_url"));
                link.setDesc(rs.getString("description"));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return link;
    }

    public List<Link> getNews_Detail(String linkId,int currUser) {

        List<Link> linksList = new ArrayList<Link>();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TimeZone istTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
        java.util.Date d = new java.util.Date();
        sdf.setTimeZone(istTimeZone);
        String date1 = sdf.format(d);


        DataSource ds = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String SQL = "select idlink,url,title,domain,description,category,(select voted from link_votes where link_id=idlink and user_id='"+currUser +"')as voted,vote_up,vote_down,comments,image_url,likes,date_created,TIMEDIFF('"+date1+"',date_created)as hours from link where idlink='"+linkId+"' ";

            rs = stmt.executeQuery(SQL);

            while (rs.next()) {
                Link link = new Link();
                link.setUrl(rs.getString("url"));
                link.setTitle(rs.getString("title"));
                link.setLinkId(rs.getInt("idlink"));
                //  link.setVotes(rs.getInt("votes"));
                link.setNoOfComments(rs.getInt("comments"));
                link.setDomain(rs.getString("domain"));
                link.setCategory(rs.getString("category"));
                // link.setLanguage(rs.getString("language"));
                //  link.setCountry(rs.getString("country"));
                link.setVote_up(rs.getInt("vote_up"));
                link.setVote_down(rs.getInt("vote_down"));
                //System.out.println("voted--->"+rs.getString("voted"));
                if(rs.getString("voted")==null)
                {
                    link.setUserVoted("0");
                }
                else {
                    link.setUserVoted(rs.getString("voted"));
                }

               // link.setHoursElapsed(rs.getLong("hours"));
                // link.setSubmitedBy(rs.getString("submittedBy"));
                Date date= sdf.parse(date1);
                Date date2 = sdf.parse(rs.getString("date_created"));
                // Calucalte time difference in milliseconds
                long time_difference = date.getTime() - date2.getTime();

                long hours_difference = (time_difference / (1000*60*60)) % 24;


                link.setHoursElapsed(hours_difference);

                link.setImageUrl(rs.getString("image_url"));
                link.setDesc(rs.getString("description")+"..");



                String newsSource="";

                if(rs.getString("domain").equals("hindi.oneindia.com"))
                {
                    newsSource="हिंदी वनइंडिया";
                }
                if(rs.getString("domain").equals("hindi.news18.com"))
                {
                    newsSource="हिंदी न्यूज़ 18";
                }
                if(rs.getString("domain").equals("livehindustan.com"))
                {
                    newsSource="लाइव हिंदुस्तान";
                }
                if(rs.getString("domain").equals("amarujala.com")) {
                    newsSource = "अमर उजाला";
                }
                if(rs.getString("domain").equals("hindi.filmibeat.com")) {
                    newsSource = "हिंदी फिल्मीबीट";
                }
                if(rs.getString("domain").equals("zeenews.india.com")) {
                    newsSource = "जी नेवस";
                }
                if(rs.getString("domain").equals("bbc.com")) {
                    newsSource = "बीबीसी";
                }
                if(rs.getString("domain").equals("naidunia.com")) {
                    newsSource = "नईदुनिया";
                }
                if(rs.getString("domain").equals("prabhasakshi.com")) {
                    newsSource = "प्रभासाक्षी";
                }
                if(rs.getString("domain").equals("dabangdunia.co")) {
                    newsSource = "दबंगदुनिया";
                }

                link.setDomain(newsSource);
                linksList.add(link);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return linksList;
    }

    public void voteUpLink(int linkid, int userid) {
        DataSource ds = null;
        Connection conn = null;
        PreparedStatement pstmt1 = null;
        PreparedStatement pstmt2 = null;
        ResultSet rs = null;

        try {

            ds = DBConnection.getDataSource();
            conn = ds.getConnection();

            conn.setAutoCommit(false);

            String SQL1 = "update link set votes = votes+1 where idlink= ?";
            String SQL2 = "insert into link_user values (0,?,?,now())";

            logger.info("SQL:" + SQL1);
            logger.info("SQL:" + SQL2);

            if (!isVotedAlready(userid, linkid)) {
                pstmt1 = conn.prepareStatement(SQL1);

                pstmt1.setInt(1, linkid);
                pstmt1.executeUpdate();

                pstmt2 = conn.prepareStatement(SQL2);
                pstmt2.setInt(1, linkid);
                pstmt2.setInt(2, userid);
                pstmt2.executeUpdate();

                conn.commit();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (pstmt1 != null)
                    pstmt1.close();
                if (pstmt2 != null)
                    pstmt2.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public void addBookmark(int linkid, int userid) {
        DataSource ds = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            conn.setAutoCommit(false);
            String SQL1 = "insert into bookmark values (0,?,?,now())";
            logger.info("SQL:" + SQL1);
            if (!isBookmarkedAlready(userid, linkid)) {
                pstmt = conn.prepareStatement(SQL1);
                pstmt.setInt(1, linkid);
                pstmt.setInt(2, userid);
                pstmt.executeUpdate();
                conn.commit();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (pstmt != null)
                    pstmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public boolean isBookmarkedAlready(int userId, int linkId) {
        DataSource ds = null;
        Connection conn = null;
        Statement stmt = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String SQL = "select * from bookmark where userId=" + userId
                    + " and linkId=" + linkId;
            rs = stmt.executeQuery(SQL);

            if (rs.next())
                return true;

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    public void removeBookmark(int userId, int linkId) {
        DataSource ds = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            conn.setAutoCommit(false);
            String SQL = "delete  from bookmark where userId=" + userId
                    + " and linkId=" + linkId;
            pstmt = conn.prepareStatement(SQL);
            pstmt.executeUpdate();
            conn.commit();
            logger.info("SQL:" + SQL);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (pstmt != null)
                    pstmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }


    public boolean isVotedAlready(int userId, int linkId) {
        DataSource ds = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String SQL = "select * from link_user where userId=" + userId
                    + " and linkId=" + linkId;
            rs = stmt.executeQuery(SQL);

            if (rs.next())
                return true;

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    public boolean isLinkPresent(String url) {
        DataSource ds = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String SQL = "select * from link where url='" + url + "'";
            rs = stmt.executeQuery(SQL);

            if (rs.next())
                return true;

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    public int isTagPresent(String tagName) {
        DataSource ds = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String SQL = "select * from tag where tag_name='" + tagName + "'";
            rs = stmt.executeQuery(SQL);

            if (rs.next())
                return rs.getInt("tag_id");

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return -1;
    }

    public Link getLink(String url) {
        Link link = new Link();

        DataSource ds = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String SQL = "select * from link where url='" + url + "'";
            rs = stmt.executeQuery(SQL);

            if (rs.next()) {
                link.setUrl(rs.getString("url"));
                link.setTitle(rs.getString("title"));
                link.setLinkId(rs.getInt("idlink"));
                link.setVotes(rs.getInt("votes"));
                link.setNoOfComments(rs.getInt("comments"));
                link.setDomain(rs.getString("domain"));
                link.setLanguage(rs.getString("language"));
                link.setCategory(rs.getString("category"));
                link.setCountry(rs.getString("country"));
                link.setImageUrl(rs.getString("image_url"));
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return link;
    }

    public List<String> getRecentlySubmittedUsers() {
        List<String> usernames = new ArrayList<String>();

        DataSource ds = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String SQL = "select distinct * from  ( select  (select username from user u where u.iduser=l.userid) as username from link l  order by l.date_created desc  ) as b limit 0,5 ";
            rs = stmt.executeQuery(SQL);

            while (rs.next()) {
                usernames.add(rs.getString("username"));
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return usernames;
    }

    public List<Link> getSubmittedByLinks(int submittedBy) {
        List<Link> links = new ArrayList<Link>();

        DataSource ds = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {

            ds = DBConnection.getDataSource();
            conn = ds.getConnection();

            String SQL = "select *, (select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=idlink and t.tag_id=lt.tag_id) as tags,timestampdiff(HOUR, date_created, now()) as hours from link where userId=?"
                    + " order by date_created desc limit 0,25";
            logger.info("Submitted by SQL:" + SQL);

            pstmt = conn.prepareStatement(SQL);

            pstmt.setInt(1, submittedBy);

            rs = pstmt.executeQuery();

            while (rs.next()) {
                Link link = new Link();
                link.setUrl(rs.getString("url"));
                link.setTitle(rs.getString("title"));
                link.setLinkId(rs.getInt("idlink"));
                link.setVotes(rs.getInt("votes"));
                // link.setSubmitedBy(rs.getString("username"));
                // link.setVoted((rs.getString("voted")!=null));
                link.setHoursElapsed((rs.getLong("hours")));
                link.setDomain(rs.getString("domain"));
                link.setCategory(rs.getString("category"));
                link.setLanguage(rs.getString("language"));
                link.setCountry(rs.getString("country"));
                link.setNoOfComments(rs.getInt("comments"));
                link.setTags(rs.getString("tags"));
                link.setSpam(rs.getInt("spam"));
                link.setImageUrl(rs.getString("image_url"));
                links.add(link);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (pstmt != null)
                    pstmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return links;
    }

    public List<NameCountVO> getCategoryLinkCount(int userId, int type, String cond) {
        List<NameCountVO> links = new ArrayList<NameCountVO>();

        DataSource ds = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {

            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            stmt = conn.createStatement();

            // by tag:
            // select t.tag_name,count(t.tag_name) as cnt from link_tag lt, tag
            // t, link l where l.idlink=lt. and lt.tag_id=t.tag_id group
            // by t.tag_name order by cnt desc limit 0,5,

            String SQL = "select concat(category, ' (', group_concat(distinct l.country separator ','),')' ) as category, count(distinct l.idlink) as count from link l, link_user lu where l.idlink=lu.linkId and lu.userId="
                    + userId + " and spam < 4 group by category";

            if (userId > 0 && type == 1)
                SQL = "select concat(domain,' (', group_concat(distinct language separator ','),')' ) as domain, count(*) as count from link l, link_user lu  where l.idlink=lu.linkId and lu.userId=" + userId + " and spam < 4  group by domain order by count desc limit 0,10";

            if (userId > 0 && type == 3)
                SQL = "select category, count(*) as count from link l, link_user lu where l.idlink=lu.linkId and lu.userId=" + userId + " and domain='" + cond + "' and spam < 4 group by category order by count desc";

            // For homepage userId will be -1

            if (userId == -1)
                SQL = "select category, count(*) from link where spam < 4 and timestampdiff(HOUR, date_created, now()) < 500 group by category";

            // For breakdown by language (homepage)

            if (userId == -2)
                SQL = "select language, count(*) from link where spam < 4 and timestampdiff(HOUR, date_created, now()) < 500 group by language";

            // Breakdown by country for homepage

            if (userId == -3)
                SQL = "select country, count(*) from link where spam < 4 and timestampdiff(HOUR, date_created, now()) < 500 group by country";

            if (userId == -4)
                SQL = "select domain, count(*) from link where spam < 4 and timestampdiff(HOUR, date_created, now()) < 500 group by domain";

            logger.info("SQL:" + SQL);
            System.out.println("SQL-->"+SQL);

            rs = stmt.executeQuery(SQL);

            while (rs.next()) {
                NameCountVO countVO = new NameCountVO();
                countVO.setCategory(rs.getString(1)); // category column
                countVO.setCount(rs.getInt(2));
                links.add(countVO);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return links;
    }

    public List<Link> getDomainLinks(String domain) {

        List<Link> links = new ArrayList<Link>();

        DataSource ds = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();

            String SQL = "select (select username from user where user.idUser=l.userId) as username, (select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=l.idlink and t.tag_id=lt.tag_id) as tags,l.*,timestampdiff(HOUR, l.date_created, now()) as hours from link l where domain = ?"
                    + " order by date_created desc limit 0,25";
            logger.info("Domain Links SQL: " + SQL);

            pstmt = conn.prepareStatement(SQL);
            pstmt.setString(1, domain);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                Link link = new Link();
                link.setUrl(rs.getString("url"));
                link.setTitle(rs.getString("title"));
                link.setLinkId(rs.getInt("idlink"));
                link.setVotes(rs.getInt("votes"));
                link.setSubmitedBy(rs.getString("username"));
                link.setHoursElapsed((rs.getLong("hours")));
                link.setDomain(rs.getString("domain"));
                link.setCategory(rs.getString("category"));
                link.setLanguage(rs.getString("language"));
                link.setNoOfComments(rs.getInt("comments"));
                link.setCountry(rs.getString("country"));
                link.setTags(rs.getString("tags"));
                link.setSpam(rs.getInt("spam"));
                link.setImageUrl(rs.getString("image_url"));
                links.add(link);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (pstmt != null)
                    pstmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return links;

    }

    //getTagLinks
    public List<Link> getTagLinks(String tag) {

        List<Link> links = new ArrayList<Link>();

        DataSource ds = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();

            String SQL = "select (select username from user where user.idUser=l.userId) as username,(select group_concat(t1.tag_name separator ',') from tag t1, link_tag lt1 where lt1.link_id=l.idlink and t1.tag_id=lt1.tag_id) as tags,l.*,timestampdiff(HOUR, l.date_created, now()) as hours from link l, tag t where t.tag_name=? and l.idlink in (select link_id from link_tag lt where lt.tag_id=t.tag_id)";
            logger.info("Tag Links SQL: " + SQL);

            pstmt = conn.prepareStatement(SQL);
            pstmt.setString(1, tag);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                Link link = new Link();
                link.setUrl(rs.getString("url"));
                link.setTitle(rs.getString("title"));
                link.setLinkId(rs.getInt("idlink"));
                link.setVotes(rs.getInt("votes"));
                link.setSubmitedBy(rs.getString("username"));
                link.setHoursElapsed((rs.getLong("hours")));
                link.setDomain(rs.getString("domain"));
                link.setCategory(rs.getString("category"));
                link.setLanguage(rs.getString("language"));
                link.setNoOfComments(rs.getInt("comments"));
                link.setCountry(rs.getString("country"));
                link.setTags(rs.getString("tags"));
                link.setSpam(rs.getInt("spam"));
                link.setImageUrl(rs.getString("image_url"));
                links.add(link);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (pstmt != null)
                    pstmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return links;

    }

    @SuppressWarnings("resource")
    public List<Link> getMyLinks(int userId, String lang, String category,
                                 String country) {

        List<Link> links = new ArrayList<Link>();

        DataSource ds = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();

            String SQL = "select distinct (select username from user where idUser=l.userId) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=l.idlink and t.tag_id=lt.tag_id) as tags,l.*,timestampdiff(HOUR, l.date_created, now()) as hours from link l, link_user lu where lu.userId=? and l.idlink=lu.linkId order by l.date_created desc limit 0,20";
            logger.info("My Links SQL: " + SQL);

            if (lang == null || category == null || country == null)
                return links;

            if (lang.equalsIgnoreCase("all")
                    && category.equalsIgnoreCase("all")
                    && country.equalsIgnoreCase("all")) {
                SQL = "select distinct (select username from user where idUser=l.userId) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=l.idlink and t.tag_id=lt.tag_id) as tags,l.*,timestampdiff(HOUR, l.date_created, now()) as hours from link l, link_user lu where lu.userId=? and l.idlink=lu.linkId order by l.date_created desc limit 0,20";
                pstmt = conn.prepareStatement(SQL);
                pstmt.setInt(1, userId);

            }
            // Filter by Language

            if (!lang.equalsIgnoreCase("all")
                    && category.equalsIgnoreCase("all")
                    && country.equalsIgnoreCase("all")) {
                SQL = "select distinct (select username from user where idUser=l.userId) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=l.idlink and t.tag_id=lt.tag_id) as tags,l.*,timestampdiff(HOUR, l.date_created, now()) as hours from link l, link_user lu where lu.userId=? and lu.linkId=l.idlink and l.language=? order by l.date_created desc limit 0,20";
                pstmt = conn.prepareStatement(SQL);
                pstmt.setInt(1, userId);
                pstmt.setString(2, lang);
            }

            // Filter by category

            if (lang.equalsIgnoreCase("all") && country.equalsIgnoreCase("all")
                    && !category.equalsIgnoreCase("all")) {
                SQL = "select distinct (select username from user where idUser=l.userId) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=l.idlink and t.tag_id=lt.tag_id) as tags,l.*,timestampdiff(HOUR, l.date_created, now()) as hours from link l, link_user lu where lu.userId=? and lu.linkId=l.idlink and l.category=? order by l.date_created desc limit 0,20";
                pstmt = conn.prepareStatement(SQL);
                pstmt.setInt(1, userId);
                pstmt.setString(2, category);

            }

            // Filter by country

            if (lang.equalsIgnoreCase("all")
                    && !country.equalsIgnoreCase("all")
                    && category.equalsIgnoreCase("all")) {
                SQL = "select distinct (select username from user where idUser=l.userId) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=l.idlink and t.tag_id=lt.tag_id) as tags,l.*,timestampdiff(HOUR, l.date_created, now()) as hours from link l, link_user lu where lu.userId=? and lu.linkId=l.idlink and l.country=? order by l.date_created desc limit 0,20";
                pstmt = conn.prepareStatement(SQL);
                pstmt.setInt(1, userId);
                pstmt.setString(2, country);

            }

            // Filter by language and category

            if (!lang.equalsIgnoreCase("all")
                    && !category.equalsIgnoreCase("all")
                    && country.equalsIgnoreCase("all")) {
                SQL = "select distinct (select username from user where idUser=l.userId) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=l.idlink and t.tag_id=lt.tag_id) as tags,l.*,timestampdiff(HOUR, l.date_created, now()) as hours from link l, link_user lu where lu.userId=? and lu.linkId=l.idlink and l.language=? and l.category=? order by l.date_created desc limit 0,20";
                pstmt = conn.prepareStatement(SQL);
                pstmt.setInt(1, userId);
                pstmt.setString(2, lang);
                pstmt.setString(3, category);
            }

            // Filter by language and country

            if (!lang.equalsIgnoreCase("all")
                    && category.equalsIgnoreCase("all")
                    && !country.equalsIgnoreCase("all")) {
                SQL = "select distinct (select username from user where idUser=l.userId) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=l.idlink and t.tag_id=lt.tag_id) as tags,l.*,timestampdiff(HOUR, l.date_created, now()) as hours from link l, link_user lu where lu.userId=? and lu.linkId=l.idlink and l.language=? and l.country=? order by l.date_created desc limit 0,20";
                pstmt = conn.prepareStatement(SQL);
                pstmt.setInt(1, userId);
                pstmt.setString(2, lang);
                pstmt.setString(3, country);
            }

            // Filter by category and country

            if (lang.equalsIgnoreCase("all")
                    && !category.equalsIgnoreCase("all")
                    && !country.equalsIgnoreCase("all")) {
                SQL = "select distinct (select username from user where idUser=l.userId) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=l.idlink and t.tag_id=lt.tag_id) as tags,l.*,timestampdiff(HOUR, l.date_created, now()) as hours from link l, link_user lu where lu.userId=? and lu.linkId=l.idlink and l.category=? and l.country=? order by l.date_created desc limit 0,20";
                pstmt = conn.prepareStatement(SQL);
                pstmt.setInt(1, userId);
                pstmt.setString(2, category);
                pstmt.setString(3, country);
            }

            // Filter by language, category and country

            if (!lang.equalsIgnoreCase("all")
                    && !category.equalsIgnoreCase("all")
                    && !country.equalsIgnoreCase("all")) {
                SQL = "select distinct (select username from user where idUser=l.userId) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=l.idlink and t.tag_id=lt.tag_id) as tags,l.*,timestampdiff(HOUR, l.date_created, now()) as hours from link l, link_user lu where lu.userId=? and lu.linkId=l.idlink and l.languge=? and l.category=? and l.country=? order by l.date_created desc limit 0,20";
                pstmt = conn.prepareStatement(SQL);
                pstmt.setInt(1, userId);
                pstmt.setString(2, lang);
                pstmt.setString(3, category);
                pstmt.setString(4, country);
            }

            rs = pstmt.executeQuery();

            while (rs.next()) {
                Link link = new Link();
                link.setUrl(rs.getString("url"));
                link.setTitle(rs.getString("title"));
                link.setLinkId(rs.getInt("idlink"));
                link.setVotes(rs.getInt("votes"));
                link.setSubmitedBy(rs.getString("username"));
                link.setHoursElapsed((rs.getLong("hours")));
                link.setDomain(rs.getString("domain"));
                link.setCategory(rs.getString("category"));
                link.setLanguage(rs.getString("language"));
                link.setCountry(rs.getString("country"));
                link.setTags(rs.getString("tags"));
                link.setSpam(rs.getInt("spam"));
                link.setVoted(true);
                link.setImageUrl(rs.getString("image_url"));
                links.add(link);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (pstmt != null)
                    pstmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return links;

    }

    public List<Link> getMyBookmarks(int userId, String lang, String category,
                                     String country) {

        List<Link> links = new ArrayList<Link>();

        DataSource ds = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();

            String SQL = "select distinct (select username from user where idUser=l.userId) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=l.idlink and t.tag_id=lt.tag_id) as tags,l.*,timestampdiff(HOUR, l.date_created, now()) as hours from link l, bookmark lu where lu.userId=? and l.idlink=lu.linkId order by l.date_created desc limit 0,20";
            logger.info("My Bookmarks SQL: " + SQL);

            if (lang == null || category == null || country == null)
                return links;

            if (lang.equalsIgnoreCase("all")
                    && category.equalsIgnoreCase("all")
                    && country.equalsIgnoreCase("all")) {
                SQL = "select distinct (select username from user where idUser=l.userId) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=l.idlink and t.tag_id=lt.tag_id) as tags,l.*,timestampdiff(HOUR, l.date_created, now()) as hours from link l, bookmark lu where lu.userId=? and l.idlink=lu.linkId order by l.date_created desc limit 0,20";
                pstmt = conn.prepareStatement(SQL);
                pstmt.setInt(1, userId);

            }
            // Filter by Language

            if (!lang.equalsIgnoreCase("all")
                    && category.equalsIgnoreCase("all")
                    && country.equalsIgnoreCase("all")) {
                SQL = "select distinct (select username from user where idUser=l.userId) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=l.idlink and t.tag_id=lt.tag_id) as tags,l.*,timestampdiff(HOUR, l.date_created, now()) as hours from link l, bookmark lu where lu.userId=? and lu.linkId=l.idlink and l.language=? order by l.date_created desc limit 0,20";
                pstmt = conn.prepareStatement(SQL);
                pstmt.setInt(1, userId);
                pstmt.setString(2, lang);
            }

            // Filter by category

            if (lang.equalsIgnoreCase("all") && country.equalsIgnoreCase("all")
                    && !category.equalsIgnoreCase("all")) {
                SQL = "select distinct (select username from user where idUser=l.userId) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=l.idlink and t.tag_id=lt.tag_id) as tags,l.*,timestampdiff(HOUR, l.date_created, now()) as hours from link l, link_user lu where lu.userId=? and lu.linkId=l.idlink and l.category=? order by l.date_created desc limit 0,20";
                pstmt = conn.prepareStatement(SQL);
                pstmt.setInt(1, userId);
                pstmt.setString(2, category);

            }

            // Filter by country

            if (lang.equalsIgnoreCase("all")
                    && !country.equalsIgnoreCase("all")
                    && category.equalsIgnoreCase("all")) {
                SQL = "select distinct (select username from user where idUser=l.userId) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=l.idlink and t.tag_id=lt.tag_id) as tags,l.*,timestampdiff(HOUR, l.date_created, now()) as hours from link l, bookmark lu where lu.userId=? and lu.linkId=l.idlink and l.country=? order by l.date_created desc limit 0,20";
                pstmt = conn.prepareStatement(SQL);
                pstmt.setInt(1, userId);
                pstmt.setString(2, country);

            }

            // Filter by language and category

            if (!lang.equalsIgnoreCase("all")
                    && !category.equalsIgnoreCase("all")
                    && country.equalsIgnoreCase("all")) {
                SQL = "select distinct (select username from user where idUser=l.userId) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=l.idlink and t.tag_id=lt.tag_id) as tags,l.*,timestampdiff(HOUR, l.date_created, now()) as hours from link l, bookmark lu where lu.userId=? and lu.linkId=l.idlink and l.language=? and l.category=? order by l.date_created desc limit 0,20";
                pstmt = conn.prepareStatement(SQL);
                pstmt.setInt(1, userId);
                pstmt.setString(2, lang);
                pstmt.setString(3, category);
            }

            // Filter by language and country

            if (!lang.equalsIgnoreCase("all")
                    && category.equalsIgnoreCase("all")
                    && !country.equalsIgnoreCase("all")) {
                SQL = "select distinct (select username from user where idUser=l.userId) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=l.idlink and t.tag_id=lt.tag_id) as tags,l.*,timestampdiff(HOUR, l.date_created, now()) as hours from link l, bookmark lu where lu.userId=? and lu.linkId=l.idlink and l.language=? and l.country=? order by l.date_created desc limit 0,20";
                pstmt = conn.prepareStatement(SQL);
                pstmt.setInt(1, userId);
                pstmt.setString(2, lang);
                pstmt.setString(3, country);
            }

            // Filter by category and country

            if (lang.equalsIgnoreCase("all")
                    && !category.equalsIgnoreCase("all")
                    && !country.equalsIgnoreCase("all")) {
                SQL = "select distinct (select username from user where idUser=l.userId) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=l.idlink and t.tag_id=lt.tag_id) as tags,l.*,timestampdiff(HOUR, l.date_created, now()) as hours from link l, bookmark lu where lu.userId=? and lu.linkId=l.idlink and l.category=? and l.country=? order by l.date_created desc limit 0,20";
                pstmt = conn.prepareStatement(SQL);
                pstmt.setInt(1, userId);
                pstmt.setString(2, category);
                pstmt.setString(3, country);
            }

            // Filter by language, category and country

            if (!lang.equalsIgnoreCase("all")
                    && !category.equalsIgnoreCase("all")
                    && !country.equalsIgnoreCase("all")) {
                SQL = "select distinct (select username from user where idUser=l.userId) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=l.idlink and t.tag_id=lt.tag_id) as tags,l.*,timestampdiff(HOUR, l.date_created, now()) as hours from link l, bookmark lu where lu.userId=? and lu.linkId=l.idlink and l.languge=? and l.category=? and l.country=? order by l.date_created desc limit 0,20";
                pstmt = conn.prepareStatement(SQL);
                pstmt.setInt(1, userId);
                pstmt.setString(2, lang);
                pstmt.setString(3, category);
                pstmt.setString(4, country);
            }

            rs = pstmt.executeQuery();

            while (rs.next()) {
                Link link = new Link();
                link.setUrl(rs.getString("url"));
                link.setTitle(rs.getString("title"));
                link.setLinkId(rs.getInt("idlink"));
                link.setVotes(rs.getInt("votes"));
                link.setSubmitedBy(rs.getString("username"));
                link.setHoursElapsed((rs.getLong("hours")));
                link.setDomain(rs.getString("domain"));
                link.setCategory(rs.getString("category"));
                link.setLanguage(rs.getString("language"));
                link.setCountry(rs.getString("country"));
                link.setTags(rs.getString("tags"));
                link.setSpam(rs.getInt("spam"));
                link.setBookmarked(true);
                link.setVoted(true);
                link.setImageUrl(rs.getString("image_url"));
                links.add(link);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (pstmt != null)
                    pstmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return links;

    }

    public List<NameCountVO> getMyTags(int userId) {

        List<NameCountVO> tags = new ArrayList<NameCountVO>();

        DataSource ds = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();

            String SQL = "select t.*, count(lt.tag_id) as tag_count from link_tag lt, link_user lu, tag t where lt.link_id = lu.linkId and t.tag_id=lt.tag_id and lu.userId=? group by lt.tag_id order by tag_count desc";
            logger.info("My Links SQL: " + SQL);

            pstmt = conn.prepareStatement(SQL);
            pstmt.setInt(1, userId);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                NameCountVO countVO = new NameCountVO();
                countVO.setCategory(rs.getString("tag_name"));
                countVO.setCount(rs.getInt("tag_count"));
                tags.add(countVO);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (pstmt != null)
                    pstmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return tags;

    }

    public boolean isSpam(int linkId, int userId) {
        boolean spam = false;

        DataSource ds = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String SQL = "select * from spam where linkId=" + linkId
                    + " and userId=" + userId;
            rs = stmt.executeQuery(SQL);
            if (rs.next())
                return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return spam;

    }

    public void addSpam(int linkId, User user) {
        DataSource ds = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            if (!isSpam(linkId, user.getUserId())) {
                ds = DBConnection.getDataSource();
                conn = ds.getConnection();
                stmt = conn.createStatement();

                String SQL = "insert into spam values(" + user.getUserId()
                        + "," + linkId + ",'" + user.getUserName() + "')";
                String SQL1 = "update link set spam=spam+1 where idlink="
                        + linkId;
                stmt.addBatch(SQL);
                stmt.addBatch(SQL1);
                stmt.executeBatch();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void applyTag(String tagName, int linkId) {
        DataSource ds = null;
        Connection conn = null;
        PreparedStatement pstmt1 = null;
        PreparedStatement pstmt2 = null;
        PreparedStatement pstmt3 = null;
        ResultSet rs = null;
        ResultSet rs3 = null;

        if (tagName == null || tagName.equalsIgnoreCase("null")
                || tagName.trim().length() == 0)
            return;

        try {

            ds = DBConnection.getDataSource();
            conn = ds.getConnection();

            conn.setAutoCommit(false);

            String SQL1 = "insert into tag values(0,?)";
            String SQL2 = "insert into link_tag values (0,?,?)";
            String SQL3 = "select * from tag where tag_name = ?";

            logger.info("SQL1:" + SQL1);
            logger.info("SQL2:" + SQL2);
            logger.info("SQL3:" + SQL3);

            int tagId = isTagPresent(tagName);

            if (tagId == -1) {
                pstmt1 = conn.prepareStatement(SQL1);
                pstmt1.setString(1, tagName);
                pstmt1.executeUpdate();

                pstmt3 = conn.prepareStatement(SQL3);
                pstmt3.setString(1, tagName);
                rs3 = pstmt3.executeQuery();

                if (rs3.next()) {
                    tagId = rs3.getInt("tag_id");
                }

            }

            if (tagId != -1) {
                pstmt2 = conn.prepareStatement(SQL2);
                pstmt2.setInt(1, linkId);
                pstmt2.setInt(2, tagId);
                pstmt2.executeUpdate();
            }
            conn.commit();

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (pstmt1 != null)
                    pstmt1.close();
                if (pstmt2 != null)
                    pstmt2.close();
                if (pstmt3 != null)
                    pstmt3.close();
                if (rs3 != null)
                    rs3.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }


    public List<Link> getCategoryNews(String category,int descriptionLength,int currUser,String start,String limit) {


        List<Link> links = new ArrayList<Link>();

        DataSource ds = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TimeZone istTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
        java.util.Date d = new java.util.Date();
        sdf.setTimeZone(istTimeZone);
        String date1 = sdf.format(d);

        long millis=System.currentTimeMillis();
      //  java.sql.Date date=new java.sql.Date(millis);


        try {
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();

            String SQL ="select idlink,url,title,(select user_id from web_bookmark where link_id=idlink and user_id='"+currUser+"')as userId,votes,domain,image_url,category,description,(select voted from link_votes where link_id=idlink and user_id='"+currUser +"')as voted,vote_up,vote_down,(select distinct isBookmarked from web_bookmark where link_id=idlink and user_id='"+currUser +"')as bookmarked,(select voted from link_votes where link_id=idlink and user_id='"+currUser +"')as voted,date_created,publish,deleted,TIMEDIFF('"+date1+"',date_created)as hours from link where domain like '%" + category + "%'  and deleted=0 and media_type='Text/HTML' GROUP BY title ORDER BY date_created DESC limit "+start+","+limit+"";
            //String SQL ="select idlink,url,title,(select user_id from web_bookmark where link_id=idlink and user_id='"+currUser+"')as userId,votes,domain,image_url,category,description,(select voted from link_votes where link_id=idlink and user_id='"+currUser +"')as voted,vote_up,vote_down,(select distinct isBookmarked from web_bookmark where link_id=idlink and user_id='"+currUser +"')as bookmarked,(select voted from link_votes where link_id=idlink and user_id='"+currUser +"')as voted,date_created,publish,deleted,TIMEDIFF('"+date1+"',date_created)as hours from link where domain like '%" + category + "%'  and deleted=0 and media_type='Text/HTML'  ORDER BY date_created DESC limit 0,"+limit+"";

            logger.info("SQL:" + SQL);
            pstmt = conn.prepareStatement(SQL);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                Link link = new Link();

                    link.setUrl(rs.getString("url"));
                    link.setTitle(rs.getString("title"));
                    link.setLinkId(rs.getInt("idlink"));
                    link.setVotes(rs.getInt("votes"));
                    link.setDesc(rs.getString("description"));
                    link.setBookmark(rs.getInt("bookmarked"));
                    link.setUserId(rs.getString("userId"));

                    link.setDomain(rs.getString("domain"));
                    link.setDateCreated(rs.getDate("date_created"));
                  //  link.setHoursElapsed(rs.getLong("hours"));
                    //For Developement
                    Date date= sdf.parse(date1);
                    Date date2 = sdf.parse(rs.getString("date_created"));
                    // Calucalte time difference in milliseconds
                    long time_difference = date.getTime() - date2.getTime();

                    long hours_difference = (time_difference / (1000*60*60)) % 24;


                    link.setHoursElapsed(hours_difference);
                    //end of hours for dev
                    link.setCategory(rs.getString("category"));
                    //  link.setLanguage(rs.getString("language"));
                    //link.setCountry(rs.getString("country"));

                    //link.setSpam(rs.getInt("spam"));
                    link.setBookmarked(true);
                    link.setVoted(true);
                    link.setImageUrl(rs.getString("image_url"));
                    link.setVote_up(rs.getInt("vote_up"));
                    link.setVote_down(rs.getInt("vote_down"));
                    //System.out.println("voted--->"+rs.getString("voted"));
                    if(rs.getString("voted")==null)
                    {
                        link.setUserVoted("0");
                    }
                    else {
                        link.setUserVoted(rs.getString("voted"));
                    }
                link.setSubmitedBy("GaramGaram");
                String cat=rs.getString("category");
                if(cat.equals("इंडिया")){
                    cat="india";
                }if(cat.equals("दुनिया")){
                    System.out.println("Inside of World Category");
                    cat="world";
                }
                if(cat.equals("खेल")){cat="sports";}
                if(cat.equals("सामान्य समाचार")){cat="general";}
                if(cat.equals("मनोरंजन")){cat="cinema";}
                link.setCategory2(cat);

                String newsSource="";

                if(rs.getString("domain").equals("hindi.oneindia.com"))
                {
                    newsSource="हिंदी वनइंडिया";
                }
                if(rs.getString("domain").equals("hindi.news18.com"))
                {
                    newsSource="हिंदी न्यूज़ 18";
                }
                if(rs.getString("domain").equals("livehindustan.com"))
                {
                    newsSource="लाइव हिंदुस्तान";
                }
                if(rs.getString("domain").equals("amarujala.com")) {
                    newsSource = "अमर उजाला";
                }
                if(rs.getString("domain").equals("hindi.filmibeat.com")) {
                    newsSource = "हिंदी फिल्मीबीट";
                }
                if(rs.getString("domain").equals("zeenews.india.com")) {
                    newsSource = "जी नेवस";
                }
                if(rs.getString("domain").equals("bbc.com")) {
                    newsSource = "बीबीसी";
                }
                if(rs.getString("domain").equals("naidunia.com")) {
                    newsSource = "नईदुनिया";
                }
                if(rs.getString("domain").equals("prabhasakshi.com")) {
                    newsSource = "प्रभासाक्षी";
                }
                if(rs.getString("domain").equals("dabangdunia.co")) {
                    newsSource = "दबंगदुनिया";
                }

                link.setDomain(newsSource);
                String dom=link.getDomain();
                if(dom.equals("हिंदी वनइंडिया")){ dom="hindi.oneindia.com"; }
                if(dom.equals("हिंदी न्यूज़ 18")){dom="hindi.news18.com";}
                if(dom.equals("लाइव हिंदुस्तान")){dom="livehindustan.com";}
                if(dom.equals("अमर उजाला")){dom="amarujala.com";}
                if(dom.equals("हिंदी फिल्मीबीट")){dom="hindi.filmibeat.com";}
                if(dom.equals("जी नेवस")){dom="zeenews.india.com";}
                if(dom.equals("बीबीसी")){dom="bbc.com";}
                if(dom.equals("नईदुनिया")){dom="naidunia.com";}
                if(dom.equals("प्रभासाक्षी")){dom="prabhasakshi.com";}
                if(dom.equals("दबंगदुनिया")){dom="dabangdunia.co";}
                link.setDomain2(dom);
                links.add(link);

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (pstmt != null)
                    pstmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return links;
    }
    public List<Link> getFrontPageNews(int currUser,String start,String limit) {
        System.out.println("front page");

         List<Link> links = new ArrayList<Link>();

        DataSource ds = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TimeZone istTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
        java.util.Date d = new java.util.Date();
        sdf.setTimeZone(istTimeZone);
        String date1 = sdf.format(d);

        long millis=System.currentTimeMillis();
        // java.sql.Date date=new java.sql.Date(millis);


        try {
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();

            //   String SQL ="select idlink,url,title,votes,(select user_id from web_bookmark where link_id=idlink and user_id='"+currUser+"')as userId,domain,image_url,description,category,vote_up,vote_down,(select voted from link_votes where link_id=idlink and user_id='"+currUser +"')as voted,(select distinct isBookmarked from web_bookmark where link_id=idlink and user_id='"+currUser +"')as bookmarked,date_created,publish,deleted,TIMEDIFF('"+date1+"',date_created)as hours from link where category='" + category + "' and deleted=0 and media_type='Text/HTML'  ORDER BY date_created DESC limit 0,"+limit+"";


            String SQL ="select idlink,url,title,votes,(select user_id from web_bookmark where link_id=idlink and user_id='"+currUser+"')as userId,domain,image_url,description,category,vote_up,vote_down,(select voted from link_votes where link_id=idlink and user_id='"+currUser +"')as voted,(select distinct isBookmarked from web_bookmark where link_id=idlink and user_id='"+currUser +"')as bookmarked,date_created,publish,deleted,TIMEDIFF('"+date1+"',date_created)as hours from link where domain not IN('ibctamil.com','puthiyathalaimurai.com','tamil.samayam.com')  ORDER BY date_created DESC limit "+start+","+limit+"";
            logger.info("SQL:" + SQL);

            pstmt = conn.prepareStatement(SQL);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                Link link = new Link();

                link.setUrl(rs.getString("url"));
                link.setTitle(rs.getString("title"));
                link.setUserId(rs.getString("userId"));
                link.setLinkId(rs.getInt("idlink"));
                link.setVotes(rs.getInt("votes"));
                link.setDesc(rs.getString("description"));
                link.setBookmark(rs.getInt("bookmarked"));

                link.setDomain(rs.getString("domain"));
                link.setDateCreated(rs.getDate("date_created"));
                //   link.setHoursElapsed(rs.getLong("hours"));
                //For Developement
                Date date= sdf.parse(date1);
                Date date2 = sdf.parse(rs.getString("date_created"));
                // Calucalte time difference in milliseconds
                long time_difference = date.getTime() - date2.getTime();

                long hours_difference = (time_difference / (1000*60*60)) % 24;


                link.setHoursElapsed(hours_difference);
                //end of hours for dev
                link.setCategory(rs.getString("category"));
                //  link.setLanguage(rs.getString("language"));
                //link.setCountry(rs.getString("country"));

                //link.setSpam(rs.getInt("spam"));
                link.setBookmarked(true);
                link.setVoted(true);
                link.setImageUrl(rs.getString("image_url"));
                link.setVote_up(rs.getInt("vote_up"));
                link.setVote_down(rs.getInt("vote_down"));
                //System.out.println("voted--->"+rs.getString("voted"));
                if(rs.getString("voted")==null)
                {
                    link.setUserVoted("0");
                }
                else {
                    link.setUserVoted(rs.getString("voted"));
                }
                link.setSubmitedBy("GaramGaram");
                String cat=rs.getString("category");
                if(cat.equals("इंडिया")){
                    cat="india";
                }if(cat.equals("दुनिया")){
                    System.out.println("Inside of World Category");
                    cat="world";
                }
                if(cat.equals("खेल")){cat="sports";}
                if(cat.equals("सामान्य समाचार")){cat="general";}
                if(cat.equals("मनोरंजन")){cat="cinema";}
                link.setCategory2(cat);

                String newsSource="";

                if(rs.getString("domain").equals("hindi.oneindia.com"))
                {
                    newsSource="हिंदी वनइंडिया";
                }
                if(rs.getString("domain").equals("hindi.news18.com"))
                {
                    newsSource="हिंदी न्यूज़ 18";
                }
                if(rs.getString("domain").equals("livehindustan.com"))
                {
                    newsSource="लाइव हिंदुस्तान";
                }
                if(rs.getString("domain").equals("amarujala.com")) {
                    newsSource = "अमर उजाला";
                }
                if(rs.getString("domain").equals("hindi.filmibeat.com")) {
                    newsSource = "हिंदी फिल्मीबीट";
                }
                if(rs.getString("domain").equals("zeenews.india.com")) {
                    newsSource = "जी नेवस";
                }
                if(rs.getString("domain").equals("bbc.com")) {
                    newsSource = "बीबीसी";
                }
                if(rs.getString("domain").equals("naidunia.com")) {
                    newsSource = "नईदुनिया";
                }
                if(rs.getString("domain").equals("prabhasakshi.com")) {
                    newsSource = "प्रभासाक्षी";
                }
                if(rs.getString("domain").equals("dabangdunia.co")) {
                    newsSource = "दबंगदुनिया";
                }

                link.setDomain(newsSource);
                String dom=link.getDomain();
                if(dom.equals("हिंदी वनइंडिया")){ dom="hindi.oneindia.com"; }
                if(dom.equals("हिंदी न्यूज़ 18")){dom="hindi.news18.com";}
                if(dom.equals("लाइव हिंदुस्तान")){dom="livehindustan.com";}
                if(dom.equals("अमर उजाला")){dom="amarujala.com";}
                if(dom.equals("हिंदी फिल्मीबीट")){dom="hindi.filmibeat.com";}
                if(dom.equals("जी नेवस")){dom="zeenews.india.com";}
                if(dom.equals("बीबीसी")){dom="bbc.com";}
                if(dom.equals("नईदुनिया")){dom="naidunia.com";}
                if(dom.equals("प्रभासाक्षी")){dom="prabhasakshi.com";}
                if(dom.equals("दबंगदुनिया")){dom="dabangdunia.co";}
                link.setDomain2(dom);
                links.add(link);

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (pstmt != null)
                    pstmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TimeZone istTimeZone2 = TimeZone.getTimeZone("Asia/Kolkata");
        java.util.Date d2 = new java.util.Date();
        sdf.setTimeZone(istTimeZone);
        String date2 = sdf.format(d2);

        System.out.println("query ended at"+date2);
        return links;
    }
    //For category
    public List<Link> getNews(String category,int descriptionLength,int currUser,String start,String limit) {

        System.out.println("inside dao"+category);
        List<Link> links = new ArrayList<Link>();

        DataSource ds = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TimeZone istTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
        java.util.Date d = new java.util.Date();
        sdf.setTimeZone(istTimeZone);
        String date1 = sdf.format(d);

        long millis=System.currentTimeMillis();
       // java.sql.Date date=new java.sql.Date(millis);


        try {
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();

         //   String SQL ="select idlink,url,title,votes,(select user_id from web_bookmark where link_id=idlink and user_id='"+currUser+"')as userId,domain,image_url,description,category,vote_up,vote_down,(select voted from link_votes where link_id=idlink and user_id='"+currUser +"')as voted,(select distinct isBookmarked from web_bookmark where link_id=idlink and user_id='"+currUser +"')as bookmarked,date_created,publish,deleted,TIMEDIFF('"+date1+"',date_created)as hours from link where category='" + category + "' and deleted=0 and media_type='Text/HTML'  ORDER BY date_created DESC limit 0,"+limit+"";


              String SQL ="select idlink,url,title,votes,(select user_id from web_bookmark where link_id=idlink and user_id='"+currUser+"')as userId,domain,image_url,description,category,vote_up,vote_down,(select voted from link_votes where link_id=idlink and user_id='"+currUser +"')as voted,(select distinct isBookmarked from web_bookmark where link_id=idlink and user_id='"+currUser +"')as bookmarked,date_created,publish,deleted,TIMEDIFF('"+date1+"',date_created)as hours from link where category='" + category + "' and deleted=0 and media_type='Text/HTML' GROUP BY title ORDER BY date_created DESC limit "+start+","+limit+"";
            logger.info("SQL:" + SQL);

            pstmt = conn.prepareStatement(SQL);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                Link link = new Link();
                    System.out.println("inside of ......");
                    link.setUrl(rs.getString("url"));
                    link.setTitle(rs.getString("title"));
                    link.setUserId(rs.getString("userId"));
                    link.setLinkId(rs.getInt("idlink"));
                    link.setVotes(rs.getInt("votes"));
                    link.setDesc(rs.getString("description"));
                    link.setBookmark(rs.getInt("bookmarked"));

                    link.setDomain(rs.getString("domain"));
                    link.setDateCreated(rs.getDate("date_created"));
                 //   link.setHoursElapsed(rs.getLong("hours"));
                    //For Developement
                    Date date= sdf.parse(date1);
                    Date date2 = sdf.parse(rs.getString("date_created"));
                    // Calucalte time difference in milliseconds
                    long time_difference = date.getTime() - date2.getTime();

                    long hours_difference = (time_difference / (1000*60*60)) % 24;


                    link.setHoursElapsed(hours_difference);
                    //end of hours for dev
                    link.setCategory(rs.getString("category"));
                    //  link.setLanguage(rs.getString("language"));
                    //link.setCountry(rs.getString("country"));

                    //link.setSpam(rs.getInt("spam"));
                    link.setBookmarked(true);
                    link.setVoted(true);
                    link.setImageUrl(rs.getString("image_url"));
                    link.setVote_up(rs.getInt("vote_up"));
                    link.setVote_down(rs.getInt("vote_down"));
                    //System.out.println("voted--->"+rs.getString("voted"));
                    if(rs.getString("voted")==null)
                    {
                        link.setUserVoted("0");
                    }
                    else {
                        link.setUserVoted(rs.getString("voted"));
                    }
                link.setSubmitedBy("GaramGaram");
                String cat=rs.getString("category");
                if(cat.equals("इंडिया")){
                    cat="india";
                }if(cat.equals("दुनिया")){
                    System.out.println("Inside of World Category");
                    cat="world";
                }
                if(cat.equals("खेल")){cat="sports";}
                if(cat.equals("सामान्य समाचार")){cat="general";}
                if(cat.equals("मनोरंजन")){cat="cinema";}
                link.setCategory2(cat);

                String newsSource="";

                if(rs.getString("domain").equals("hindi.oneindia.com"))
                {
                    newsSource="हिंदी वनइंडिया";
                }
                if(rs.getString("domain").equals("hindi.news18.com"))
                {
                    newsSource="हिंदी न्यूज़ 18";
                }
                if(rs.getString("domain").equals("livehindustan.com"))
                {
                    newsSource="लाइव हिंदुस्तान";
                }
                if(rs.getString("domain").equals("amarujala.com")) {
                    newsSource = "अमर उजाला";
                }
                if(rs.getString("domain").equals("hindi.filmibeat.com")) {
                    newsSource = "हिंदी फिल्मीबीट";
                }
                if(rs.getString("domain").equals("zeenews.india.com")) {
                    newsSource = "जी नेवस";
                }
                if(rs.getString("domain").equals("bbc.com")) {
                    newsSource = "बीबीसी";
                }
                if(rs.getString("domain").equals("naidunia.com")) {
                    newsSource = "नईदुनिया";
                }
                if(rs.getString("domain").equals("prabhasakshi.com")) {
                    newsSource = "प्रभासाक्षी";
                }
                if(rs.getString("domain").equals("dabangdunia.co")) {
                    newsSource = "दबंगदुनिया";
                }

                link.setDomain(newsSource);
                String dom=link.getDomain();
                if(dom.equals("हिंदी वनइंडिया")){ dom="hindi.oneindia.com"; }
                if(dom.equals("हिंदी न्यूज़ 18")){dom="hindi.news18.com";}
                if(dom.equals("लाइव हिंदुस्तान")){dom="livehindustan.com";}
                if(dom.equals("अमर उजाला")){dom="amarujala.com";}
                if(dom.equals("हिंदी फिल्मीबीट")){dom="hindi.filmibeat.com";}
                if(dom.equals("जी नेवस")){dom="zeenews.india.com";}
                if(dom.equals("बीबीसी")){dom="bbc.com";}
                if(dom.equals("नईदुनिया")){dom="naidunia.com";}
                if(dom.equals("प्रभासाक्षी")){dom="prabhasakshi.com";}
                if(dom.equals("दबंगदुनिया")){dom="dabangdunia.co";}
                link.setDomain2(dom);
                links.add(link);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (pstmt != null)
                    pstmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        System.out.println(links.get(0).getTitle());
        return links;
    }
    //For domain and category
    public List<Link> getCategoryForNews(String category,String domain,int descriptionLength,int currUser,String start,String limit) {

        System.out.println("category"+category);
        System.out.println("domain"+domain);
        List<Link> links = new ArrayList<Link>();

        DataSource ds = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TimeZone istTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
        java.util.Date d = new java.util.Date();
        sdf.setTimeZone(istTimeZone);
        String date1 = sdf.format(d);

        long millis=System.currentTimeMillis();
        //java.sql.Date date=new java.sql.Date(millis);


        try {
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();

            String SQL ="select idlink,url,title,votes,vote_up,(select user_id from web_bookmark where link_id=idlink and user_id='"+currUser+"')as userId,vote_down,domain,image_url,description,category,(select voted from link_votes where link_id=idlink and user_id='"+currUser +"')as voted,(select distinct isBookmarked from web_bookmark where link_id=idlink and user_id='"+currUser +"')as bookmarked,date_created,publish,deleted,TIMEDIFF('"+date1+"',date_created)as hours from link where domain like '%" + domain + "%' and category='"+category+"' and deleted=0 and media_type='Text/HTML' GROUP BY title ORDER BY date_created DESC limit "+start+","+limit+"";
           // String SQL ="select idlink,url,title,votes,vote_up,(select user_id from web_bookmark where link_id=idlink and user_id='"+currUser+"')as userId,vote_down,domain,image_url,description,category,(select voted from link_votes where link_id=idlink and user_id='"+currUser +"')as voted,(select distinct isBookmarked from web_bookmark where link_id=idlink and user_id='"+currUser +"')as bookmarked,date_created,publish,deleted,TIMEDIFF('"+date1+"',date_created)as hours from link where domain like '%" + domain + "%' and category='"+category+"' and deleted=0 and media_type='Text/HTML'  ORDER BY date_created DESC limit 0,"+limit+"";

            pstmt = conn.prepareStatement(SQL);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                Link link = new Link();

                    link.setUrl(rs.getString("url"));
                    link.setTitle(rs.getString("title"));
                    link.setLinkId(rs.getInt("idlink"));
                    link.setVotes(rs.getInt("votes"));
                    link.setDesc(rs.getString("description"));
                    link.setBookmark(rs.getInt("bookmarked"));
                    link.setUserId(rs.getString("userId"));

                    link.setDomain(rs.getString("domain"));
                    link.setDateCreated(rs.getDate("date_created"));
                   // link.setHoursElapsed(rs.getLong("hours"));

                    //For Developement
                    Date date= sdf.parse(date1);
                    Date date2 = sdf.parse(rs.getString("date_created"));
                    // Calucalte time difference in milliseconds
                    long time_difference = date.getTime() - date2.getTime();

                    long hours_difference = (time_difference / (1000*60*60)) % 24;


                    link.setHoursElapsed(hours_difference);
                    //end of hours for dev
                    link.setCategory(rs.getString("category"));
                    //  link.setLanguage(rs.getString("language"));
                    //link.setCountry(rs.getString("country"));

                    //link.setSpam(rs.getInt("spam"));
                    link.setBookmarked(true);
                    link.setVoted(true);
                    link.setImageUrl(rs.getString("image_url"));
                    link.setVote_up(rs.getInt("vote_up"));
                    link.setVote_down(rs.getInt("vote_down"));
                    //System.out.println("voted--->"+rs.getString("voted"));
                    if(rs.getString("voted")==null)
                    {
                        link.setUserVoted("0");
                    }
                    else {
                        link.setUserVoted(rs.getString("voted"));
                    }
                link.setSubmitedBy("GaramGaram");
                String cat=rs.getString("category");
                if(cat.equals("इंडिया")){
                    cat="india";
                }if(cat.equals("दुनिया")){
                    System.out.println("Inside of World Category");
                    cat="world";
                }
                if(cat.equals("खेल")){cat="sports";}
                if(cat.equals("सामान्य समाचार")){cat="general";}
                if(cat.equals("मनोरंजन")){cat="cinema";}
                link.setCategory2(cat);

                String newsSource="";

                if(rs.getString("domain").equals("hindi.oneindia.com"))
                {
                    newsSource="हिंदी वनइंडिया";
                }
                if(rs.getString("domain").equals("hindi.news18.com"))
                {
                    newsSource="हिंदी न्यूज़ 18";
                }
                if(rs.getString("domain").equals("livehindustan.com"))
                {
                    newsSource="लाइव हिंदुस्तान";
                }
                if(rs.getString("domain").equals("amarujala.com")) {
                    newsSource = "अमर उजाला";
                }
                if(rs.getString("domain").equals("hindi.filmibeat.com")) {
                    newsSource = "हिंदी फिल्मीबीट";
                }
                if(rs.getString("domain").equals("zeenews.india.com")) {
                    newsSource = "जी नेवस";
                }
                if(rs.getString("domain").equals("bbc.com")) {
                    newsSource = "बीबीसी";
                }
                if(rs.getString("domain").equals("naidunia.com")) {
                    newsSource = "नईदुनिया";
                }
                if(rs.getString("domain").equals("prabhasakshi.com")) {
                    newsSource = "प्रभासाक्षी";
                }
                if(rs.getString("domain").equals("dabangdunia.co")) {
                    newsSource = "दबंगदुनिया";
                }

                link.setDomain(newsSource);
                String dom=link.getDomain();
                if(dom.equals("हिंदी वनइंडिया")){ dom="hindi.oneindia.com"; }
                if(dom.equals("हिंदी न्यूज़ 18")){dom="hindi.news18.com";}
                if(dom.equals("लाइव हिंदुस्तान")){dom="livehindustan.com";}
                if(dom.equals("अमर उजाला")){dom="amarujala.com";}
                if(dom.equals("हिंदी फिल्मीबीट")){dom="hindi.filmibeat.com";}
                if(dom.equals("जी नेवस")){dom="zeenews.india.com";}
                if(dom.equals("बीबीसी")){dom="bbc.com";}
                if(dom.equals("नईदुनिया")){dom="naidunia.com";}
                if(dom.equals("प्रभासाक्षी")){dom="prabhasakshi.com";}
                if(dom.equals("दबंगदुनिया")){dom="dabangdunia.co";}
                link.setDomain2(dom);
                links.add(link);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (pstmt != null)
                    pstmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return links;
    }
    public List<Link> getCategoryNewsForMobile(String category,String val,int descriptionLength) {

        System.out.println("Inside of News "+category);
        List<Link> links = new ArrayList<Link>();

        DataSource ds = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TimeZone istTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
        java.util.Date d = new java.util.Date();
        sdf.setTimeZone(istTimeZone);
        String date1 = sdf.format(d);

        long millis=System.currentTimeMillis();
        java.sql.Date date=new java.sql.Date(millis);


        try {
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();

            //  String SQL ="select * from link where domain='" + category + "' and deleted=0 and publish=1 ORDER BY date_created DESC";

            String SQL ="select idlink,url,title,votes,domain,image_url,description,date_created,category,likes,bookmark,publish,deleted,(select distinct no_of_likes from like_user where link_id=idlink)as liked,TIMEDIFF('"+date1+"',date_created)as hours from link where domain like '%" + category + "%'  and media_type='Text/HTML' and deleted=0 GROUP BY title ORDER BY date_created  DESC limit 0,"+val+" ";
            logger.debug("SQL"+SQL);

            pstmt = conn.prepareStatement(SQL);
            rs = pstmt.executeQuery();

            //          For Description limit
            while (rs.next()) {
                Link link = new Link();
                String desc=rs.getString("description");
                int descLength=desc.length();
                //System.out.println("length is"+descLength);
                if(descLength>descriptionLength)
                {

                    link.setLinkId(rs.getInt("idlink"));
                    link.setUrl(rs.getString("url"));
                    link.setTitle(rs.getString("title"));
                    link.setCategory(rs.getString("category"));
                    link.setVotes(rs.getInt("votes"));

                    link.setDateCreated(rs.getDate("date_created"));
                    link.setDomain(rs.getString("domain"));
                    link.setLikes(rs.getInt("likes"));
                    link.setLiked(rs.getInt("liked"));
                    link.setDesc(rs.getString("description"));
                    link.setBookmark(rs.getInt("bookmark"));
                    link.setBookmarked(true);
                    link.setVoted(true);
                    link.setImageUrl(rs.getString("image_url"));
                    link.setHoursElapsed(rs.getLong("hours"));
                    link.setSubmitedBy("ChudaChuda");

                    // System.out.println("description is"+rs.getString("description"));

                    String descriptionValue=link.getDesc();
                    int desc2=descriptionValue.length();

                    // System.out.println("length"+desc2);
                    if (desc2 >= 120 && desc2 <= 180) {
                        System.out.println("120 to 180"+descriptionValue);
                        link.setDesc(descriptionValue);
                    }
                    else {

                        System.out.println("180 above");


                        link.setDesc(descriptionValue.substring(0, 180) + "....");
                    }
                    links.add(link);
                }

                else
                {
                    //System.out.println("text is less than 120");
                }



            }



        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (pstmt != null)
                    pstmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return links;
    }


    public List<Link> getTopNews() {

        List<Link> links = new ArrayList<Link>();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TimeZone istTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
        java.util.Date d = new java.util.Date();
        sdf.setTimeZone(istTimeZone);
        String date1 = sdf.format(d);
        System.out.println("timestamp---->"+date1);

        long millis=System.currentTimeMillis();
        java.sql.Date date=new java.sql.Date(millis);



        DataSource ds = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();

            String SQL ="select idlink,url,title,domain,image_url,description,publish,deleted,category,spam,position,TIMEDIFF('"+date1+"',date_created)as hours from link where  date_created LIKE CONCAT('%','"+date+"','%') and deleted=0 and publish=1 ORDER BY(position >0) desc ,position limit 0,10;";
            pstmt = conn.prepareStatement(SQL);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                Link link = new Link();
                link.setUrl(rs.getString("url"));
                link.setTitle(rs.getString("title"));
                link.setLinkId(rs.getInt("idlink"));
                link.setVotes(rs.getInt("publish"));
                link.setDesc(rs.getString("description"));

                link.setDomain(rs.getString("domain"));
                link.setCategory(rs.getString("category"));
                link.setHoursElapsed(rs.getLong("hours"));
                link.setSpam(rs.getInt("spam"));
                link.setBookmarked(true);
                link.setVoted(true);
                link.setImageUrl(rs.getString("image_url"));
                //  link.setPublish(rs.getInt("publish"));
                link.setSubmitedBy("ChudaChuda");

                link.setPosition(rs.getInt("position"));


                links.add(link);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (pstmt != null)
                    pstmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return links;
    }


    public List<Link> getVideoFeed() {

        List<Link> links = new ArrayList<Link>();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TimeZone istTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
        java.util.Date d = new java.util.Date();
        sdf.setTimeZone(istTimeZone);
        String date1 = sdf.format(d);
        System.out.println("timestamp---->"+date1);

        long millis=System.currentTimeMillis();
        java.sql.Date date=new java.sql.Date(millis);



        DataSource ds = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();

            String SQL ="select idlink,url,title,domain,image_url,description,publish,deleted,category,media_type,format,spam,position,TIMEDIFF('"+date1+"',date_created)as hours from link where media_type='video' and deleted=0 ORDER BY date_created DESC ";
            pstmt = conn.prepareStatement(SQL);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                Link link = new Link();
                link.setUrl(rs.getString("url"));
                link.setTitle(rs.getString("title"));
                link.setLinkId(rs.getInt("idlink"));
                link.setVotes(rs.getInt("publish"));
                link.setDesc(rs.getString("description"));

                link.setDomain(rs.getString("domain"));
                link.setCategory(rs.getString("category"));
                link.setHoursElapsed(rs.getLong("hours"));
                link.setSpam(rs.getInt("spam"));
                link.setMedia_type(rs.getString("media_type"));
                link.setVideo_format(rs.getString("format"));
                link.setBookmarked(true);
                link.setVoted(true);
                link.setImageUrl(rs.getString("image_url"));
                //  link.setPublish(rs.getInt("publish"));
                link.setSubmitedBy("ChudaChuda");

                link.setPosition(rs.getInt("position"));


                links.add(link);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (pstmt != null)
                    pstmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return links;
    }

    public int updateLikes(int id,String mobile_id)
    {
        List<Link> links = new ArrayList<Link>();
        int likes=0;
        DataSource ds = null;
        Connection conn = null;
        Statement stmt = null;
        String url="";
        String res="";
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {


            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String sql = "update link set likes=likes+1 where idlink='"+id+"'";
            logger.debug("SQL:" + sql);
            stmt.executeUpdate(sql);

            String SQL="insert into like_user values(0,'"+id+"','"+mobile_id+"',1)";
            logger.info("SQL:" + SQL);
            stmt.executeUpdate(SQL);

            String sql_get ="select likes from link where idlink='"+id+"'";
            pstmt= conn.prepareStatement(sql_get);
            rs = pstmt.executeQuery();
            while (rs.next()) {

                likes=rs.getInt("likes");
                System.out.println("likes"+likes);
            }




        } catch (Exception ex) {
            ex.printStackTrace();
            res="Delete failure";
            logger.error(res);
        }
        finally {
            try {

                if (conn != null)
                    conn.close();
                if (pstmt != null)
                    pstmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return likes;
    }

    public ArrayList<Integer> isLiked(String deviceId,String domain) {


        ArrayList<Integer> links=new ArrayList<Integer>();

        DataSource ds = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        TimeZone istTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
        java.util.Date d = new java.util.Date();
        sdf.setTimeZone(istTimeZone);
        String date = sdf.format(d);
        System.out.println("timestamp---->"+date);



        try {
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            String SQL =" select l.idlink,lu.no_of_likes as liked,lu.mobile_id,l.domain,l.date_created from like_user lu left join link l on lu.link_id=l.idlink and lu.mobile_id='"+deviceId+"' and l.domain LIKE '%" + domain+ "%' and l.date_created LIKE CONCAT('%','"+date+"','%') ";
            logger.debug("SQL"+SQL);
            pstmt = conn.prepareStatement(SQL);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Link link = new Link();
                //String id=String.valueOf(rs.getInt("idlink"));

                if(rs.getInt("idlink")!=0) {
                    System.out.println("is id"+rs.getInt("idlink"));
                    links.add(rs.getInt("idlink"));
                }


            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (pstmt != null)
                    pstmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return links;
    }
    public ArrayList<Integer> isLikedForKathambam(String deviceId) {


        ArrayList<Integer> links=new ArrayList<Integer>();

        DataSource ds = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        TimeZone istTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
        java.util.Date d = new java.util.Date();
        sdf.setTimeZone(istTimeZone);
        String date = sdf.format(d);
        System.out.println("timestamp---->"+date);



        try {
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            String SQL =" select l.idlink,lu.no_of_likes as liked,lu.mobile_id,l.domain,l.date_created from like_user lu left join link l on lu.link_id=l.idlink and lu.mobile_id='"+deviceId+"' and l.publish=1 and l.date_created LIKE CONCAT('%','"+date+"','%') ";
            logger.debug("SQL"+SQL);
            pstmt = conn.prepareStatement(SQL);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Link link = new Link();
                //String id=String.valueOf(rs.getInt("idlink"));

                if(rs.getInt("idlink")!=0) {
                    System.out.println("is id"+rs.getInt("idlink"));
                    links.add(rs.getInt("idlink"));
                }


            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (pstmt != null)
                    pstmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return links;
    }


    public int updateBookmark(int id,String mobile_id)
    {
        List<Link> links = new ArrayList<Link>();
        int bookmark=0;
        DataSource ds = null;
        Connection conn = null;
        Statement stmt = null;
        String url="";
        String res="";
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TimeZone istTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
        java.util.Date d = new java.util.Date();
        sdf.setTimeZone(istTimeZone);
        String date = sdf.format(d);
        System.out.println("timestamp---->"+date);
        try {


            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String sql = "update link set bookmark=1 where idlink='"+id+"'";
            logger.debug("SQL:" + sql);
            stmt.executeUpdate(sql);

            String SQL="insert into mobile_bookmark values(0,'"+id+"','"+mobile_id+"',1,'"+date+"')";
            logger.info("SQL:" + SQL);
            stmt.executeUpdate(SQL);

            String sql_get ="select bookmark from link where idlink='"+id+"'";
            pstmt= conn.prepareStatement(sql_get);
            rs = pstmt.executeQuery();
            while (rs.next()) {

                bookmark=rs.getInt("bookmark");
                System.out.println("bookmark"+bookmark);
            }

            System.out.println("bookmark inserted successfully.....");

        } catch (Exception ex) {
            ex.printStackTrace();
            res="Delete failure";
            logger.error(res);
        }
        finally {
            try {

                if (conn != null)
                    conn.close();
                if (pstmt != null)
                    pstmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return bookmark;
    }


    public ArrayList<Integer> isBookmarked(String deviceId,String domain) {

        System.out.println("Find News is Alreday Bookmarked....");
        ArrayList<Integer> bookmarks=new ArrayList<Integer>();

        DataSource ds = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        TimeZone istTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
        java.util.Date d = new java.util.Date();
        sdf.setTimeZone(istTimeZone);
        String date = sdf.format(d);
        System.out.println("timestamp---->"+date);



        try {
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();

            //  String SQL =" select l.idlink,lu.no_of_likes as liked,lu.mobile_id,l.domain,l.date_created from like_user lu left join link l on lu.link_id=l.idlink and lu.mobile_id='"+deviceId+"' and l.domain='"+domain+"' and l.date_created LIKE CONCAT('%','"+date+"','%') ";
            String SQL= "select l.idlink,mb.isBookmarked as bookmarked,mb.mobile_id,l.domain,l.date_created from mobile_bookmark mb left join link l on mb.linkId=l.idlink and mb.mobile_id='"+deviceId+"' and l.domain LIKE '%" + domain+ "%'";
            logger.debug("SQL"+SQL);
            pstmt = conn.prepareStatement(SQL);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Link link = new Link();
                //String id=String.valueOf(rs.getInt("idlink"));

                if(rs.getInt("idlink")!=0) {
                    System.out.println("is id"+rs.getInt("idlink"));
                    bookmarks.add(rs.getInt("idlink"));
                }


            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (pstmt != null)
                    pstmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return bookmarks;
    }
    public List<Link> getBookmarks(String deviceId) {


        System.out.println("Getting Bookmarks for Mobile");
        ArrayList links = new ArrayList();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TimeZone istTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
        Date d = new Date();
        sdf.setTimeZone(istTimeZone);
        String date1 = sdf.format(d);
        System.out.println("timestamp---->" + date1);
        long millis = System.currentTimeMillis();
        new java.sql.Date(millis);
        DataSource ds = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            String ex = "select l.idlink,l.title,l.url,l.description,l.date_created,l.domain,l.image_url,b.mobile_id,b.date_created as bookmarkCreated from link l left join mobile_bookmark b on l.idlink=b.linkId where b.mobile_id='" + deviceId + "' order by b.date_created desc";
            pstmt = conn.prepareStatement(ex);
            rs = pstmt.executeQuery();

            while(rs.next()) {
                Link link = new Link();
                link.setUrl(rs.getString("url"));
                link.setTitle(rs.getString("title"));
                link.setLinkId(rs.getInt("idlink"));
                link.setDesc(rs.getString("description"));
                link.setDomain(rs.getString("domain"));
                link.setImageUrl(rs.getString("image_url"));
                link.setDateCreated(rs.getDate("bookmarkCreated"));
                System.out.println("bookmark title" + rs.getString("title"));
                links.add(link);
            }
        } catch (Exception var25) {
            var25.printStackTrace();
        } finally {
            try {
                if(conn != null) {
                    conn.close();
                }

                if(pstmt != null) {
                    pstmt.close();
                }

                if(rs != null) {
                    rs.close();
                }
            } catch (SQLException var24) {
                var24.printStackTrace();
            }

        }

        return links;
    }

    public  HashMap<String, String> likeArticles(String emailId, String linkId,float longitude,float latitude,String conutry) {
        List<String> likeInterestData = new ArrayList<String>();

        //String likeInterestDataSql="select voted from google_fav_like where google_fav_id=:id and user_id=(select id from user where email=:email)";
        // HashMap<String, Object> parameters4 = new HashMap<String, Object>();
        //parameters4.put("id", linkId);
        //parameters4.put("email", emailId);

        DataSource ds = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
        PreparedStatement pstmt1 = null;
        PreparedStatement pstmt2 = null;
        ResultSet rs = null;
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        String userId="";

        System.out.println("inside of likeArticles");
        try {
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();

            String ex1 = "select idUser from user where email='" + emailId + "'";
            System.out.println(ex1);
            pstmt2 = conn.prepareStatement(ex1);
            rs2 = pstmt2.executeQuery();
            System.out.println("next--->"+rs2.next()+rs2.getString("idUser"));
            userId=rs2.getString("idUser");

            //String likeInterestDataSql="select voted from link_votes where linkId=:id and user_id=(select id from user where email=:email)";
            String ex = "select voted from link_votes where link_id='" + linkId + "' and user_id='"+ userId+ "'";
            System.out.println(ex);
            pstmt = conn.prepareStatement(ex);
            rs = pstmt.executeQuery();

            while (rs.next()) {


                likeInterestData.add(0, rs.getString("voted"));
                System.out.println("bookmark title" + rs.getString("voted"));

            }
        } catch (Exception var25) {
            var25.printStackTrace();
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }

                if (pstmt != null) {
                    pstmt.close();
                }

                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException var24) {
                var24.printStackTrace();
            }

        }


        int numberOfVotesUp = 0;
        System.out.println("num"+numberOfVotesUp);
        if (likeInterestData.size()>0) {
            numberOfVotesUp = Integer.parseInt(likeInterestData.get(0));
        }
        System.out.println("num"+numberOfVotesUp);
        if (likeInterestData.size()>0) {

            System.out.println("userId in deleye--->"+userId);
            try {
                ds = DBConnection.getDataSource();
                conn = ds.getConnection();
                //String likeInterestDataSql="select voted from link_votes where linkId=:id and user_id=(select id from user where email=:email)";
                String ex = "delete from link_votes where link_id='" + linkId + "' and user_id='" + userId + "' ";
                pstmt = conn.prepareStatement(ex);
                int t = pstmt.executeUpdate();
                System.out.println("inside delete--->"+ex);
                System.out.println("inside delete--->"+t);

            } catch (Exception var25) {
                var25.printStackTrace();
            } finally {
                try {
                    if (conn != null) {
                        conn.close();
                    }

                    if (pstmt != null) {
                        pstmt.close();
                    }

                    if (rs != null) {
                        rs.close();
                    }
                } catch (SQLException var24) {
                    var24.printStackTrace();
                }

            }
        }
        if (numberOfVotesUp == 0) {
            try {
                ds = DBConnection.getDataSource();
                conn = ds.getConnection();
                //String likeInterestDataSql="select voted from link_votes where linkId=:id and user_id=(select id from user where email=:email)";
                String ex = "update  link set vote_up=vote_up+1 where idlink='" + linkId + "'";

                pstmt = conn.prepareStatement(ex);
                int t = pstmt.executeUpdate();
                System.out.println("ex--"+ex);
                System.out.println("ex--"+rs);


                //String likeInterestDataSql="select voted from link_votes where linkId=:id and user_id=(select id from user where email=:email)";
                String ex1 = "select idUser from user where email='" + emailId + "'";
                System.out.println(ex1);
                pstmt2 = conn.prepareStatement(ex1);
                rs2 = pstmt2.executeQuery();
                System.out.println("next--->"+rs2.next()+rs2.getString("idUser"));
                userId=rs2.getString("idUser");
                /*while (rs2.next()) {
                    System.out.println("bookmark title if" + rs2.getString("idUser"));
                    //likeInterestData.set(0, rs2.getString("id"));
                    userId=rs2.getString("idUser");
                    System.out.println("bookmark title" + rs2.getString("idUser"));

                }*/


                String SQL2 = "insert into link_votes(link_id,user_id,voted,longitude,latitude,country) values ('" + linkId + "','"+ userId+"','1','"+longitude+"','"+latitude+"','"+conutry+"')";
                pstmt1 = conn.prepareStatement(SQL2);
                int r1 = pstmt1.executeUpdate();
                System.out.println("ex--"+SQL2);
                System.out.println("ex--"+r1);

            } catch (Exception var25) {
                var25.printStackTrace();
            } finally {
                try {
                    if (conn != null) {
                        conn.close();
                    }

                    if (pstmt != null) {
                        pstmt.close();
                    }

                    if (rs != null) {
                        rs.close();
                    }
                } catch (SQLException var24) {
                    var24.printStackTrace();
                }

            }


        } else if (numberOfVotesUp == 1) {

            try {
                ds = DBConnection.getDataSource();
                conn = ds.getConnection();
                //String likeInterestDataSql="select voted from link_votes where linkId=:id and user_id=(select id from user where email=:email)";
                String ex = "update  link set vote_up=vote_up-1 where idlink='" + linkId + "'";
                pstmt = conn.prepareStatement(ex);
                int t1 = pstmt.executeUpdate();


            } catch (Exception var25) {
                var25.printStackTrace();
            } finally {
                try {
                    if (conn != null) {
                        conn.close();
                    }

                    if (pstmt != null) {
                        pstmt.close();
                    }

                    if (rs != null) {
                        rs.close();
                    }
                } catch (SQLException var24) {
                    var24.printStackTrace();
                }

            }
        } else {

            try {
                ds = DBConnection.getDataSource();
                conn = ds.getConnection();
                //String likeInterestDataSql="select voted from link_votes where linkId=:id and user_id=(select id from user where email=:email)";
                String ex = "update  link set vote_down=vote_down-1 where idlink='" + linkId + "'";
                pstmt = conn.prepareStatement(ex);
                int rs4 = pstmt.executeUpdate();

                String ex1 = "update  link set vote_up=vote_up+1 where idlink='" + linkId + "'";
                pstmt = conn.prepareStatement(ex1);
                pstmt.executeUpdate();

                String ex2 = "select idUser from user where email='" + emailId + "'";
                System.out.println(ex2);
                pstmt = conn.prepareStatement(ex2);
                rs = pstmt.executeQuery();
                System.out.println("next--->"+rs.next());

                userId=rs.getString("idUser");  userId=rs.getString("idUser");
                /*while (rs.next()) {
                    System.out.println("bookmark title if" + rs.getString("idUser"));
                    //likeInterestData.set(0, rs.getString("idUser"));
                    userId=rs.getString("idUser");
                    System.out.println("bookmark title" + rs.getString("idUser"));

                }*/

                String SQL2 = "insert into link_votes(link_id,user_id,voted,longitude,latitude,country) values ('" + linkId + "','"+ userId+"','1','"+longitude+"','"+latitude +"','"+conutry+"')";
                pstmt1 = conn.prepareStatement(SQL2);
                pstmt1.executeUpdate();
            } catch (Exception var25) {
                var25.printStackTrace();
            } finally {
                try {
                    if (conn != null) {
                        conn.close();
                    }

                    if (pstmt != null) {
                        pstmt.close();
                    }

                    if (rs != null) {
                        rs.close();
                    }
                } catch (SQLException var24) {
                    var24.printStackTrace();
                }
            }

        }
        //String getVotesUpSql = "select vote_up,vote_down from google_fav where id=:id";
        HashMap<String,String> vote_up = new HashMap<String, String>();


        try {
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            //String likeInterestDataSql="select voted from link_votes where linkId=:id and user_id=(select id from user where email=:email)";
            String ex = "select vote_up,vote_down from link where idlink='" + linkId + "'";
            pstmt = conn.prepareStatement(ex);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                System.out.println("vote_up" + rs.getString("vote_up"));
                System.out.println("vote_up" + rs.getString("vote_down"));

                vote_up.put("vote_up", rs.getString("vote_up"));
                vote_up.put("vote_down", rs.getString("vote_down"));

            }
        } catch (Exception var25) {
            var25.printStackTrace();
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }

                if (pstmt != null) {
                    pstmt.close();
                }

                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException var24) {
                var24.printStackTrace();
            }

        }



        return vote_up;
    }


    public  HashMap<String, String> disLikeArticles(String emailId, String linkId,float longitude,float latitude,String country) {
        List<String> likeInterestData = new ArrayList<String>();

        //String likeInterestDataSql="select voted from google_fav_like where google_fav_id=:id and user_id=(select id from user where email=:email)";
        HashMap<String, Object> parameters4 = new HashMap<String, Object>();
        parameters4.put("id", linkId);
        parameters4.put("email", emailId);
        DataSource ds = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
        PreparedStatement pstmt1 = null;
        PreparedStatement pstmt2 = null;
        ResultSet rs = null;
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        String userId="";

        System.out.println("inside of likeArticles");
        try {
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();

            String ex1 = "select idUser from user where email='" + emailId + "'";
            System.out.println(ex1);
            pstmt2 = conn.prepareStatement(ex1);
            rs2 = pstmt2.executeQuery();
            System.out.println("next--->"+rs2.next()+rs2.getString("idUser"));
            userId=rs2.getString("idUser");

            //String likeInterestDataSql="select voted from link_votes where linkId=:id and user_id=(select id from user where email=:email)";
            String ex = "select voted from link_votes where link_id='" + linkId + "' and user_id='"+ userId+ "'";
            System.out.println(ex);
            pstmt = conn.prepareStatement(ex);
            rs = pstmt.executeQuery();

            while (rs.next()) {


                likeInterestData.add(0, rs.getString("voted"));


            }
        } catch (Exception var25) {
            var25.printStackTrace();
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }

                if (pstmt != null) {
                    pstmt.close();
                }

                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException var24) {
                var24.printStackTrace();
            }

        }


        int numberOfVotesUp = 0;
        System.out.println("num"+numberOfVotesUp);
        if (likeInterestData.size()>0) {
            numberOfVotesUp = Integer.parseInt(likeInterestData.get(0));
        }
        System.out.println("num"+numberOfVotesUp);
        if (likeInterestData.size()>0) {

            System.out.println("userId in deleye--->"+userId);
            try {
                ds = DBConnection.getDataSource();
                conn = ds.getConnection();
                //String likeInterestDataSql="select voted from link_votes where linkId=:id and user_id=(select id from user where email=:email)";
                String ex = "delete from link_votes where link_id='" + linkId + "' and user_id='" + userId + "' ";
                pstmt = conn.prepareStatement(ex);
                int t = pstmt.executeUpdate();
                System.out.println("inside delete--->"+ex);
                System.out.println("inside delete--->"+t);

            } catch (Exception var25) {
                var25.printStackTrace();
            } finally {
                try {
                    if (conn != null) {
                        conn.close();
                    }

                    if (pstmt != null) {
                        pstmt.close();
                    }

                    if (rs != null) {
                        rs.close();
                    }
                } catch (SQLException var24) {
                    var24.printStackTrace();
                }

            }
        }
        if (numberOfVotesUp == 0) {
            try {
                ds = DBConnection.getDataSource();
                conn = ds.getConnection();
                //String likeInterestDataSql="select voted from link_votes where linkId=:id and user_id=(select id from user where email=:email)";
                String ex = "update link set vote_down=vote_down+1 where idlink='" + linkId + "'";

                pstmt = conn.prepareStatement(ex);
                int t = pstmt.executeUpdate();
                System.out.println("ex--"+ex);
                System.out.println("ex--"+rs);


                //String likeInterestDataSql="select voted from link_votes where linkId=:id and user_id=(select id from user where email=:email)";
                String ex1 = "select idUser from user where email='" + emailId + "'";
                System.out.println(ex1);
                pstmt2 = conn.prepareStatement(ex1);
                rs2 = pstmt2.executeQuery();
                System.out.println("next--->"+rs2.next()+rs2.getString("idUser"));
                userId=rs2.getString("idUser");
                /*while (rs2.next()) {
                    System.out.println("bookmark title if" + rs2.getString("idUser"));
                    //likeInterestData.set(0, rs2.getString("id"));
                    userId=rs2.getString("idUser");
                    System.out.println("bookmark title" + rs2.getString("idUser"));

                }*/


                String SQL2 = "insert into link_votes(link_id,user_id,voted,longitude,latitude, country) values ('" + linkId + "','"+ userId+"','-1','"+longitude+"','"+latitude+"','"+country+"')";
                pstmt1 = conn.prepareStatement(SQL2);
                int r1 = pstmt1.executeUpdate();
                System.out.println("ex--"+SQL2);
                System.out.println("ex--"+r1);

            } catch (Exception var25) {
                var25.printStackTrace();
            } finally {
                try {
                    if (conn != null) {
                        conn.close();
                    }

                    if (pstmt != null) {
                        pstmt.close();
                    }

                    if (rs != null) {
                        rs.close();
                    }
                } catch (SQLException var24) {
                    var24.printStackTrace();
                }

            }


        }

        else if(numberOfVotesUp==1) {

            try {
                ds = DBConnection.getDataSource();
                conn = ds.getConnection();
                //String likeInterestDataSql="select voted from link_votes where linkId=:id and user_id=(select id from user where email=:email)";
                String ex = "update  link set vote_up=vote_up-1 where idlink='" + linkId + "'";
                pstmt = conn.prepareStatement(ex);
                int rs4 = pstmt.executeUpdate();

                String ex1 = "update  link set vote_down=vote_down+1 where idlink='" + linkId + "'";
                pstmt = conn.prepareStatement(ex1);
                pstmt.executeUpdate();

                String ex2 = "select idUser from user where email='" + emailId + "'";
                System.out.println(ex2);
                pstmt = conn.prepareStatement(ex2);
                rs = pstmt.executeQuery();
                System.out.println("next--->"+rs.next());

                userId=rs.getString("idUser");  userId=rs.getString("idUser");
                /*while (rs.next()) {
                    System.out.println("bookmark title if" + rs.getString("idUser"));
                    //likeInterestData.set(0, rs.getString("idUser"));
                    userId=rs.getString("idUser");
                    System.out.println("bookmark title" + rs.getString("idUser"));

                }*/

                String SQL2 = "insert into link_votes(link_id,user_id,voted, longitude,latitude,country) values ('" + linkId + "','"+ userId+"','-1','"+longitude+"','"+latitude+"','"+country+"')";

                pstmt1 = conn.prepareStatement(SQL2);
                pstmt1.executeUpdate();
            } catch (Exception var25) {
                var25.printStackTrace();
            } finally {
                try {
                    if (conn != null) {
                        conn.close();
                    }

                    if (pstmt != null) {
                        pstmt.close();
                    }

                    if (rs != null) {
                        rs.close();
                    }
                } catch (SQLException var24) {
                    var24.printStackTrace();
                }
            }

        }
        else  {

            try {
                ds = DBConnection.getDataSource();
                conn = ds.getConnection();
                //String likeInterestDataSql="select voted from link_votes where linkId=:id and user_id=(select id from user where email=:email)";
                String ex = "update  link set vote_down=vote_down-1 where idlink='" + linkId + "'";
                pstmt = conn.prepareStatement(ex);
                int t1 = pstmt.executeUpdate();


            } catch (Exception var25) {
                var25.printStackTrace();
            } finally {
                try {
                    if (conn != null) {
                        conn.close();
                    }

                    if (pstmt != null) {
                        pstmt.close();
                    }

                    if (rs != null) {
                        rs.close();
                    }
                } catch (SQLException var24) {
                    var24.printStackTrace();
                }

            }
        }
        //String getVotesUpSql = "select vote_up,vote_down from google_fav where id=:id";
        HashMap<String,String> vote_up = new HashMap<String, String>();


        try {
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            //String likeInterestDataSql="select voted from link_votes where linkId=:id and user_id=(select id from user where email=:email)";
            String ex = "select vote_up,vote_down from link where idlink='" + linkId + "'";
            pstmt = conn.prepareStatement(ex);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                System.out.println("vote_up" + rs.getString("vote_up"));
                System.out.println("vote_up" + rs.getString("vote_down"));

                vote_up.put("vote_up", rs.getString("vote_up"));
                vote_up.put("vote_down", rs.getString("vote_down"));

            }
        } catch (Exception var25) {
            var25.printStackTrace();
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }

                if (pstmt != null) {
                    pstmt.close();
                }

                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException var24) {
                var24.printStackTrace();
            }

        }



        return vote_up;
    }
    public List<HashMap<String,String>> getMapDetailsForArticle(String linkId)
    {



        DataSource ds = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        PreparedStatement pstmt2 = null;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TimeZone istTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
        java.util.Date d = new java.util.Date();
        sdf.setTimeZone(istTimeZone);
        String date1 = sdf.format(d);


        List<HashMap<String,String>> voteDetails = new ArrayList<>();

        try {
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            //String likeInterestDataSql="select voted from link_votes where linkId=:id and user_id=(select id from user where email=:email)";
            //String ex = " select t.voted,t.country,MAX(t.count) as count,t.longitude,t.latitude from (SELECT voted,country,COUNT(*)as count,longitude,latitude  FROM link_votes where link_id='"+linkId+"' GROUP BY voted,country ) as t group by t.country";
            String ex="SELECT voted,country,COUNT(*)as count,longitude,latitude  FROM link_votes where link_id="+linkId+" and longitude!=0 and latitude!=0 GROUP BY voted,country";
            pstmt = conn.prepareStatement(ex);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                HashMap<String,String> vote_up = new HashMap<String, String>();

                System.out.println("voted---"+rs.getString("voted"));
                vote_up.put("voted", rs.getString("voted"));
                vote_up.put("country", rs.getString("country"));
                vote_up.put("count", rs.getString("count"));
                vote_up.put("longitude", rs.getString("longitude"));
                vote_up.put("latitude", rs.getString("latitude"));


                voteDetails.add(vote_up);

            }
        } catch (Exception var25) {
            var25.printStackTrace();
        }
        finally {
            try {
                if (conn != null) {
                    conn.close();
                }

                if (pstmt != null) {
                    pstmt.close();
                }

                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException var24) {
                var24.printStackTrace();
            }
        }




        return voteDetails;
    }

    public List<Link> getTrendingLinks(int currUser, String language,
                                       String category, String country,int descriptionLength) {
        List<Link> links = new ArrayList<Link>();

        DataSource ds = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TimeZone istTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
        java.util.Date d = new java.util.Date();
        sdf.setTimeZone(istTimeZone);
        String date1 = sdf.format(d);


        try {


            // SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v,timestampdiff(HOUR, a.date_created, now()) as hours,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId=" + currUser + " ) as bookmark from link a left join link_user c on a.idlink=c.linkId and c.userId=" + currUser + " where spam < 4 and deleted=0 and publish=1 and timestampdiff(HOUR, a.date_created, now()) <= 50 order by a.date_created desc limit 0,20"
            //String  SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v, TIMEDIFF('"+date1+"',date_created)as hours,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId=" + currUser + " ) as bookmark from link a left join link_user c on a.idlink=c.linkId and c.userId=" + currUser + " where spam < 4 and deleted=0 order by a.date_created desc limit 0,20";
            // Language only filter

            String SQL="select idlink,url,title,category,(select user_id from web_bookmark where link_id=idlink and user_id='"+currUser+"')as userId,domain,description,image_url,vote_up,vote_down,likes,date_created,(select voted from link_votes where link_id=idlink and user_id='"+currUser +"')as voted,(select distinct isBookmarked from web_bookmark where link_id=idlink and user_id='"+currUser +"')as bookmarked,(select distinct no_of_likes from like_user where link_id=idlink)as liked,TIMEDIFF('"+date1+"',date_created)as hours from link where (media_type='Text/HTML' and  date_created >= ( CURDATE() - INTERVAL 1 DAY )) GROUP BY title order by  LOG10(ABS(vote_up - vote_down) ) * (SIGN(vote_up - vote_down) + (UNIX_TIMESTAMP(ads_publish_date)-1640998800)) / 45000  DESC,date_created desc limit 0,50";
           // String SQL="select idlink,url,title,category,(select user_id from web_bookmark where link_id=idlink and user_id='"+currUser+"')as userId,domain,description,image_url,vote_up,vote_down,likes,date_created,(select voted from link_votes where link_id=idlink and user_id='"+currUser +"')as voted,(select distinct isBookmarked from web_bookmark where link_id=idlink and user_id='"+currUser +"')as bookmarked,(select distinct no_of_likes from like_user where link_id=idlink)as liked,TIMEDIFF('"+date1+"',date_created)as hours from link where (media_type='Text/HTML' and  date_created >= ( CURDATE() - INTERVAL 1 DAY ))  order by  LOG10(ABS(vote_up - vote_down) ) * (SIGN(vote_up - vote_down) + (UNIX_TIMESTAMP(ads_publish_date)-1640998800)) / 45000  DESC,date_created desc limit 0,50";

            logger.info(SQL);

            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            stmt = conn.createStatement();

            rs = stmt.executeQuery(SQL);

            while (rs.next()) {
                Link link = new Link();

                String desc=rs.getString("description");

                int descLength=desc.length();
                //System.out.println("length is"+descLength);

                System.out.println("inside des leg");
                link.setUrl(rs.getString("url"));
                link.setTitle(rs.getString("title"));
                link.setLinkId(rs.getInt("idlink"));
                link.setUserId(rs.getString("userId"));

              //  link.setHoursElapsed((rs.getLong("hours")));

                //For Developement
                Date date= sdf.parse(date1);
                Date date2 = sdf.parse(rs.getString("date_created"));
                // Calucalte time difference in milliseconds
                long time_difference = date.getTime() - date2.getTime();
                System.out.println("difference is"+time_difference);
                long hours_difference = (time_difference / (1000*60*60)) % 24;


                link.setHoursElapsed(hours_difference);
                //end of hours for dev

                link.setLikes(rs.getInt("likes"));
                link.setLiked(rs.getInt("liked"));
                link.setDomain(rs.getString("domain"));
                link.setDesc(rs.getString("description"));
                link.setBookmark(rs.getInt("bookmarked"));

                link.setSubmitedBy("GaramGaram");
                link.setImageUrl(rs.getString("image_url"));
                link.setVote_up(rs.getInt("vote_up"));
                link.setVote_down(rs.getInt("vote_down"));
                link.setCategory(rs.getString("category"));
                //System.out.println("voted--->"+rs.getString("voted"));
                if(rs.getString("voted")==null)
                {
                    link.setUserVoted("0");
                }
                else {
                    link.setUserVoted(rs.getString("voted"));
                }
                link.setDateCreated(rs.getTimestamp("date_created"));




                String cat=rs.getString("category");
                if(cat.equals("इंडिया")){
                    cat="india";
                }if(cat.equals("दुनिया")){
                    System.out.println("Inside of World Category");
                    cat="world";
                }
                if(cat.equals("खेल")){cat="sports";}
                if(cat.equals("सामान्य समाचार")){cat="general";}
                if(cat.equals("मनोरंजन")){cat="cinema";}
                link.setCategory2(cat);

                String newsSource="";

                if(rs.getString("domain").equals("hindi.oneindia.com"))
                {
                    newsSource="हिंदी वनइंडिया";
                }
                if(rs.getString("domain").equals("hindi.news18.com"))
                {
                    newsSource="हिंदी न्यूज़ 18";
                }
                if(rs.getString("domain").equals("livehindustan.com"))
                {
                    newsSource="लाइव हिंदुस्तान";
                }
                if(rs.getString("domain").equals("amarujala.com")) {
                    newsSource = "अमर उजाला";
                }
                if(rs.getString("domain").equals("hindi.filmibeat.com")) {
                    newsSource = "हिंदी फिल्मीबीट";
                }
                if(rs.getString("domain").equals("zeenews.india.com")) {
                    newsSource = "जी नेवस";
                }
                if(rs.getString("domain").equals("bbc.com")) {
                    newsSource = "बीबीसी";
                }
                if(rs.getString("domain").equals("naidunia.com")) {
                    newsSource = "नईदुनिया";
                }
                if(rs.getString("domain").equals("prabhasakshi.com")) {
                    newsSource = "प्रभासाक्षी";
                }
                if(rs.getString("domain").equals("dabangdunia.co")) {
                    newsSource = "दबंगदुनिया";
                }

                link.setDomain(newsSource);
                String dom=link.getDomain();
                if(dom.equals("हिंदी वनइंडिया")){ dom="hindi.oneindia.com"; }
                if(dom.equals("हिंदी न्यूज़ 18")){dom="hindi.news18.com";}
                if(dom.equals("लाइव हिंदुस्तान")){dom="livehindustan.com";}
                if(dom.equals("अमर उजाला")){dom="amarujala.com";}
                if(dom.equals("हिंदी फिल्मीबीट")){dom="hindi.filmibeat.com";}
                if(dom.equals("जी नेवस")){dom="zeenews.india.com";}
                if(dom.equals("बीबीसी")){dom="bbc.com";}
                if(dom.equals("नईदुनिया")){dom="naidunia.com";}
                if(dom.equals("प्रभासाक्षी")){dom="prabhasakshi.com";}
                if(dom.equals("दबंगदुनिया")){dom="dabangdunia.co";}
                link.setDomain2(dom);
                links.add(link);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null)
                    conn.close();
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("success fetched....."+links.size());
        return links;
    }
    public void saveBookmark(String linkId,String emailId)
    {
        List<Link> links = new ArrayList<Link>();
        int bookmark=0;
        DataSource ds = null;
        Connection conn = null;
        Statement stmt = null;
        String url="";
        String res="";
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TimeZone istTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
        java.util.Date d = new java.util.Date();
        sdf.setTimeZone(istTimeZone);
        String date = sdf.format(d);
        System.out.println("timestamp---->"+date);
        try {


            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String SQL="insert into web_bookmark values(0,'"+emailId+"','"+linkId+"',1,'"+date+"')";
            logger.info("SQL:" + SQL);
            stmt.executeUpdate(SQL);


            System.out.println("bookmark inserted successfully.....");

        } catch (Exception ex) {
            ex.printStackTrace();
            res="Delete failure";
            logger.error(res);
        }
        finally {
            try {

                if (conn != null)
                    conn.close();
                if (pstmt != null)
                    pstmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }
    public List<Link> getReadLaterArticle(String emailId,int currUser) {


        System.out.println("Getting Bookmarks for Mobile");
        ArrayList links = new ArrayList();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TimeZone istTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
        Date d = new Date();
        sdf.setTimeZone(istTimeZone);
        String date1 = sdf.format(d);
        System.out.println("timestamp---->" + date1);
        long millis = System.currentTimeMillis();
        new java.sql.Date(millis);
        DataSource ds = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            String ex = "select l.idlink,l.userId,vote_up,vote_down,(select distinct isBookmarked from web_bookmark where link_id=l.idlink)as bookmarked,(select username from user where l.userId=idUser)as submitedBy,(select voted from link_votes where link_id=idlink and user_id='"+currUser +"')as voted,l.title,l.url,l.description,l.date_created,l.domain,l.category,l.image_url,b.date_created as bookmarkCreated from link l left join web_bookmark b on l.idlink=b.link_id where b.user_id='"+emailId+"' order by b.date_created desc";
            pstmt = conn.prepareStatement(ex);
            rs = pstmt.executeQuery();

            while(rs.next()) {
                Link link = new Link();
                link.setUrl(rs.getString("url"));
                link.setTitle(rs.getString("title"));
                link.setUserId(rs.getString("userId"));
                link.setLinkId(rs.getInt("idlink"));
                link.setDesc(rs.getString("description"));
                link.setDomain(rs.getString("domain"));
                link.setImageUrl(rs.getString("image_url"));
                link.setDateCreated(rs.getDate("bookmarkCreated"));
                link.setCategory(rs.getString("category"));
                link.setVote_up(rs.getInt("vote_up"));
                link.setVote_down(rs.getInt("vote_down"));
                link.setBookmark(rs.getInt("bookmarked"));
                //System.out.println("voted--->"+rs.getString("voted"));
                if(rs.getString("voted")==null)
                {
                    link.setUserVoted("0");
                }
                else {
                    link.setUserVoted(rs.getString("voted"));
                }

                String cat=rs.getString("category");
                if(cat.equals("इंडिया")){
                    cat="india";
                }if(cat.equals("दुनिया")){
                    System.out.println("Inside of World Category");
                    cat="world";
                }
                if(cat.equals("खेल")){cat="sports";}
                if(cat.equals("सामान्य समाचार")){cat="general";}
                if(cat.equals("मनोरंजन")){cat="cinema";}
                link.setCategory2(cat);

                String newsSource="";

                if(rs.getString("domain").equals("hindi.oneindia.com"))
                {
                    newsSource="हिंदी वनइंडिया";
                }
                if(rs.getString("domain").equals("hindi.news18.com"))
                {
                    newsSource="हिंदी न्यूज़ 18";
                }
                if(rs.getString("domain").equals("livehindustan.com"))
                {
                    newsSource="लाइव हिंदुस्तान";
                }
                if(rs.getString("domain").equals("amarujala.com")) {
                    newsSource = "अमर उजाला";
                }
                if(rs.getString("domain").equals("hindi.filmibeat.com")) {
                    newsSource = "हिंदी फिल्मीबीट";
                }
                if(rs.getString("domain").equals("zeenews.india.com")) {
                    newsSource = "जी नेवस";
                }
                if(rs.getString("domain").equals("bbc.com")) {
                    newsSource = "बीबीसी";
                }
                if(rs.getString("domain").equals("naidunia.com")) {
                    newsSource = "नईदुनिया";
                }
                if(rs.getString("domain").equals("prabhasakshi.com")) {
                    newsSource = "प्रभासाक्षी";
                }
                if(rs.getString("domain").equals("dabangdunia.co")) {
                    newsSource = "दबंगदुनिया";
                }

                link.setDomain(newsSource);
                String dom=link.getDomain();
                if(dom.equals("हिंदी वनइंडिया")){ dom="hindi.oneindia.com"; }
                if(dom.equals("हिंदी न्यूज़ 18")){dom="hindi.news18.com";}
                if(dom.equals("लाइव हिंदुस्तान")){dom="livehindustan.com";}
                if(dom.equals("अमर उजाला")){dom="amarujala.com";}
                if(dom.equals("हिंदी फिल्मीबीट")){dom="hindi.filmibeat.com";}
                if(dom.equals("जी नेवस")){dom="zeenews.india.com";}
                if(dom.equals("बीबीसी")){dom="bbc.com";}
                if(dom.equals("नईदुनिया")){dom="naidunia.com";}
                if(dom.equals("प्रभासाक्षी")){dom="prabhasakshi.com";}
                if(dom.equals("दबंगदुनिया")){dom="dabangdunia.co";}
                link.setDomain2(dom);

                if(rs.getString("userId").equals("999"))
                {
                    link.setSubmitedBy("ChudaChuda");
                }
                else
                {
                    link.setSubmitedBy(rs.getString("submitedBy"));
                }
                links.add(link);
            }
        } catch (Exception var25) {
            var25.printStackTrace();
        } finally {
            try {
                if(conn != null) {
                    conn.close();
                }

                if(pstmt != null) {
                    pstmt.close();
                }

                if(rs != null) {
                    rs.close();
                }
            } catch (SQLException var24) {
                var24.printStackTrace();
            }

        }

        return links;
    }


    public List<Link> getRecommendationLinks(int currUser, int descriptionLength,int compareLimit) {
        List<Link> links = new ArrayList<Link>();
        System.out.println("curr user --->"+currUser);
        DataSource ds = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TimeZone istTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
        java.util.Date d = new java.util.Date();
        sdf.setTimeZone(istTimeZone);
        String date1 = sdf.format(d);


        try {


            // SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v,timestampdiff(HOUR, a.date_created, now()) as hours,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId=" + currUser + " ) as bookmark from link a left join link_user c on a.idlink=c.linkId and c.userId=" + currUser + " where spam < 4 and deleted=0 and publish=1 and timestampdiff(HOUR, a.date_created, now()) <= 50 order by a.date_created desc limit 0,20"
            //String  SQL = "select distinct (select b.username from user b where a.userId=b.idUser) as username,(select group_concat(tag_name separator ',') from tag t, link_tag lt where lt.link_id=a.idlink and t.tag_id=lt.tag_id) as tags, a.*, c.userId as voted, a.votes as v, TIMEDIFF('"+date1+"',date_created)as hours,(select userId from bookmark lu1 where lu1.linkId=a.idlink and lu1.userId=" + currUser + " ) as bookmark from link a left join link_user c on a.idlink=c.linkId and c.userId=" + currUser + " where spam < 4 and deleted=0 order by a.date_created desc limit 0,20";
            // Language only filter

            //String SQL="select l.idlink,l.url,l.title,l.category,(select user_id from web_bookmark where link_id=l.idlink and user_id='"+currUser+"')as userId,l.domain,l.description,l.image_url,l.vote_up,l.vote_down,l.likes,l.date_created,(select voted from link_votes where link_id=l.idlink and user_id='"+currUser +"')as voted,(select distinct isBookmarked from web_bookmark where link_id=l.idlink and user_id='"+currUser +"')as bookmarked,(select distinct no_of_likes from like_user where link_id=l.idlink)as liked,TIMEDIFF('"+date1+"',l.date_created)as hours from link l inner join link_votes lv on l.idlink=lv.link_id where l.idlink in (select  distinct link_id  from link_votes where link_id not in (select link_id from link_votes where user_id='"+currUser +"') and date_created >= ( CURDATE() - INTERVAL 1 DAY ) and user_id in (select user_id from link_votes where  user_id !='"+currUser+"' and link_id in (select link_id from link_votes where user_id='"+currUser +"'))) group by l.title order by l.date_created desc limit 0,50";


            String SQL="select l.idlink,l.url,l.title,l.category,(select user_id from web_bookmark where link_id=l.idlink and user_id='"+currUser+"')as userId,l.domain,l.description,l.image_url,l.vote_up,l.vote_down,l.likes,l.date_created,(select voted from link_votes where link_id=l.idlink and user_id='"+currUser+"')as voted,(select distinct isBookmarked from web_bookmark where link_id=l.idlink and user_id='"+currUser+"')as bookmarked,(select distinct no_of_likes from like_user where link_id=l.idlink)as liked,TIMEDIFF('"+date1+"',l.date_created)as hours from link l inner join link_votes lv on l.idlink=lv.link_id where l.date_created >= ( CURDATE() - INTERVAL 1 DAY ) and l.idlink in ( select distinct link_id from link_votes where link_id not in ( select link_id from link_votes where user_id='"+currUser+"') and user_id in (select f.user_id from link_votes t left join link_votes f on t.link_id=f.link_id where t.user_id!=f.user_id and t.user_id='"+currUser+"'   group by f.user_id having count(f.user_id) >="+compareLimit+" )) group by l.title limit 0,50";
          //  String SQL="select l.idlink,l.url,l.title,l.category,(select user_id from web_bookmark where link_id=l.idlink and user_id='"+currUser+"')as userId,l.domain,l.description,l.image_url,l.vote_up,l.vote_down,l.likes,l.date_created,(select voted from link_votes where link_id=l.idlink and user_id='"+currUser+"')as voted,(select distinct isBookmarked from web_bookmark where link_id=l.idlink and user_id='"+currUser+"')as bookmarked,(select distinct no_of_likes from like_user where link_id=l.idlink)as liked,TIMEDIFF('"+date1+"',l.date_created)as hours from link l inner join link_votes lv on l.idlink=lv.link_id where l.date_created >= ( CURDATE() - INTERVAL 1 DAY ) and l.idlink in ( select distinct link_id from link_votes where link_id not in ( select link_id from link_votes where user_id='"+currUser+"') and user_id in (select f.user_id from link_votes t left join link_votes f on t.link_id=f.link_id where t.user_id!=f.user_id and t.user_id='"+currUser+"'   group by f.user_id having count(f.user_id) >="+compareLimit+" ))  limit 0,50";

            logger.info(SQL);

            ds = DBConnection.getDataSource();
            conn = ds.getConnection();
            stmt = conn.createStatement();

            rs = stmt.executeQuery(SQL);

            while (rs.next()) {
                Link link = new Link();

                String desc=rs.getString("description");


                    System.out.println("inside des leg");
                    link.setUrl(rs.getString("url"));
                    link.setTitle(rs.getString("title"));
                    link.setLinkId(rs.getInt("idlink"));
                    link.setUserId(rs.getString("userId"));

                 //   link.setHoursElapsed((rs.getLong("hours")));

                //For Developement
                Date date= sdf.parse(date1);
                Date date2 = sdf.parse(rs.getString("date_created"));
                // Calucalte time difference in milliseconds
                long time_difference = date.getTime() - date2.getTime();

                long hours_difference = (time_difference / (1000*60*60)) % 24;


                link.setHoursElapsed(hours_difference);
                //end of hours for dev
                    link.setLikes(rs.getInt("likes"));
                    link.setLiked(rs.getInt("liked"));
                    link.setDomain(rs.getString("domain"));
                    link.setDesc(rs.getString("description"));
                    link.setBookmark(rs.getInt("bookmarked"));

                    link.setSubmitedBy("GaramGaram");
                    link.setImageUrl(rs.getString("image_url"));
                    link.setVote_up(rs.getInt("vote_up"));
                    link.setVote_down(rs.getInt("vote_down"));
                    link.setCategory(rs.getString("category"));
                    //System.out.println("voted--->"+rs.getString("voted"));
                    if(rs.getString("voted")==null)
                    {
                        link.setUserVoted("0");
                    }
                    else {
                        link.setUserVoted(rs.getString("voted"));
                    }
                    link.setDateCreated(rs.getTimestamp("date_created"));

                link.setSubmitedBy("GaramGaram");
                String cat=rs.getString("category");
                if(cat.equals("इंडिया")){
                    cat="india";
                }if(cat.equals("दुनिया")){
                    System.out.println("Inside of World Category");
                    cat="world";
                }
                if(cat.equals("खेल")){cat="sports";}
                if(cat.equals("सामान्य समाचार")){cat="general";}
                if(cat.equals("मनोरंजन")){cat="cinema";}
                link.setCategory2(cat);

                String newsSource="";

                if(rs.getString("domain").equals("hindi.oneindia.com"))
                {
                    newsSource="हिंदी वनइंडिया";
                }
                if(rs.getString("domain").equals("hindi.news18.com"))
                {
                    newsSource="हिंदी न्यूज़ 18";
                }
                if(rs.getString("domain").equals("livehindustan.com"))
                {
                    newsSource="लाइव हिंदुस्तान";
                }
                if(rs.getString("domain").equals("amarujala.com")) {
                    newsSource = "अमर उजाला";
                }
                if(rs.getString("domain").equals("hindi.filmibeat.com")) {
                    newsSource = "हिंदी फिल्मीबीट";
                }
                if(rs.getString("domain").equals("zeenews.india.com")) {
                    newsSource = "जी नेवस";
                }
                if(rs.getString("domain").equals("bbc.com")) {
                    newsSource = "बीबीसी";
                }
                if(rs.getString("domain").equals("naidunia.com")) {
                    newsSource = "नईदुनिया";
                }
                if(rs.getString("domain").equals("prabhasakshi.com")) {
                    newsSource = "प्रभासाक्षी";
                }
                if(rs.getString("domain").equals("dabangdunia.co")) {
                    newsSource = "दबंगदुनिया";
                }

                link.setDomain(newsSource);
                String dom=link.getDomain();
                if(dom.equals("हिंदी वनइंडिया")){ dom="hindi.oneindia.com"; }
                if(dom.equals("हिंदी न्यूज़ 18")){dom="hindi.news18.com";}
                if(dom.equals("लाइव हिंदुस्तान")){dom="livehindustan.com";}
                if(dom.equals("अमर उजाला")){dom="amarujala.com";}
                if(dom.equals("हिंदी फिल्मीबीट")){dom="hindi.filmibeat.com";}
                if(dom.equals("जी नेवस")){dom="zeenews.india.com";}
                if(dom.equals("बीबीसी")){dom="bbc.com";}
                if(dom.equals("नईदुनिया")){dom="naidunia.com";}
                if(dom.equals("प्रभासाक्षी")){dom="prabhasakshi.com";}
                if(dom.equals("दबंगदुनिया")){dom="dabangdunia.co";}
                link.setDomain2(dom);
                links.add(link);

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (conn != null)
                    conn.close();
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("success fetched.....");
        return links;
    }
}
