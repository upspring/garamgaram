package com.sudasuda.app.dao;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import javax.sql.DataSource;

import com.sudasuda.app.db.DBConnection;
import com.sudasuda.app.domain.Link;
import com.sudasuda.app.domain.User;

public class UserDAO {

	public User getUser(String username) {

		User user = new User();

		DataSource ds = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			ds = DBConnection.getDataSource();
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String SQL = "select * from user where username='" + username + "'";
			System.out.println(SQL);
			rs = stmt.executeQuery(SQL);

			while (rs.next()) {
				user.setEmail(rs.getString("email"));
				user.setUserId(rs.getInt("idUser"));
				user.setPassword(rs.getString("password"));
				user.setUserName(rs.getString("username"));
				user.setPicture(rs.getString("picture"));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {

				if (conn != null)
					conn.close();
				if (stmt != null)
					stmt.close();
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return user;
	}
	public User getLoginUser(String email,String network) {

		User user = new User();
		System.out.println("given nt"+network);
		DataSource ds = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			ds = DBConnection.getDataSource();
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String SQL = "select * from user where email='" + email + "' and network='"+network+"'" ;
			System.out.println(SQL);
			rs = stmt.executeQuery(SQL);

			while (rs.next()) {
				user.setEmail(rs.getString("email"));
				user.setUserId(rs.getInt("idUser"));
				user.setPassword(rs.getString("password"));
				user.setUserName(rs.getString("username"));
				user.setPicture(rs.getString("picture"));
				System.out.println("network is"+rs.getString("network"));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {

				if (conn != null)
					conn.close();
				if (stmt != null)
					stmt.close();
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		System.out.println("---->"+user.getEmail());
		return user;
	}
	public User getUserByEmail(String email) {

		User user = new User();

		DataSource ds = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			ds = DBConnection.getDataSource();
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String SQL = "select * from user where lower(email)='" + email.toLowerCase() + "'";
			System.out.println(SQL);
			rs = stmt.executeQuery(SQL);

			while (rs.next()) {
				user.setEmail(rs.getString("email"));
				user.setUserId(rs.getInt("idUser"));
				user.setPassword(rs.getString("password"));
				user.setUserName(rs.getString("username"));
				user.setPicture(rs.getString("picture"));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {

				if (conn != null)
					conn.close();
				if (stmt != null)
					stmt.close();
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return user;
	}
	public List<User> getAllUser() {

		List<User> userList = new ArrayList<User>();
		DataSource ds = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			ds = DBConnection.getDataSource();
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String SQL = "select idUser,email,password,username,picture from user";
			System.out.println(SQL);
			rs = stmt.executeQuery(SQL);

			while (rs.next()) {
				System.out.println("inisde....users");
				User user=new User();
				user.setEmail(rs.getString("email"));
				user.setUserId(rs.getInt("idUser"));
				user.setPassword(rs.getString("password"));
				user.setUserName(rs.getString("username"));
				user.setPicture(rs.getString("picture"));
				//user.setDate_created(rs.getString("date_created"));
				userList.add(user);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {

				if (conn != null)
					conn.close();
				if (stmt != null)
					stmt.close();
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return userList;
	}
	
	public void addUser(String username, String email, String password, String network,String picture) {
		DataSource ds = null;
		Connection conn = null;
		Statement stmt = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		TimeZone istTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
		java.util.Date d = new java.util.Date();
		sdf.setTimeZone(istTimeZone);
		String date2 = sdf.format(d);

		System.out.println("Asia/Kolkatta time is-->"+date2);
		/*String SQL = "insert into user values (0,'" + username + "','" + email
				+ "','" + password + "','"+network+"','" + picture + "','"+date2+"')";*/

		String SQL = "insert into user values (0,'" + username + "','" + email
				+ "','" + password + "','"+network+"','" + picture + "')";
		try {
			ds = DBConnection.getDataSource();
			conn = ds.getConnection();
			stmt = conn.createStatement();

			stmt.executeUpdate(SQL);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (conn != null)
					conn.close();
				if (stmt != null)
					stmt.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	public int updatePassword(String email,String password)
	{

		int likes=0;
		DataSource ds = null;
		Connection conn = null;
		Statement stmt = null;

		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {


			ds = DBConnection.getDataSource();
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "update user set password='"+password+"' where email='"+email+"'";
			System.out.println("SQL: " + sql);

			stmt.executeUpdate(sql);


		} catch (Exception ex) {
			ex.printStackTrace();

		}
		finally {
			try {

				if (conn != null)
					conn.close();
				if (pstmt != null)
					pstmt.close();
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return likes;
	}

	public List<User> getTopPostersForDomain(String domain) {

		DataSource ds = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		List<User> users = new ArrayList<User>();

		try {

			ds = DBConnection.getDataSource();
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String SQL = "select distinct l.userId,u.* from link l, user u where l.userId=u.idUser and domain='"
					+ domain + "' order by votes desc limit 0,5";
			System.out.println(SQL);
			rs = stmt.executeQuery(SQL);

			while (rs.next()) {
				User user = new User();
				user.setEmail(rs.getString("email"));
				user.setUserId(rs.getInt("idUser"));
				// user.setPassword(rs.getString("password"));
				user.setUserName(rs.getString("username"));
				users.add(user);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {

				if (conn != null)
					conn.close();
				if (stmt != null)
					stmt.close();
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return users;
	}

	public User findUUID1(String uuid) {
		User user = null;

		DataSource ds = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {

			String SQL = "select * from user where uuid='" + uuid + "'";
			System.out.println(SQL);

			ds = DBConnection.getDataSource();
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(SQL);

			if (rs.next()) {
				user = new User();
				user.setEmail(rs.getString("email"));
				user.setUserId(rs.getInt("idUser"));
				user.setPassword(rs.getString("password"));
				user.setUserName(rs.getString("username"));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {

				if (conn != null)
					conn.close();
				if (stmt != null)
					stmt.close();
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return user;
	}

	public User findUUID(String uuid) {
		User user = null;

		DataSource ds = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			ds = DBConnection.getDataSource();
			conn = ds.getConnection();
			// conn =
			// DriverManager.getConnection("jdbc:mysql://localhost:3306/leaderread",
			// "root", "venkk23");
			stmt = conn.createStatement();

			String SQL = "select *,b.* from user_uuid a, user b where a.	uuid='"
					+ uuid + "' and a.username=b.username";
			System.out.println(SQL);
			rs = stmt.executeQuery(SQL);

			if (rs.next()) {
				user = new User();
				user.setEmail(rs.getString("email"));
				user.setUserId(rs.getInt("idUser"));
				user.setPassword(rs.getString("password"));
				user.setUserName(rs.getString("username"));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {

				if (conn != null)
					conn.close();
				if (stmt != null)
					stmt.close();
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return user;
	}

	public void saveUUID(String uuid, String username) {
		DataSource ds = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			String SQL = "update user set uuid='" + uuid
					+ "',last_updated=now() where username='" + username + "'";

			ds = DBConnection.getDataSource();
			conn = ds.getConnection();
			stmt = conn.createStatement();

			if (findUUID(uuid) == null)
				SQL = "insert into user_uuid values('" + uuid + "','"
						+ username + "',now())";
			stmt.executeUpdate(SQL);
			System.out.println("SQL: " + SQL);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {

				if (conn != null)
					conn.close();
				if (stmt != null)
					stmt.close();
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	public void deleteUUID(String username) {
		DataSource ds = null;
		Connection conn = null;
		Statement stmt = null;

		try {

			ds = DBConnection.getDataSource();
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String SQL = "delete from user_uuid where username='" + username
					+ "'";
			System.out.println("SQL:" + SQL);
			stmt.executeUpdate(SQL);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {

				if (conn != null)
					conn.close();
				if (stmt != null)
					stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

}
