package com.sudasuda.app.domain;



/**
 * Created by ponvandu on 3/17/2017.
 */
import java.util.Date;

public class AddLink {

    private int linkId;
    private int userId;
    private String url;
    private String title;
    private String submitedBy;
    private boolean voted;
    private boolean bookmarked;

    private long hoursElapsed;
    private int votes;
    private int noOfComments;
    private String domain;
    private String language;
    private String category;
    private String country;
    private String tags;
    private int spam;
    private int activists;
    private String imageUrl;
    private String desc;
    private  String ads_published_date;
    private Date dateCreated;
    private String media_type;
    private String video_format;
    private  long ads_timestamp;



    public int getLinkId() {
        return linkId;
    }

    public void setLinkId(int linkId) {
        this.linkId = linkId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getVotes() {
        return votes;
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }

    public String getSubmitedBy() {
        return submitedBy;
    }

    public void setSubmitedBy(String submitedBy) {
        this.submitedBy = submitedBy;
    }

    public boolean isVoted() {
        return voted;
    }
    public void setVoted(boolean voted) {
        this.voted = voted;
    }


    public boolean isBookmarked()
    {
        return bookmarked;
    }

    public void setBookmarked(boolean bookmarked) {
        this.bookmarked = bookmarked;
    }





    public long getHoursElapsed() {
        return hoursElapsed;
    }

    public void setHoursElapsed(long hoursElapsed) {
        this.hoursElapsed = hoursElapsed;
    }

    public int getNoOfComments() {
        return noOfComments;
    }

    public void setNoOfComments(int noOfComments) {
        this.noOfComments = noOfComments;
    }



    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public int getSpam() {
        return spam;
    }

    public void setSpam(int spam) {
        this.spam = spam;
    }

    public int getActivists() {
        return activists;
    }

    public void setActivists(int activists) {
        this.activists = activists;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getMedia_type() {
        return media_type;
    }

    public void setMedia_type(String media_type) {
        this.media_type = media_type;
    }

    public String getVideo_format() {
        return video_format;
    }

    public void setVideo_format(String video_format) {
        this.video_format = video_format;
    }

    public String getAds_published_date() {
        return ads_published_date;
    }

    public String setAds_published_date(String ads_published_date) {
        return  this.ads_published_date = ads_published_date;
    }

    public long getAds_timestamp() {
        return ads_timestamp;
    }

    public void setAds_timestamp(long ads_timestamp) {
        this.ads_timestamp = ads_timestamp;
    }
}

