package com.sudasuda.app.service;

import com.sudasuda.app.dao.AddLinkDAO;
import com.sudasuda.app.domain.Feed;
import com.sudasuda.app.domain.FeedMessage;
import com.sudasuda.app.domain.AddLink;
import org.apache.commons.feedparser.*;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
//import com.chudachuda.service.RSSFeedParser;

@Component("RssFeed")
@Service
public class RssFeedImpl  implements  RssFeed{
    private static final Logger logger = LoggerFactory
            .getLogger(RssFeedService.class);



    @Autowired(required = true)
    private AddLinkDAO linkDAO;
    String imageURL = null;
    String media_type="";
    String format="";
    String desc=null;
    String category="";
    String vikatanTitle="";
    HttpServletRequest request;

    /*public static void main(String[] args) {

                    RssFeedImpl.addRssFeed();
    }*/

    public synchronized void addRssFeed() throws Exception {

        logger.info("Rss Feeds Starts");


        List<String> rssFeedList = new ArrayList<String>();




     /*   rssFeedList.add("http://feeds.feedburner.com/Puthiyathalaimurai_World_News?fmt=xml");
        rssFeedList.add("https://www.vikatan.com/api/v1/collections/tamilnadu-news.rss");

        rssFeedList.add("http://feeds.feedburner.com/Puthiyathalaimurai_topnews?fmt=xml");
        rssFeedList.add("https://www.dinamani.com/%E0%AE%87%E0%AE%A8%E0%AF%8D%E0%AE%A4%E0%AE%BF%E0%AE%AF%E0%AE%BE/rssfeed/?id=337&getXmlFeed=true");
        rssFeedList.add("https://tamil.indianexpress.com/tamilnadu/feed/");
        rssFeedList.add("https://www.dinakaran.com/rss_news.asp?id=9");
        rssFeedList.add("https://www.dinakaran.com/rss_Latest.asp");
        rssFeedList.add("https://tamil.indianexpress.com/india/feed/");
        rssFeedList.add("https://zeenews.india.com/tamil/india.xml");
        rssFeedList.add("https://www.vikatan.com/api/v1/collections/india-news.rss");

        rssFeedList.add("https://tamil.oneindia.com/rss/tamil-news-fb.xml");
        rssFeedList.add("https://tamil.oneindia.com/rss/tamil-fb.xml");
        rssFeedList.add("https://tamil.indianexpress.com/feed/");

        rssFeedList.add("http://feeds.feedburner.com/Puthiyathalaimurai_Tamilnadu_News?fmt=xml");
        rssFeedList.add("https://zeenews.india.com/tamil/world.xml");
        rssFeedList.add("https://www.dinamani.com/%E0%AE%9A%E0%AE%BF%E0%AE%A9%E0%AE%BF%E0%AE%AE%E0%AE%BE/%E0%AE%9A%E0%AF%86%E0%AE%AF%E0%AF%8D%E0%AE%A4%E0%AE%BF%E0%AE%95%E0%AE%B3%E0%AF%8D/rssfeed/?id=485&getXmlFeed=true");

        rssFeedList.add("https://www.vikatan.com/api/v1/collections/international.rss");
        rssFeedList.add("https://tamil.indianexpress.com/entertainment/feed/");
        rssFeedList.add("https://ibctamil.com/rss");
        rssFeedList.add("https://ibctamil.com/rss/world");
        rssFeedList.add("https://zeenews.india.com/tamil/movies.xml");

        rssFeedList.add("https://www.dinamani.com/%E0%AE%B5%E0%AE%BF%E0%AE%B3%E0%AF%88%E0%AE%AF%E0%AE%BE%E0%AE%9F%E0%AF%8D%E0%AE%9F%E0%AF%81/%E0%AE%9A%E0%AF%86%E0%AE%AF%E0%AF%8D%E0%AE%A4%E0%AE%BF%E0%AE%95%E0%AE%B3%E0%AF%8D/rssfeed/?id=480&getXmlFeed=true");


        rssFeedList.add("http://rss.dinamalar.com/?cat=ara1");
        rssFeedList.add("https://api.hindutamil.in/rss/tamilnadu");
        rssFeedList.add("https://tamil.samayam.com/rssfeedsdefault.cms");

        rssFeedList.add("https://zeenews.india.com/tamil/tamil-nadu.xml");
        rssFeedList.add("http://feeds.bbci.co.uk/tamil/rss.xml#sa-link_location=story-body&intlink_from_url=https%3A%2F%2Fwww.bbc.com%2Ftamil%2Finstitutional%2F2011%2F08%2F000000_rss&intlink_ts=1551514105358-sa");
        rssFeedList.add("http://rss.dinamalar.com/?cat=pot1");
        rssFeedList.add("https://www.dinakaran.com/rss_news.asp?id=26");
        rssFeedList.add("https://zeenews.india.com/tamil/sports.xml");
        rssFeedList.add("https://feeds.feedburner.com/Puthiyathalaimurai_India_news?fmt=xml");
        rssFeedList.add("https://api.hindutamil.in/rss/india");
        rssFeedList.add("https://www.dinakaran.com/rss_news.asp?id=13");
        rssFeedList.add("http://rss.dinamalar.com/?cat=INL1");
        rssFeedList.add("https://api.hindutamil.in/rss/sports");
        rssFeedList.add("https://tamil.samayam.com/sports/rssfeedsection/47766652.cms");
        rssFeedList.add("http://cinema.dinakaran.com/Rss/RssKollywood.aspx");
        rssFeedList.add("http://cinema.dinakaran.com/Rss/RssBollywood.aspx");
        rssFeedList.add("https://api.hindutamil.in/rss/cinema");
        rssFeedList.add("https://tamil.filmibeat.com/rss/filmibeat-tamil-news-fb.xml");
          rssFeedList.add("https://www.vikatan.com/api/v1/collections/kollywood-entertainment.rss");*/
        rssFeedList.add("https://hindi.news18.com/rss/khabar/ajab-gajab/ajab-gajab.xml");
        rssFeedList.add("https://hindi.news18.com/rss/khabar/sports/others.xml");
        rssFeedList.add("https://feed.livehindustan.com/rss/3116");
        rssFeedList.add("https://feed.livehindustan.com/rss/3127");
        rssFeedList.add("https://hindi.oneindia.com/rss/hindi-news-fb.xml");
        rssFeedList.add("https://hindi.oneindia.com/rss/hindi-city-fb.xml");
        rssFeedList.add("https://hindi.oneindia.com/rss/hindi-fb.xml");
        rssFeedList.add("https://hindi.filmibeat.com/rss/filmibeat-hindi-fb.xml");
        rssFeedList.add("https://www.amarujala.com/rss/breaking-news.xml");
        rssFeedList.add("https://zeenews.india.com/hindi/india.xml");
        rssFeedList.add("https://zeenews.india.com/hindi/world.xml");
        rssFeedList.add("https://zeenews.india.com/hindi/sports.xml");
        rssFeedList.add("https://feeds.bbci.co.uk/hindi/rss.xml");
        rssFeedList.add("http://rss.jagran.com/naidunia/entertainment.xml");
        rssFeedList.add("http://rss.jagran.com/naidunia/topnews.xml");
        rssFeedList.add("http://rss.jagran.com/naidunia/national.xml");
        rssFeedList.add("https://www.prabhasakshi.com/rss/national");
        rssFeedList.add("https://www.prabhasakshi.com/rss/international");
        rssFeedList.add("https://www.prabhasakshi.com/rss/sport");
        rssFeedList.add("https://www.prabhasakshi.com/rss/entertainment");
        rssFeedList.add("https://www.dabangdunia.co/feed.aspx?cat_id=2");
        rssFeedList.add("https://www.dabangdunia.co/feed.aspx?cat_id=4");
        rssFeedList.add(" https://www.dabangdunia.co/feed.aspx?cat_id=75");
        rssFeedList.add("https://www.dabangdunia.co/feed.aspx?cat_id=76");




        /*  1. https://www.dinakaran.com/rss_news.asp?id=9
       2. https://www.dinakaran.com/rss_Latest.asp
       3. https://tamil.oneindia.com/rss/tamil-news-fb.xml --date
       4. https://tamil.oneindia.com/rss/tamil-fb.xml--date
       5. http://rss.dinamalar.com/?cat=ara1 --date
        6.https://api.hindutamil.in/rss/tamilnadu
       7. https://tamil.samayam.com/rssfeedsdefault.cms---no date
       8. http://feeds.bbci.co.uk/tamil/rss.xml#sa-link_location=story-body&intlink_from_url=https%3A%2F%2Fwww.bbc.com%2Ftamil%2Finstitutional%2F2011%2F08%2F000000_rss&intlink_ts=1551514105358-sa --date
       9. http://rss.dinamalar.com/?cat=pot1 --date
       10. https://www.dinakaran.com/rss_news.asp?id=26
       11. https://api.hindutamil.in/rss/india --nodate
        12.https://www.dinakaran.com/rss_news.asp?id=13
       13. http://rss.dinamalar.com/?cat=INL1--date
       14. https://api.hindutamil.in/rss/sports
       15. https://tamil.samayam.com/sports/rssfeedsection/47766652.cms--date
        16.http://cinema.dinakaran.com/Rss/RssKollywood.aspx
        17.http://cinema.dinakaran.com/Rss/RssBollywood.aspx
       18. https://api.hindutamil.in/rss/cinema
       19. https://tamil.filmibeat.com/rss/filmibeat-tamil-news-fb.xml --date*/

        for (String feedLink : rssFeedList) {
            System.out.println("RSS Source:" + feedLink);
            try {
                //main();
                System.setProperty("http.agent", "Chrome");
                getArticleDetails(feedLink);



            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        clearCache();
        logger.info("Rss Feeds End");

    }

    public void getArticleDetails(final String rssFeedLink) throws Exception {

        String channel = null;
        String imageURL = "";
        String desc = "";
        String lang = "Tamil";
        String country="India";
        String domain="";
        String timestamp="";

        RSSFeedParser parser = new RSSFeedParser(rssFeedLink);
        Feed feed = parser.readFeed();
        System.out.println("channel--->"+feed.getTitle());
        channel=feed.getTitle();
        if(channel.equals("Tamil Hindu - வீடியோ"))
        {
            channel="வீடியோ";
        }
        if(channel.equals("திரை பாடல்கள் - வீடியோ - Tamil Samayam"))
        {
            channel="Samayam-Video";
        }

        for (FeedMessage message : feed.getMessages()) {
            String url=message.getLink();
            String description=message.getDescription();
            String title=message.getTitle();
            String pubDate=message.getPubDate();
            System.out.println("url-->"+url);
            System.out.println("url-->"+title);
            System.out.println("url-->"+pubDate);
           long ads_timestamp=0;



            if(channel.contains("Puthiyathalaimurai - Tamil News | Latest Tamil News | Tamil News Online | Tamilnadu News"))
            {
                /*String dateString = pubDate;
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                Date date = dateFormat.parse(dateString );
                long unixTime = (long) date.getTime()/1000;
                ads_timestamp=unixTime;
                System.out.println("timestamp---> puth-->"+unixTime );*/

                String responseBody = null;
                responseBody = getBody(message.getLink());
                //System.out.println(responseBody);
                imageURL = getPuthiyathalaimuraiImgURL(responseBody);
                //desc = getPuthiyathalaimuraiDesURL(responseBody);
                desc = message.getDescription().replaceAll("\n", "");
                desc = (desc.length() > 400) ? desc.substring(0, 400).replaceAll("'", "") : desc.replaceAll("'", "");
                //byte[] bytes = desc.getBytes(StandardCharsets.ISO_8859_1);
                //desc = new String(bytes, StandardCharsets.UTF_8);

                System.out.println(desc);
                media_type="Text/HTML";
                format="";
                domain="puthiyathalaimurai.com";



                if(rssFeedLink.equals("http://feeds.feedburner.com/Puthiyathalaimurai_Tamilnadu_News?fmt=xml")){category="அரசியல்";}
                if(rssFeedLink.equals("https://feeds.feedburner.com/Puthiyathalaimurai_India_news?fmt=xml")){category="இந்தியா";}
                if(rssFeedLink.equals("http://feeds.feedburner.com/Puthiyathalaimurai_World_News?fmt=xml")){category="உலகம்";}
                if(rssFeedLink.equals("http://feeds.feedburner.com/Puthiyathalaimurai_topnews?fmt=xml")){category="தமிழகம்";}





                //desc=description;
                //System.out.println("dessss--->"+desc);
            }

            if (channel.contains("Dinakaran") || channel.contains("Tamil Samayam")||channel.contains("செய்திகள்")||channel.contains("FilmiBeat")||channel.contains("Vikatan.com")||channel.contains("City News")||channel.contains("World News")||channel.contains("Samayam-Video")||channel.contains("இந்து தமிழ் திசை")||channel.contains("Sri Lanka Tamil New")||
                    channel.contains("பொழுதுபோக்கு")||channel.contains("தமிழ்நாடு")||channel.contains("Oneindia")||channel.contains("மாலை மலர்")||channel.contains("இந்தியா")||channel.contains("தேசிய செய்திகள்")||channel.contains("Dinamani")|| channel.contains("Malaimurasu")|| channel.contains("dinamalar.com")||channel.contains("Sports News in Tamil") ||channel.contains("வீடியோ") || channel.contains("Tamil Hindu")|| channel.contains("India News") ||channel.contains("BBC News தமிழ் - முகப்பு")|| channel.contains("Cinema.Dinakaran")
                    ||channel.contains("Indian Express")  ||channel.contains("World News")||channel.contains("Movies News")||channel.contains("Sports News")||channel.contains("Tamil Nadu News")||channel.contains("மாவட்ட செய்திகள்")||channel.contains("தமிழ் சினிமா")||channel.contains("International")
                    ||channel.contains("News18 हिंदी") ||channel.contains("Dabang Dunia")||channel.contains("Prabhasakshi")||channel.contains("Khaskhabar")||channel.contains("Nai Dunia Hindi News")||channel.contains("NDTVKhabar.com")||channel.contains("BBC News हिंदी")||channel.contains("Zee News Hindi")||channel.contains("Movie News")||channel.contains("अन्य खेल")||channel.contains("Hindustan Rss feed")||channel.contains("आज के समाचार")||channel.contains("Amar Ujala")) {
                String responseBody = null;

                try {
                    System.out.println("inside ------------<");

                    responseBody = getResponseBody(url);
                    System.out.println("channel--->"+channel);


                    URI uri = new URI(url);

                    String domainName = uri.getHost();
                    System.out.println("domain"+domainName);
                    if (domainName != null) {
                        String domainSite = domainName.startsWith("www.") ? domainName.substring(4) : domainName;
                        if (domainSite == null) {
                            domain = uri.getHost();
                            //System.out.println("domain222"+domain);

                        } else {
                            domain = domainSite;
                            System.out.println("domain3333"+domain);
                            if(domain.equals("hindutamil.in"))
                            {
                                domain="tamil.thehindu.com";
                            }

                        }
                    }


                    // System.out.println("print the response body"+responseBody);
                    // boolean status=pingUrl();

                    //System.out.println("status------->"+responseBody);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                catch (URISyntaxException e)
                {

                }


                if (channel.contains("Dinakaran")) {

                    /*String dateString = pubDate;
                    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
                    Date date = dateFormat.parse(dateString );
                    long unixTime = (long) date.getTime()/1000;
                    System.out.println("timestamp--->"+unixTime );
                    ads_timestamp=unixTime;*/

                    imageURL = getDinakaranImgURL(responseBody);

                    System.out.println("New Source-->"+rssFeedLink);
                    if(rssFeedLink.equals("https://www.dinakaran.com/rss_news.asp?id=9")){category="அரசியல்";}
                    if(rssFeedLink.equals("https://www.dinakaran.com/rss_news.asp?id=26")){category="இந்தியா";}
                    if(rssFeedLink.equals("https://www.dinakaran.com/rss_news.asp?id=13")){category="உலகம்";}
                    if(rssFeedLink.equals("https://www.dinakaran.com/rss_Latest.asp")){category="தமிழகம்";}

                    desc = description.substring(description.indexOf("<p>") + 3, description.indexOf("</p>") - 3);
                    desc = (desc.length() > 400) ? desc.substring(0, 400).replaceAll("'", "") : desc.replaceAll("'", "");
                    media_type="Text/HTML";
                    format="";

                }


                else if(channel.contains("Cinema.Dinakaran"))
                {

                    if(rssFeedLink.equals("http://cinema.dinakaran.com/Rss/RssKollywood.aspx")){category="சினிமா";}
                    if(rssFeedLink.equals("http://cinema.dinakaran.com/Rss/RssKollywood.aspx")){category="சினிமா";}



                    imageURL = getDinakaranImgURL(responseBody);

                    desc = description.substring(description.indexOf("<p>") + 3, description.indexOf("</p>") - 3);
                    desc = (desc.length() > 400) ? desc.substring(0, 400).replaceAll("'", "") : desc.replaceAll("'", "");
                    media_type="Text/HTML";
                    format="";
                }
                else if(channel.contains("மாலை மலர்"))
                {

                    System.out.println("malai malr");
                    if( rssFeedLink.equals("https://www.maalaimalar.com/SectionRSS/news/topnews")){category="பொது செய்திகள்";}
                    if(rssFeedLink.equals("https://www.maalaimalar.com/SectionRSS/news/national")){category="இந்தியா";}
                    if(rssFeedLink.equals("https://www.maalaimalar.com/SectionRSS/news/world")){category="உலகம்";}
                    if( rssFeedLink.equals("https://www.maalaimalar.com/SectionRSS/news/state")){category="தமிழகம்";}
                    if(rssFeedLink.equals("https://www.maalaimalar.com/SectionRSS/cinema/cinemanews")){category="சினிமா";}
                    if(rssFeedLink.equals("https://www.maalaimalar.com/SectionRSS/news/sports")){category="விளையாட்டு";}
                    responseBody = getBody(url);
                    imageURL = getMalaimalarImgURL(responseBody);

                    desc = description.replaceAll("'", "");
                    media_type="Text/HTML";
                    format="";

                }
                else if(channel.contains("RSS Sourceurasu | Trending Tamil News Channel"))
                {
                    System.out.println("inside of malaimurasu");
                    if( rssFeedLink.equals("https://www.malaimurasu.com/rss/category/tamilnadu")){category="தமிழகம்";}
                    if(rssFeedLink.equals("https://www.malaimurasu.com/rss/category/india")){category="இந்தியா";}
                    if(rssFeedLink.equals("https://www.malaimurasu.com/rss/category/worldnews")){category="உலகம்";}


                    imageURL = getMalaimurasuImgURL(responseBody);

                    desc = description.replaceAll("'", "");
                    media_type="Text/HTML";
                    format="";

                }
                else if(channel.contains("Dinamani"))
                {
                    System.out.println("inside of dinamani");
                    if( rssFeedLink.equals("https://www.dinamani.com/%E0%AE%87%E0%AE%A8%E0%AF%8D%E0%AE%A4%E0%AE%BF%E0%AE%AF%E0%AE%BE/rssfeed/?id=337&getXmlFeed=true")){category="இந்தியா";}
                    if(rssFeedLink.equals("https://www.dinamani.com/%E0%AE%9A%E0%AE%BF%E0%AE%A9%E0%AE%BF%E0%AE%AE%E0%AE%BE/%E0%AE%9A%E0%AF%86%E0%AE%AF%E0%AF%8D%E0%AE%A4%E0%AE%BF%E0%AE%95%E0%AE%B3%E0%AF%8D/rssfeed/?id=485&getXmlFeed=true")){category="சினிமா";}
                    if(rssFeedLink.equals("https://www.dinamani.com/%E0%AE%B5%E0%AE%BF%E0%AE%B3%E0%AF%88%E0%AE%AF%E0%AE%BE%E0%AE%9F%E0%AF%8D%E0%AE%9F%E0%AF%81/%E0%AE%9A%E0%AF%86%E0%AE%AF%E0%AF%8D%E0%AE%A4%E0%AE%BF%E0%AE%95%E0%AE%B3%E0%AF%8D/rssfeed/?id=480&getXmlFeed=true")){category="விளையாட்டு";}


                    imageURL = getDinamaniImgURL(responseBody);

                    desc = description.replaceAll("'", "");
                    media_type="Text/HTML";
                    format="";

                }
                else if(channel.contains("தேசிய செய்திகள்")||channel.contains("மாவட்ட செய்திகள்")||channel.contains("தமிழ் சினிமா")||channel.contains("International"))
                {
                    System.out.println("vikatan news---->");
                    if(url.contains("vikatan.com")) {
                       if(rssFeedLink.equals("https://www.vikatan.com/api/v1/collections/india-news.rss")){category="இந்தியா";};
                        if(rssFeedLink.equals("https://www.vikatan.com/api/v1/collections/tamilnadu-news.rss")){category="தமிழகம்";};
                        if(rssFeedLink.equals("https://www.vikatan.com/api/v1/collections/international.rss")){category="உலகம்";};
                        if(rssFeedLink.equals("https://www.vikatan.com/api/v1/collections/kollywood-entertainment.rss")){category="சினிமா";};
                        title=getVikatanTitle(responseBody);
                        desc = getDescriptionForVikatan(responseBody);
                        imageURL = getVikatanImgUrl(responseBody);
                        media_type = "Text/HTML";
                        format = "";
                    }
                    System.out.println("title--->"+title);
                }
                else if(channel.contains("Indian Express")||channel.contains("இந்தியா")||channel.contains("தமிழ்நாடு")||channel.contains("பொழுதுபோக்கு"))
                {
                    System.out.println("inside of indian express");

                    if(url.contains("tamil.indianexpress.com")) {
                        if( rssFeedLink.equals("https://tamil.indianexpress.com/feed/")){category="பொது செய்திகள்";}
                        if(rssFeedLink.equals("https://tamil.indianexpress.com/tamilnadu/feed/")){category="தமிழகம்";}
                        if(rssFeedLink.equals("https://tamil.indianexpress.com/entertainment/feed/")){category="சினிமா";}
                        if(rssFeedLink.equals("https://tamil.indianexpress.com/india/feed/")){category="இந்தியா";}

                        imageURL = getIndianExpressImgURL(responseBody);

                        desc = description.replaceAll("'", "");
                        media_type = "Text/HTML";
                        format = "";

                    }

                }

                    else if(channel.contains("Sri Lanka Tamil New")||channel.contains("IBC Tamil"))
                 {
                    System.out.println("Sri Lanka Tamil New");
                    if(rssFeedLink.equals("https://ibctamil.com/rss")){category="பொது செய்திகள்";}
                    if(rssFeedLink.equals("https://ibctamil.com/rss/world")){category="உலகம்";}

                   imageURL = getIBCImgURL(responseBody);

                    desc = description.replaceAll("'", "");
                    desc=desc.replaceAll("\\<.*?>", "");
                    String regexp = "\\{/?iframe.*?\\}";
                    desc=desc.replaceAll(regexp, "");
                    desc=desc.replaceAll("<iframe","");
                    desc=desc.replaceAll("</iframe..","");
                    media_type="Text/HTML";
                    format="";

                }

                /*else if(channel.contains("News18 हिंदी")) {
                    System.out.println("News18 हिंदी");
                    imageURL = getImgUrl(responseBody);
                    desc = description.replaceAll("'", "");
                    media_type = "Text/HTML";
                    format = "";

                }*/
                else if (channel.contains("தி இந்து") || (channel.contains("தி இந்து - தமிழகம் ")||(channel.contains("இந்து தமிழ் திசை")))) {

                    /*String dateString = pubDate;
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                Date date = dateFormat.parse(dateString );
                long unixTime = (long) date.getTime()/1000;
                System.out.println("timestamp---> the hindu "+unixTime );
                    ads_timestamp=unixTime;*/
                    System.out.println("inside of tamil hindu");
                    if(url.contains("hindutamil")) {
                        if (rssFeedLink.equals("https://api.hindutamil.in/rss/tamilnadu")) {
                            category = "தமிழகம்";
                        }
                        if (rssFeedLink.equals("https://api.hindutamil.in/rss/india")) {
                            category = "இந்தியா";
                        }

                        if (rssFeedLink.equals("https://api.hindutamil.in/rss/sports")) {
                            category = "விளையாட்டு";
                        }
                        if (rssFeedLink.equals("https://api.hindutamil.in/rss/cinema")) {
                            category = "சினிமா";
                        }


                        imageURL = getTamilHinduImgURL(responseBody);
                        desc = description.replaceAll("'", "");
                        media_type = "Text/HTML";
                        format = "";
                    }

                }
                else if (channel.contains("Tamil Hindu"))
                {
                    /*String dateString = pubDate;
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                    Date date = dateFormat.parse(dateString );
                    long unixTime = (long) date.getTime()/1000;
                    ads_timestamp=unixTime;*/

                    //System.out.println("timestamp---> the hindu "+unixTime );

                    if(rssFeedLink.equals("https://api.hindutamil.in/rss/tamilnadu")){category="தமிழகம்";}
                    if(rssFeedLink.equals("https://api.hindutamil.in/rss/india")){category="இந்தியா";}

                    if(rssFeedLink.equals("https://api.hindutamil.in/rss/sports")){category="விளையாட்டு";}
                    if(rssFeedLink.equals("https://api.hindutamil.in/rss/cinema")){category="சினிமா";}

                    imageURL = getTamilHinduImgURL(responseBody);

                    desc = description.replaceAll("'", "");
                    media_type="Text/HTML";
                    format="";

                }
                else if(channel.contains("வீடியோ"))
                {

                    desc = description.replaceAll("'", "");
                    Document doc = Jsoup.parse(responseBody);
                    Element iframe = doc.select("iframe").first();
                    String iframeSrc = iframe.attr("src");
                    String pattern = "(?<=youtu.be/|watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";
                    Pattern compiledPattern = Pattern.compile(pattern);
                    Matcher matcher = compiledPattern.matcher(iframeSrc);


                    if (matcher.find()) {
                        System.out.println(matcher.group());
                        imageURL = matcher.group();
                    }
                    media_type="video";
                    format="youtube";
                    System.out.println("youtube original src"+iframeSrc );

                }
                else if(channel.contains("Samayam-Video"))
                {
                    desc = description.replaceAll("'", "");
                    Document doc = Jsoup.parse(responseBody);
                    Element iframe = doc.select("iframe").first();
                    String iframeSrc = iframe.attr("src");

                    media_type="video";
                    format="youtube";

                }

                else if (channel.contains("Tamil Samayam")||channel.contains("Samayam Tamil")) {
                     System.out.println("date--->"+pubDate);

                    /*String dateString = pubDate;
                    if(!pubDate.equals("") && !pubDate.contains("GMT")) {
                        Date date = null;
                        DateFormat dateFormat = new SimpleDateFormat("EEE dd MMM yyyy, HH:mm:ss");
                        dateFormat.setLenient(false);

                        date = dateFormat.parse(dateString);

                        long unixTime = (long) date.getTime() / 1000;
                        ads_timestamp=unixTime;
                        System.out.println("timestamp---> the samayam---> " + unixTime);
                    }
                    else
                    {
                        if(!pubDate.equals("") && pubDate.contains("GMT")) {


                            Date date = null;
                                DateFormat dateFormat1 = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss ");
                                dateFormat1.setLenient(false);

                                date = dateFormat1.parse(dateString.replace("GMT",""));
                                long unixTime = (long) date.getTime() / 1000;
                            ads_timestamp=unixTime;
                            System.out.println("timestamp---> the samayam---> " + unixTime);
                        }
                    }*/

                    if(rssFeedLink.equals("https://tamil.samayam.com/rssfeedsdefault.cms")){category="தமிழகம்";}

                    if(rssFeedLink.equals("https://tamil.samayam.com/sports/rssfeedsection/47766652.cms")){category="விளையாட்டு";}

                    imageURL=getSamayamUrl(responseBody);
                    desc = description.replaceAll("'", "");
                    media_type="Text/HTML";
                    format="";

                }
                else if(channel.contains("செய்திகள்"))
                {
                    imageURL= getWebduniaImageURL(responseBody);
                    desc = description.replaceAll("'", "");
                    media_type="Text/HTML";
                    format="";
                }
                else if(channel.contains("FilmiBeat"))
                {
                    /*System.out.println("film beat-->"+pubDate);
                    Date date = null;
                    DateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss");
                    dateFormat.setLenient(false);

                    date = dateFormat.parse(pubDate);

                    long unixTime = (long) date.getTime() / 1000;
                    System.out.println("uix timestao--> filmbeat"+unixTime);
                    ads_timestamp=unixTime;
                    */
                    System.out.println("inside of filmibeat");
                    imageURL=getFilmiBeatImgUrl(responseBody);
                    desc = description.replaceAll("'", "");
                    media_type="Text/HTML";
                    format="";
                    if(rssFeedLink.equals("https://tamil.filmibeat.com/rss/filmibeat-tamil-news-fb.xml")){category="சினிமா";}


                }

                else if(channel.contains("News18 हिंदी")||channel.contains("अन्य खेल")) {
                    System.out.println("News18 हिंदी");
                    imageURL = getNews18ImgUrl(responseBody);
                    desc = description.replaceAll("'", "");
                    media_type = "Text/HTML";
                    format = "";
                    if(rssFeedLink.equals("https://hindi.news18.com/rss/khabar/lifestyle/health.xml")){category="सामान्य समाचार";}
                    if(rssFeedLink.equals("https://hindi.news18.com/rss/khabar/ajab-gajab/ajab-gajab.xml")){category="सामान्य समाचार";}
                    if(rssFeedLink.equals("https://hindi.news18.com/rss/khabar/sports/others.xml")){category="सामान्य समाचार";}
                }
                else if(channel.contains("Dabang Dunia")) {
                    System.out.println("Inside of Dabang Dunia");
                    imageURL = getDabangDuniaImgUrl(responseBody);
                    desc = description.replaceAll("'", "");
                    Document document = Jsoup.parse(desc);
                    document.select("img").remove();
                    desc = document.text();
                    System.out.println("description here "+desc);
                    media_type = "Text/HTML";
                    format = "";
                    if(rssFeedLink.equals("https://www.dabangdunia.co/feed.aspx?cat_id=2")){category="इंडिया";}
                    if(rssFeedLink.equals("https://www.dabangdunia.co/feed.aspx?cat_id=4")){category="दुनिया";}
                    if(rssFeedLink.equals("https://www.dabangdunia.co/feed.aspx?cat_id=75")){category="मनोरंजन";}
                    if(rssFeedLink.equals("https://www.dabangdunia.co/feed.aspx?cat_id=76")){category="मनोरंजन";}
                }
                else if(channel.contains("Prabhasakshi")) {
                    System.out.println("Inside of Prabhaskshi");
                    imageURL = getPrabhasakshiImgUrl(responseBody);
                    desc = description.replaceAll("'", "");
                    Document document = Jsoup.parse(desc);
                    document.select("img").remove();
                    document.select("div").remove();
                    document.select("p").remove();
                    desc = document.text();
                    System.out.println("description here "+desc);
                    media_type = "Text/HTML";
                    format = "";
                    if(rssFeedLink.equals("https://www.prabhasakshi.com/rss/national")){category="इंडिया";}
                    if(rssFeedLink.equals("https://www.prabhasakshi.com/rss/international")){category="दुनिया";}
                    if(rssFeedLink.equals("https://www.prabhasakshi.com/rss/sport")){category="खेल";}
                    if(rssFeedLink.equals("https://www.prabhasakshi.com/rss/entertainment")){category="मनोरंजन";}
                }
                else if(channel.contains("Khaskhabar")) {
                    System.out.println("Inside of Khaskhabar");
                    imageURL = getKhasKabarImgUrl(responseBody);
                    desc = description.replaceAll("'", "");
                    media_type = "Text/HTML";
                    format = "";
                }
                else if(channel.contains("Nai Dunia Hindi News")) {
                    System.out.println("Inside of Nai Dunia");
                    imageURL = getNaiDuniaImgUrl(responseBody);
                    desc = description.replaceAll("'", "");
                    media_type = "Text/HTML";
                    format = "";
                    if(rssFeedLink.equals("http://rss.jagran.com/naidunia/entertainment.xml")){category="मनोरंजन";}
                    if(rssFeedLink.equals("http://rss.jagran.com/naidunia/topnews.xml")){category="सामान्य समाचार";}
                    if(rssFeedLink.equals("http://rss.jagran.com/naidunia/national.xml")){category="इंडिया";}
                }
                else if(channel.contains("NDTVKhabar.com")) {
                    System.out.println("Inside of ND Tv");
                    imageURL = getNdtvImgUrl(responseBody);
                    desc = description.replaceAll("'", "");
                    media_type = "Text/HTML";
                    format = "";
                }
                else if(channel.contains("BBC News हिंदी")) {
                    System.out.println("BBC हिंदी");
                    imageURL = getBBCHindiImgUrl(responseBody);
                    desc = description.replaceAll("'", "");
                    media_type = "Text/HTML";
                    format = "";
                    if(rssFeedLink.equals("https://feeds.bbci.co.uk/hindi/rss.xml")){category="दुनिया";}
                }

                else if(channel.contains("Zee News Hindi")) {
                    System.out.println("Zee News Hindi");
                    imageURL = getZeeNewsHindiImgUrl(responseBody);
                    desc = description.replaceAll("'", "");
                    media_type = "Text/HTML";
                    format = "";
                    if(rssFeedLink.equals("https://zeenews.india.com/hindi/india.xml")){category="इंडिया";}
                    if(rssFeedLink.equals("https://zeenews.india.com/hindi/world.xml")){category="दुनिया";}
                    if(rssFeedLink.equals("https://zeenews.india.com/hindi/sports.xml")){category="खेल";}
                    if(rssFeedLink.equals("https://zeenews.india.com/hindi/entertainment.xml")){category="मनोरंजन";}
                }
                else if(channel.contains("Movie News - Bollywood (Hindi)")){
                    System.out.println("Inside of Filmibeat");
                    imageURL = getFilmiBeatHindiImgUrl(responseBody);
                    desc = description.replaceAll("'", "");
                    media_type = "Text/HTML";
                    format = "";
                    if(rssFeedLink.equals("https://hindi.filmibeat.com/rss/filmibeat-hindi-fb.xml")){category="मनोरंजन";}
                }
                else if(channel.contains("Hindustan Rss feed")) {
                    System.out.println("Hindustan Rss feed");
                    imageURL = getHindustanImgUrl(responseBody);
                    desc = description.replaceAll("'", "");
                    Document document = Jsoup.parse(desc);
                    document.select("img").remove();
                    desc = document.text();
                    System.out.println("description here "+desc);
                    media_type = "Text/HTML";
                    format = "";
                    if(rssFeedLink.equals("https://feed.livehindustan.com/rss/3116")){category="सामान्य समाचार";}
                    if(rssFeedLink.equals("https://feed.livehindustan.com/rss/3116")){category="सामान्य समाचार";}

                }
                else if(channel.contains("आज के समाचार")||channel.contains("thatsHindi")) {
                    System.out.println("thatsHindi ----->");
                    imageURL = getoneIndiaHindiImgUrl(responseBody);
                    desc = description.replaceAll("'", "");
                    media_type = "Text/HTML";
                    format = "";
                    if(rssFeedLink.equals("https://hindi.oneindia.com/rss/hindi-news-fb.xml")){category="सामान्य समाचार";}
                    if(rssFeedLink.equals("https://hindi.oneindia.com/rss/hindi-city-fb.xml")){category="सामान्य समाचार";}
                    if(rssFeedLink.equals("https://hindi.oneindia.com/rss/hindi-fb.xml")){category="सामान्य समाचार";}
                }
                else if(channel.contains("Amar Ujala")) {
                    System.out.println("Amar Ujala -->");
                    imageURL = getAmarUjalaImgUrl(responseBody);
                    desc = getDescriptionForDinamalar(responseBody);
                    media_type = "Text/HTML";
                    format = "";
                    if(rssFeedLink.equals("https://www.amarujala.com/rss/breaking-news.xml")){category="सामान्य समाचार";}
                }


                else if(channel.contains("dinamalar.com")||channel.contains("Dinamalar.com"))
                {

                    /*String dateString = pubDate;
                    DateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss");
                    Date date = dateFormat.parse(dateString );
                    long unixTime = (long) date.getTime()/1000;
                    System.out.println("timestamp --- news >"+unixTime );
                    ads_timestamp=unixTime;*/

                    // System.out.println("response body"+responseBody);
                    desc=getDescriptionForDinamalar(responseBody);
                    byte[] bytes = desc.getBytes(StandardCharsets.ISO_8859_1);
                    desc = new String(bytes, StandardCharsets.UTF_8);
                    System.out.println("utf8 description is-->"+desc);
                    imageURL=getDinamalarImgURL(responseBody);
                    media_type="Text/HTML";
                    format="";
                    if(rssFeedLink.equals("http://rss.dinamalar.com/?cat=ara1")){category="அரசியல்";}
                    if(rssFeedLink.equals("http://rss.dinamalar.com/?cat=pot1")){category="பொது செய்திகள்";}
                    if(rssFeedLink.equals("http://rss.dinamalar.com/?cat=INL1")){category="உலகம்";}



                }

                else if(channel.contains("BBC News தமிழ் - முகப்பு"))
                {

                    /*String dateString = pubDate;
                    DateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss");
                    Date date = dateFormat.parse(dateString );
                    long unixTime = (long) date.getTime()/1000;
                    System.out.println("timestamp---> bbc--->"+unixTime );
                    ads_timestamp=unixTime;*/


                    if(rssFeedLink.equals("http://feeds.bbci.co.uk/tamil/rss.xml#sa-link_location=story-body&intlink_from_url=https%3A%2F%2Fwww.bbc.com%2Ftamil%2Finstitutional%2F2011%2F08%2F000000_rss&intlink_ts=1551514105358-sa")){category="உலகம்";}


                    desc = description.replaceAll("'", "");
                    imageURL=getBBCTamilImgUrl(responseBody);
                    media_type="Text/HTML";
                    format="";
                }
                else if(channel.contains("Sports News in Tamil"))
                {
                    imageURL=getSamayamUrl(responseBody);
                    desc = description.replaceAll("'", "");
                    media_type="Text/HTML";
                    format="";
                }
                else if(channel.contains("Nadu News")||channel.contains("Movies News")||channel.contains("")||channel.contains("India News")||channel.contains("World News"))
                {
                    System.out.println("inside of Zee News");

                    if(url.contains("zeenews.india.com")) {
                        if(rssFeedLink.equals("https://zeenews.india.com/tamil/india.xml")){category="இந்தியா";}
                        if(rssFeedLink.equals("https://zeenews.india.com/tamil/tamil-nadu.xml")){category="தமிழகம்";}
                        if(rssFeedLink.equals("https://zeenews.india.com/tamil/movies.xml")){category="சினிமா";}
                        if(rssFeedLink.equals("https://zeenews.india.com/tamil/world.xml")){category="உலகம்";}
                        if(rssFeedLink.equals("https://zeenews.india.com/tamil/sports.xml")){category="விளையாட்டு";}

                        imageURL = getZeeNewsImgUrl(responseBody);

                        desc = description.replaceAll("'", "");
                        media_type = "Text/HTML";
                        format = "";

                    }

                }

            }


            String tags[] = {"News"};

           if (!linkDAO.isLinkPresent(url)) {


               AddLink link=new AddLink();
                    link.setLinkId(0);
                    link.setUserId(999);
                    link.setTitle(title);
                    link.setImageUrl(imageURL);
                    link.setCategory(category);
                    link.setVotes(0);
                    link.setNoOfComments(0);
                    link.setSpam(0);
                    link.setUrl(url);
                    link.setLanguage(lang);
                    link.setDomain(domain);
                    link.setCountry(country);
                    link.setDesc(desc);
                    link.setMedia_type(media_type);
                    link.setVideo_format(format);
                    link.setAds_published_date(pubDate);

                    System.out.println("title"+title);
                    System.out.println("url"+url);
                    System.out.println("image url"+imageURL);
                    System.out.println("description"+desc);
                    System.out.println("pub Date"+pubDate);
                    System.out.println("category"+category);
                    System.out.println("domain"+domain);


                   linkDAO.addNewLink(link);

               }


        }

    }

    public void clearCache()
    {



        InputStream input = null;
        Properties prop = new Properties();


        try {

            input = new FileInputStream("/var/lib/tomcat7/webapps/ROOT/WEB-INF/db.properties");

            // input = new FileInputStream("/Users/Sathya/Documents/_PROJCTS/SUDASUDA/sudasudascrapper/src/main/webapp/WEB-INF/db.properties" );

            prop.load(input);
            System.out.println("servername"+prop.getProperty("server"));


            String listOfServer=prop.getProperty("server");
            String[] server=listOfServer.split(",");//Separating the word using delimiter Comma and stored in an array
            for(int k=0;k<server.length;k++)
            {

                System.out.println(server[k]);

                String inputs=server[k];

                CloseableHttpClient client = HttpClients.createDefault();
                URIBuilder builder = new URIBuilder(inputs);

                HttpGet get = new HttpGet(builder.build());

                CloseableHttpResponse response = client.execute(get);

                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        response.getEntity().getContent()));

                String inputLine;
                StringBuffer HttpResponse = new StringBuffer();


                System.out.println("Clearing Cache.........");
                reader.close();
            }

        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
        catch (URISyntaxException ex)
        {
            ex.printStackTrace();
        }

    }




    public String getResponseBody(String url) throws IOException {
        String responseBody = null;
        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {

            HttpGet httpget = new HttpGet(url);

            // Create a custom response handler
            ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

                // @Override
                public String handleResponse(
                        final HttpResponse response) throws ClientProtocolException, IOException {
                    int status = response.getStatusLine().getStatusCode();
                    if (status >= 200 && status < 300) {
                        HttpEntity entity = response.getEntity();
                        return entity != null ? EntityUtils.toString(entity) : null;
                    } else {
                        throw new ClientProtocolException("Unexpected response status: " + status);
                    }
                }

            };

            responseBody = httpclient.execute(httpget, responseHandler);
        } finally {
            httpclient.close();
        }
        //System.out.println(responseB)
        return responseBody;
    }


    public String getBody(String urls)
    {
        String responseBody=null;
        try
        {
            URL originalURL = new URL(urls);
            String host="";

            String domain = originalURL.getHost();

            System.out.println("Original Domain-->"+domain);
            if(domain.contains("www."))
            {
                host=domain;
            }
            else
            {
                host="www."+domain;
            }
//            System.out.println("HOST--->"+host);
            String newUrl = new URIBuilder(URI.create(urls)).setHost(host).build().toString();

  //          System.out.println("new url"+newUrl);
            CloseableHttpClient httpClient = HttpClients.createDefault();


            final HttpGet httpGet = new HttpGet(newUrl);
            CloseableHttpResponse response = httpClient.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            System.out.println(statusLine.getStatusCode() + " " + statusLine.getReasonPhrase());
            responseBody = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
            //  System.out.println("Response body: " + responseBody);

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (URISyntaxException e)
        {
            e.printStackTrace();
        }

        return responseBody;
    }

    /*public String getPuthiyathalaimuraiURL(String responseBody) {
        //System.out.println("dinakartan image url"+responseBody);


        Document document= Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage!=null) {


            if(metaOgImage.isEmpty())
            {

                imageURL="http://www.puthiyathalaimurai.com/assets/puthiyathalaimurai-logo.webp";
            }
            else {

                imageURL = metaOgImage.attr("content");
                System.out.println(imageURL);
            }
        }
        else
        {
            imageURL="http://www.puthiyathalaimurai.com/assets/puthiyathalaimurai-logo.webp";

        }
        return imageURL;
    }*/
    public String getPuthiyathalaimuraiImgURL(String responseBody) {
        //System.out.println("dinakartan image url"+responseBody);


        Document document= Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage!=null) {


            if(metaOgImage.isEmpty())
            {

                imageURL="http://www.puthiyathalaimurai.com/assets/puthiyathalaimurai-logo.webp";
            }
            else {

                imageURL = metaOgImage.attr("content");
                System.out.println(imageURL);
            }
        }
        else
        {
            imageURL="http://www.puthiyathalaimurai.com/assets/puthiyathalaimurai-logo.webp";

        }
        return imageURL;
    }

    public String getDinakaranImgURL(String responseBody) {
        //System.out.println("dinakartan image url"+responseBody);


        Document document = Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        //Elements timeDate = document.select("div[class=time]");
        //System.out.println("dinakaran time--->" + timeDate.text());
        //System.out.println("dinakaran time--->" + timeDate.text().replace('@', ' '));
        //String timestamp = timeDate.text().replace('@', ' ');
        //DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
//    Date date = formatter.parse(timestamp);
        //  Timestamp timeStampDate = new Timestamp(date.getTime());

        if (metaOgImage != null) {


            if (metaOgImage.isEmpty()) {

                imageURL = "http://www.dinakaran.com/images/site-use/logo-dinakaran-news-paper.png";
            } else {

                imageURL = metaOgImage.attr("content");
                System.out.println(imageURL);
            }

        } else {
            imageURL = "http://www.dinakaran.com/images/site-use/logo-dinakaran-news-paper.png";

        }

        return imageURL;
    }


    /* public String getTamilHinduImgURL(String responseBody) {

       //  System.out.println("tamil the hindu video"+responseBody);
         if (responseBody != null && responseBody.contains("hcenter")) {
             responseBody = responseBody.substring(responseBody.indexOf("hcenter"));
             responseBody = responseBody.substring(responseBody.indexOf("src=") + 5, responseBody.indexOf("class") - 2);
             imageURL = responseBody;
         } else if (responseBody != null && responseBody.contains("og:image")) {
             responseBody = responseBody.substring(responseBody.indexOf("og:image\""));
             responseBody = responseBody.substring(responseBody.indexOf("content=\"") + 9, responseBody.indexOf("/>") - 2);
             //  System.out.println("IMAGE URL OF HINDU ==========>" + responseBody);
             imageURL = responseBody;
         } else {

             imageURL = "http://i.imgur.com/jvGPMA2.png";
         }
         return imageURL;
     }*/
    public String getTamilHinduImgURL(String responseBody) {

        // System.out.println("tamil the hindu video"+responseBody);
        Document document= Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage!=null) {
            imageURL= metaOgImage.attr("content");
        }
        else
        {
            imageURL="http://i.imgur.com/jvGPMA2.png";
        }

        return imageURL;
    }
    public String getMalaimurasuImgURL(String responseBody) {

        // System.out.println("tamil the hindu video"+responseBody);
        Document document= Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage!=null) {
            imageURL= metaOgImage.attr("content");
        }
        else
        {
            imageURL="https://www.malaimurasu.com/uploads/logo/logo_60abc7a318fb8.jpeg";
        }

        return imageURL;
    }


    public String getDinamalarImgURL(String responseBody) {


        Document document= Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage!=null) {
            imageURL= metaOgImage.attr("content");
        }
        else
        {
            imageURL="http://www.creativemedia.co.in/images/2018/01/14/dinamalar-logo.jpg";
        }
        return imageURL;
    }

    public String getOneIndiaImgURL(String responseBody) {

        Document document= Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage!=null) {
            System.out.println("meta image---->"+metaOgImage);

            imageURL= metaOgImage.attr("content");
        }
        else
        {
            imageURL="https://images.oneindia.com/images/rss/oneindia-tamil-logo.png";
        }
        return imageURL;
    }

    public String getSamayamUrl(String responseBody) {

        Document document= Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage!=null) {
            imageURL= metaOgImage.attr("content");
        }
        else
        {
            imageURL="https://tamil.samayam.com/photo/48055721/logo.jpg";
        }



        return imageURL;
    }

    public String getFilmiBeatImgUrl(String responseBody) {

        Document document= Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage!=null) {
            imageURL= metaOgImage.attr("content");
        }
        else
        {
            imageURL="https://tamil.samayam.com/photo/48055721/logo.jpg";
        }



        return imageURL;
    }
    public String getFilmiBeatHindiImgUrl(String responseBody) {
        System.out.println("Inside of Filmibeat Hindi");
        Document document= Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage!=null) {
            System.out.println("Image URL---> for filmi"+imageURL);
            imageURL= metaOgImage.attr("content");
        }
        else
        {
            imageURL="https://www.filmibeat.com/images/fb-banner.png";
        }



        return imageURL;
    }

    public String getWebduniaImageURL(String responseBody) {

        Document document= Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage!=null) {
            imageURL= metaOgImage.attr("content");
        }
        else
        {
            imageURL="https://i.ytimg.com/vi/t-mNF53_a2I/0.jpg";
        }



        return imageURL;
    }
    public String getVikatanImgUrl(String responseBody) {



        Document document= Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage!=null) {
            imageURL= metaOgImage.attr("content");
        }
        else
        {
            imageURL="https://content3.jdmagicbox.com/comp/chennai/03/044pf003003/catalogue/vikatan-media-services-pvt-ltd-mount-road-chennai-magazines-3bdrggn.jpg";
        }



        return imageURL;
    }
    public String getVikatanTitle(String responseBody) {


        System.out.println("fetching title");
        Document document= Jsoup.parse(responseBody);
        Elements title =  document.select("meta[property=og:title]");
        if (title!=null) {
            vikatanTitle= title.attr("content");
            System.out.println("fetching title22"+title.attr("content"));
        }
        else
        {
            vikatanTitle="";

        }



        return vikatanTitle;
    }


    public String getBBCTamilImgUrl(String responseBody) {


        Document document= Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage!=null) {
            imageURL= metaOgImage.attr("content");
        }
        else
        {
            imageURL=" https://ichef.bbci.co.uk/news/ws/640/amz/worldservice/live/assets/images/2016/11/15/161115113204_bbc_tamil_logo_640x360_bbc_nocredit.jpg";
        }



        return imageURL;
    }
    public String getDescriptionForVikatan(String responseBody) {


        Document document= Jsoup.parse(responseBody);

        Elements shortDesc = document.select("meta[property=og:description]");
        Elements shortDesc2 = document.select("meta[name=Description]");
        if(shortDesc2!=null)

        {
            desc=shortDesc2.attr("content");

        }


        return desc;
    }
    public String getDescriptionForDinamalar(String responseBody) {


        Document document= Jsoup.parse(responseBody);

        Elements shortDesc = document.select("meta[property=og:description]");
        if(shortDesc!=null)
        {
            desc=shortDesc.attr("content");
        }


        return desc;
    }
    public String getMalaimalarImgURL(String responseBody) {

        Document document= Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage!=null) {
            imageURL= metaOgImage.attr("content");
        }
        else
        {
            imageURL="https://stat.maalaimalar.com/MMNews/Content/images/MM-NewsLogo.png";
        }



        return imageURL;
    }
    public String getZeeNewsImgUrl(String responseBody) {


        Document document= Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage!=null) {
            imageURL= metaOgImage.attr("content");
        }
        else
        {
            imageURL="https://pbs.twimg.com/profile_images/944366370782711809/wQtAVakc_400x400.jpg";
        }



        return imageURL;
    }

    public String getDinamaniImgURL(String responseBody) {


        Document document= Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage!=null) {
            imageURL= metaOgImage.attr("content");
        }
        else
        {
            imageURL="https://images.dinamani.com/uploads/user/imagelibrary/logo/dinamani_logo_600X300.jpg?w=400&dpr=2.6";
        }



        return imageURL;
    }
    public String getIndianExpressImgURL(String responseBody) {


        Document document= Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        System.out.println("indian express image url"+metaOgImage);
        if (metaOgImage!=null) {
            imageURL= metaOgImage.attr("content");
        }
        else
        {
            imageURL="https://play-lh.googleusercontent.com/-el8TtdDV9Hj0lPXQ4trWvtapqcvA11sNNTXtcZDJiXY_anPEuorjkyE9RMK8Z_knXI";
        }



        return imageURL;
    }
    public String getIBCImgURL(String responseBody) {


        Document document= Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage!=null) {
            imageURL= metaOgImage.attr("content");
        }
        else
        {
            imageURL="https://play-lh.googleusercontent.com/-el8TtdDV9Hj0lPXQ4trWvtapqcvA11sNNTXtcZDJiXY_anPEuorjkyE9RMK8Z_knXI";
        }



        return imageURL;
    }
    public String  getAmarUjalaImgUrl(String responseBody) {
        System.out.println("getAmarUjala Image URL");

        Document document= Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage!=null) {
            imageURL= metaOgImage.attr("content");
        }
        else
        {
            imageURL="https://yt3.ggpht.com/ytc/AKedOLR_lL1g2F3NCWgI97H3fYvAv6nFsJGfTf_KVJ7FXw=s900-c-k-c0x00ffffff-no-rj";
        }
        System.out.println("image url amar ujala"+imageURL);
        return imageURL;

    }
    public String getoneIndiaHindiImgUrl(String responseBody) {
        System.out.println("inside of oneindia hindi image url-->");
        Document document= Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage!=null) {
            System.out.println("meta image---->"+metaOgImage);

            imageURL= metaOgImage.attr("content");
        }
        else
        {
            imageURL="https://hindi.oneindia.com/images/oneindia-hindi-og-image.jpg";
        }
        return imageURL;
    }
    public String getHindustanImgUrl(String responseBody) {
        System.out.println("inside of get Hindustan Image URL--->");
        Document document= Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage!=null) {
            imageURL= metaOgImage.attr("content");
        }
        else
        {
            imageURL="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRaVcmAe7Gfwg8jPpXApkA8AGRNRSiA-858YQPPpOlEdlbe_rLmb5XYQEaxpzwVESLLVSg&usqp=CAU";
        }


        System.out.println("Image url for hindustan-->"+imageURL);
        return imageURL;
    }
    public String getNews18ImgUrl(String responseBody) {
        System.out.println("inside of get News 18 Image URL--->");
        Document document = Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage != null) {
            imageURL = metaOgImage.attr("content");
        } else {
            imageURL = "https://static.wikia.nocookie.net/logopedia/images/9/95/News18_Bharat.png/revision/latest?cb=20200105092023";
        }
        System.out.println("image url-->"+imageURL);

        return imageURL;
    }
    public String getDabangDuniaImgUrl(String responseBody) {
        System.out.println("inside of get News 18 Image URL--->");
        Document document = Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage != null) {
            imageURL = metaOgImage.attr("content");
        } else {
            imageURL = "https://static.wikia.nocookie.net/logopedia/images/9/95/News18_Bharat.png/revision/latest?cb=20200105092023";
        }
        System.out.println("image url-->"+imageURL);

        return imageURL;
    }
    public String getPrabhasakshiImgUrl (String responseBody) {
        System.out.println("inside of get News 18 Image URL--->");
        Document document = Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage != null) {
            imageURL = metaOgImage.attr("content");
        } else {
            imageURL = "https://static.wikia.nocookie.net/logopedia/images/9/95/News18_Bharat.png/revision/latest?cb=20200105092023";
        }
        System.out.println("image url-->"+imageURL);

        return imageURL;
    }

    public String getKhasKabarImgUrl (String responseBody) {
        System.out.println("inside of get News 18 Image URL--->");
        Document document = Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage != null) {
            imageURL = metaOgImage.attr("content");
        } else {
            imageURL = "https://static.wikia.nocookie.net/logopedia/images/9/95/News18_Bharat.png/revision/latest?cb=20200105092023";
        }
        System.out.println("image url-->"+imageURL);

        return imageURL;
    }

    public String getNaiDuniaImgUrl(String responseBody) {
        System.out.println("inside of get Nai Dunia URL--->");
        Document document = Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage != null) {
            imageURL = metaOgImage.attr("content");
        } else {
            imageURL = "https://static.wikia.nocookie.net/logopedia/images/9/95/News18_Bharat.png/revision/latest?cb=20200105092023";
        }
        System.out.println("image url-->"+imageURL);

        return imageURL;
    }

    public String getNdtvImgUrl(String responseBody) {
        System.out.println("inside of get Nd TV Image URL--->");
        Document document = Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage != null) {
            imageURL = metaOgImage.attr("content");
        } else {
            imageURL = "https://static.wikia.nocookie.net/logopedia/images/9/95/News18_Bharat.png/revision/latest?cb=20200105092023";
        }
        System.out.println("image url-->"+imageURL);

        return imageURL;
    }

    public String getBBCHindiImgUrl(String responseBody) {
        System.out.println("inside of get BBC Hindi Image URL--->");
        Document document = Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage != null) {
            imageURL = metaOgImage.attr("content");
        } else {
            imageURL = "https://static.wikia.nocookie.net/logopedia/images/9/95/News18_Bharat.png/revision/latest?cb=20200105092023";
        }
        System.out.println("image url-->"+imageURL);

        return imageURL;
    }
    public String getZeeNewsHindiImgUrl(String responseBody) {
        System.out.println("inside of get Zee News Hindi--->");
        Document document = Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage != null) {
            imageURL = metaOgImage.attr("content");
        } else {
            imageURL = "https://static.wikia.nocookie.net/logopedia/images/9/95/News18_Bharat.png/revision/latest?cb=20200105092023";
        }
        System.out.println("image url-->"+imageURL);

        return imageURL;
    }
    public String getImgUrl(String responseBody) {


        Document document= Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage!=null) {
            imageURL= metaOgImage.attr("content");
        }
        else
        {
            imageURL="https://play-lh.googleusercontent.com/-el8TtdDV9Hj0lPXQ4trWvtapqcvA11sNNTXtcZDJiXY_anPEuorjkyE9RMK8Z_knXI";
        }



        return imageURL;
    }
}
