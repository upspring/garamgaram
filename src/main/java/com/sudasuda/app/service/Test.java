package com.sudasuda.app.service;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Sathya on 31/10/18.
 */
public class Test
{

    public static void main(String args[])
    {


        PreparedStatement pstmt = null;
        Connection connection = null;
        String[] content = {"a1", "a2", "a3", "a4"};
        try {
            Class.forName("com.mysql.jdbc.Driver");
             connection= DriverManager.getConnection(
                    "jdbc:mysql://3.92.5.119:3306/chudachuda","root","venkk23");

            String query = "INSERT INTO array (id, array_value) VALUES (?, ARRAY['aa','bb','cc','dd'])";

            pstmt = connection.prepareStatement(query);

            pstmt.setInt(1,-1);

            for (int i = 0; i < content.length; i++) {
                pstmt.setString(i + 4, content[i]);
            }

            int count = pstmt.executeUpdate();
            System.out.println(count + " inserted");
        }
        catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (connection != null)
                    connection.close();
                if (pstmt != null)
                    pstmt.close();

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }
}
/*
package com.chudachuda.service;

import com.chudachuda.dao.LinkDAO;
import com.chudachuda.domain.Link;
import org.apache.commons.feedparser.*;
import org.apache.commons.feedparser.network.ResourceRequest;
import org.apache.commons.feedparser.network.ResourceRequestFactory;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.net.ssl.SSLSocketFactory;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;




@Component("RssFeedService")
@Service
public class RssFeedServiceImpl implements RssFeedService {

    private static final Logger logger = LoggerFactory
            .getLogger(RssFeedService.class);


    @Autowired(required = true)
    private LinkDAO linkDAO;
    String imageURL = null;
    String media_type="";
    String format="";
    String desc=null;
    String domain="";
    HttpServletRequest request;

    public synchronized void addRssFeed() throws Exception {

        logger.info("Rss Feeds Starts");


        List<String> rssFeedList = new ArrayList<String>();



        rssFeedList.add("http://cinema.dinakaran.com/Rss/RssKollywood.aspx");
        rssFeedList.add("http://cinema.dinakaran.com/Rss/RssBollywood.aspx");

        rssFeedList.add("https://tamil.oneindia.com/rss/tamil-news-fb.xml");
        rssFeedList.add("https://tamil.oneindia.com/rss/tamil-fb.xml");
        rssFeedList.add("https://tamil.oneindia.com/rss/tamil-motivational-stories-fb.xml");


        rssFeedList.add("https://api.hindutamil.in/rss/tamilnadu");
        rssFeedList.add("https://api.hindutamil.in/rss/india");
        rssFeedList.add("https://tamil.samayam.com/rssfeedsdefault.cms");
        rssFeedList.add("https://tamil.samayam.com/sports/rssfeedsection/47766652.cms");
        rssFeedList.add("http://www.dinakaran.com/rss_Latest.asp");
        //rssFeedList.add("http://tamil.webdunia.com/rss/tamil-news-updates-latest-news-101.rss");
        rssFeedList.add("http://feeds.bbci.co.uk/tamil/rss.xml#sa-link_location=story-body&intlink_from_url=https%3A%2F%2Fwww.bbc.com%2Ftamil%2Finstitutional%2F2011%2F08%2F000000_rss&intlink_ts=1551514105358-sa");

        rssFeedList.add("http://rss.dinamalar.com/?cat=pot1");

        rssFeedList.add("https://cinema.dinamalar.com/rss.php");
        rssFeedList.add("http://rss.dinamalar.com/?cat=INL1");
        rssFeedList.add("https://tamil.filmibeat.com/rss/filmibeat-tamil-news-fb.xml");

        rssFeedList.add("https://api.hindutamil.in/rss/cinema");
        rssFeedList.add("https://api.hindutamil.in/rss/sports");

        //rssFeedList.add("http://rss.vikatan.com/feeds/depth_news.rss");
        //rssFeedList.add("http://rss.vikatan.com/feeds/entertainment_article_content.rss");*/

    /*    rssFeedList.add("https://feeds.feedburner.com/Puthiyathalaimurai_World_News?fmt=xml");
        rssFeedList.add("https://feeds.feedburner.com/Puthiyathalaimurai_topnews?fmt=xml");

        rssFeedList.add("https://feeds.feedburner.com/Puthiyathalaimurai_India_news?fmt=xml");
        rssFeedList.add("https://feeds.feedburner.com/Puthiyathalaimurai_Tamilnadu_News?fmt=xml");

        // rssFeedList.add("https://api.hindutamil.in/rss/cinema");



        for (String feedLink : rssFeedList) {
            System.out.println("RSS Source:" + feedLink);
            try {
                //main();
                getArticleDetails(feedLink);



            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        clearCache();
        logger.info("Rss Feeds End");

    }


    public void getArticleDetails(String rssFeedLink) throws Exception {

        System.out.println("rssFeedLink---->"+rssFeedLink);
        //create a new FeedParser...
        FeedParser parser = FeedParserFactory.newFeedParser();

        //create a listener for handling our callbacks
        FeedParserListener listener = new DefaultFeedParserListener() {

            String channel = null;

            public void onChannel(FeedParserState state,
                                  String title,
                                  String link,

                                  String description) throws FeedParserException {
                channel = title;
                if(channel.equals("Tamil Hindu - வீடியோ"))
                {
                    channel="வீடியோ";
                }
                if(channel.equals("திரை பாடல்கள் - வீடியோ - Tamil Samayam"))
                {
                    channel="Samayam-Video";
                }
                System.out.println("host title is"+channel);


            }

            public void onItem(FeedParserState state,
                               String title,
                               String url,
                               String description,
                               String pubDate
            ) throws FeedParserException {
                String imageURL = "";
                String desc = "";
                String lang = "Tamil";
                String country="India";
                // String domain="";
                System.out.println(5);
                System.out.println("url"+url);

                System.out.println("channel--->"+channel);



                if(channel.contains("Puthiyathalaimurai - Tamil News | Latest Tamil News | Tamil News Online | Tamilnadu News"))
                {


                    try{
                        System.out.println("inside of puthiya");
                        media_type="Text/HTML";
                        format="";
                        domain="puthiyathalaimurai.com";

                        System.out.println("descriptin****"+description);
                        desc = description.replaceAll("\n", "");
                        desc = (desc.length() > 400) ? desc.substring(0, 400).replaceAll("'", "") : desc.replaceAll("'", "");


                        String responseBody2 =null;
                        responseBody2= getBody(url);
                        System.out.println("------->"+responseBody2);
                        imageURL = getPuthiyathalaimuraiImgURL(responseBody2);

                        System.out.println("puthiya image"+imageURL);
                        //desc = getPuthiyathalaimuraiDesURL(responseBody);
                        //byte[] bytes = desc.getBytes(StandardCharsets.ISO_8859_1);
                        //desc = new String(bytes, StandardCharsets.UTF_8);

                        System.out.println(desc);



                    }
                    catch (Exception e)
                    {

                    }




                    //desc=description;
                    //System.out.println("dessss--->"+desc);
                }






                if (channel.contains("Dinakaran") || channel.contains("Tamil Samayam")||channel.contains("செய்திகள்")||channel.contains("FilmiBeat")||channel.contains("Vikatan.com")||channel.contains("City News")||channel.contains("World News")||channel.contains("Samayam-Video")||channel.contains("இந்து தமிழ் திசை")||

                        channel.contains("Oneindia")||channel.contains("Dinamalar.com")||channel.contains("Sports News in Tamil") ||channel.contains("வீடியோ") || channel.contains("Tamil Hindu")|| channel.contains("India News") ||channel.contains("BBC News தமிழ் - முகப்பு")|| channel.contains("Cinema.Dinakaran")) {
                    String responseBody = null;

                    try {
                        responseBody = getResponseBody(url);
                        System.out.println("channel--->"+channel);


                        URI uri = new URI(url);

                        String domainName = uri.getHost();
                        System.out.println("domain"+domainName);
                        if (domainName != null) {
                            String domainSite = domainName.startsWith("www.") ? domainName.substring(4) : domainName;
                            if (domainSite == null) {
                                domain = uri.getHost();
                                //System.out.println("domain222"+domain);

                            } else {
                                domain = domainSite;
                                System.out.println("domain3333"+domain);
                                if(domain.equals("hindutamil.in"))
                                {
                                    domain="tamil.thehindu.com";
                                }

                            }
                        }


                        // System.out.println("print the response body"+responseBody);
                        // boolean status=pingUrl();

                        //System.out.println("status------->"+responseBody);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    catch (URISyntaxException e)
                    {

                    }


                    if (channel.contains("Dinakaran")) {

                        imageURL = getDinakaranImgURL(responseBody);

                        desc = description.substring(description.indexOf("<p>") + 3, description.indexOf("</p>") - 3);
                        desc = (desc.length() > 400) ? desc.substring(0, 400).replaceAll("'", "") : desc.replaceAll("'", "");
                        media_type="Text/HTML";
                        format="";

                    }


                    else if(channel.contains("Cinema.Dinakaran"))
                    {


                        imageURL = getDinakaranImgURL(responseBody);

                        desc = description.substring(description.indexOf("<p>") + 3, description.indexOf("</p>") - 3);
                        desc = (desc.length() > 400) ? desc.substring(0, 400).replaceAll("'", "") : desc.replaceAll("'", "");
                        media_type="Text/HTML";
                        format="";
                    }
                    else if(channel.contains("மாலை மலர் | தலைப்புச்செய்திகள்"))
                    {


                        imageURL = getMalaimalarImgURL(responseBody);

                        desc = description.replaceAll("'", "");
                        media_type="Text/HTML";
                        format="";

                    }

                    else if (channel.contains("தி இந்து") || (channel.contains("தி இந்து - தமிழகம் ")||(channel.contains("இந்து தமிழ் திசை")))) {

                        imageURL = getTamilHinduImgURL(responseBody);
                        desc = description.replaceAll("'", "");
                        media_type="Text/HTML";
                        format="";

                    } else if (channel.contains("Tamil Hindu")) {
                        imageURL = getTamilHinduImgURL(responseBody);

                        desc = description.replaceAll("'", "");
                        media_type="Text/HTML";
                        format="";

                    }
                    else if(channel.contains("வீடியோ"))
                    {

                        desc = description.replaceAll("'", "");
                        Document doc = Jsoup.parse(responseBody);
                        Element iframe = doc.select("iframe").first();
                        String iframeSrc = iframe.attr("src");
                        String pattern = "(?<=youtu.be/|watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";
                        Pattern compiledPattern = Pattern.compile(pattern);
                        Matcher matcher = compiledPattern.matcher(iframeSrc);


                        if (matcher.find()) {
                            System.out.println(matcher.group());
                            imageURL = matcher.group();
                        }
                        media_type="video";
                        format="youtube";
                        System.out.println("youtube original src"+iframeSrc );

                    }
                    else if(channel.contains("Samayam-Video"))
                    {
                        desc = description.replaceAll("'", "");
                        Document doc = Jsoup.parse(responseBody);
                        Element iframe = doc.select("iframe").first();
                        String iframeSrc = iframe.attr("src");
                      /*  String pattern = "(?<=youtu.be/|watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";
                        Pattern compiledPattern = Pattern.compile(pattern);
                        Matcher matcher = compiledPattern.matcher(iframeSrc);


                        if (matcher.find()) {
                            System.out.println(matcher.group());
                            imageURL = matcher.group();
                        }*/
          /*              media_type="video";
                        format="youtube";
                        System.out.println("samayam youtube original src"+iframeSrc );
                    }

                    else if (channel.contains("Tamil Samayam")) {
                        imageURL=getSamayamUrl(responseBody);
                        desc = description.replaceAll("'", "");
                        media_type="Text/HTML";
                        format="";

                    }
                    else if(channel.contains("செய்திகள்"))
                    {
                        imageURL= getWebduniaImageURL(responseBody);
                        desc = description.replaceAll("'", "");
                        media_type="Text/HTML";
                        format="";
                    }
                    else if(channel.contains("FilmiBeat"))
                    {
                        imageURL=getFilmiBeatImgUrl(responseBody);
                        desc = description.replaceAll("'", "");
                        media_type="Text/HTML";
                        format="";
                    }
                    else if(channel.contains("India News")||channel.contains("World News")||channel.contains("City News"))
                    {
                        imageURL=getZeeNewsImgUrl(responseBody);
                        desc = description.replaceAll("'", "");
                        media_type="Text/HTML";
                        format="";

                    }

                    else if(channel.contains("Dinamalar.com"))
                    {
                        // System.out.println("response body"+responseBody);
                        desc=getDescriptionForDinamalar(responseBody);
                        byte[] bytes = desc.getBytes(StandardCharsets.ISO_8859_1);
                        desc = new String(bytes, StandardCharsets.UTF_8);
                        System.out.println("utf8 description is-->"+desc);
                        imageURL=getDinamalarImgURL(responseBody);
                        media_type="Text/HTML";
                        format="";


                    }

                    else if(channel.contains("Vikatan.com"))
                    {


                        desc=getDescriptionForVikatan(responseBody);
                        imageURL=getVikatanImgUrl(responseBody);
                        media_type="Text/HTML";
                        format="";
                    }

                    else if (channel.contains("Oneindia") || channel.contains("Horoscope in Tamil")||channel.contains("Sports News")) {
                        System.out.println("desc one india --->"+description);
                        imageURL = getOneIndiaImgURL(responseBody);
                        //imageURL = "http://bit.ly/1P6rRrd";

                        desc = description.replaceAll("'", "");
                        media_type="Text/HTML";
                        format="";

                    }
                    else if(channel.contains("BBC News தமிழ் - முகப்பு"))
                    {

                        desc = description.replaceAll("'", "");
                        imageURL=getBBCTamilImgUrl(responseBody);
                        media_type="Text/HTML";
                        format="";
                    }
                    else if(channel.contains("Sports News in Tamil"))
                    {
                        imageURL=getSamayamUrl(responseBody);
                        desc = description.replaceAll("'", "");
                        media_type="Text/HTML";
                        format="";
                    }
                }


                String tags[] = {"News"};

                if (!linkDAO.isLinkPresent(url)) {


                    Link link=new Link();
                    link.setLinkId(0);
                    link.setUserId(999);
                    link.setTitle(title);
                    link.setImageUrl(imageURL);
                    link.setCategory("News");
                    link.setVotes(0);
                    link.setNoOfComments(0);
                    link.setSpam(0);
                    link.setUrl(url);
                    link.setLanguage(lang);
                    link.setDomain(domain);
                    link.setCountry(country);
                    link.setDesc(desc);
                    link.setMedia_type(media_type);
                    link.setVideo_format(format);


                    System.out.println("title---->"+title);
                    System.out.println("description"+desc);
                    System.out.println("url"+url);
                    System.out.println("imageUrl"+imageURL);
                    System.out.println("domain--->"+domain);
                    System.out.println("media type"+media_type);
                    linkDAO.addNewLink(link);

                }
               /* else if (!linkDAO.isLinkPresent(url) && (channel.contains("The Hindu - Home" ) )){
                    linkDAO.addLink(url, title, "999", "English", "News", "India", "Text/HTML", tags, imageURL, desc);
                }
                else{
                    linkDAO.addLink(url, title, "999", "Tamil", "News", "India", "Text/HTML", tags, imageURL, desc);
                }*/
/*            }
        };

        //specify the feed we want to fetch
        System.out.println(1);
        String resource = rssFeedLink;
        //use the FeedParser network IO package to fetch our resource URL
        ResourceRequest request = ResourceRequestFactory.getResourceRequest(resource);
        System.out.println(2);
        //grab our input stream
        System.out.println(3);
        InputStream is = request.getInputStream();
        //start parsing our feed and have the above onItem methods called
        System.out.println(4);
        parser.parse(listener, is, resource);
    }

    public String getBody(String urls)
    {
        String responseBody = null;
        try
        {

            URL url = new URL(urls);
            URLConnection con = url.openConnection();
            InputStream in = con.getInputStream();
            String encoding = con.getContentEncoding();
            encoding = encoding == null ? "UTF-8" : encoding;
            String body = IOUtils.toString(in, encoding);
            System.out.println(body);
            responseBody=body;
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return responseBody;
    }

    public String getResponseBody(String url) throws IOException {
        String responseBody = null;
        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
            System.out.println("inside of res.."+url);

            HttpGet httpget = new HttpGet(url);

            // Create a custom response handler
            ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

                // @Override
                public String handleResponse(
                        final HttpResponse response) throws ClientProtocolException, IOException {
                    int status = response.getStatusLine().getStatusCode();
                    System.out.println("status"+status);
                    if (status >= 200 && status < 300) {
                        System.out.println("status2222"+status);
                        HttpEntity entity = response.getEntity();
                        return entity != null ? EntityUtils.toString(entity) : null;
                    } else {
                        throw new ClientProtocolException("Unexpected response status: " + status);
                    }
                }

            };

            responseBody = httpclient.execute(httpget, responseHandler);
        } finally {
            httpclient.close();
        }
        //System.out.println(responseB)
        return responseBody;
    }




    /*public String getPuthiyathalaimuraiURL(String responseBody) {
        //System.out.println("dinakartan image url"+responseBody);


        Document document= Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage!=null) {


            if(metaOgImage.isEmpty())
            {

                imageURL="http://www.puthiyathalaimurai.com/assets/puthiyathalaimurai-logo.webp";
            }
            else {

                imageURL = metaOgImage.attr("content");
                System.out.println(imageURL);
            }
        }
        else
        {
            imageURL="http://www.puthiyathalaimurai.com/assets/puthiyathalaimurai-logo.webp";

        }
        return imageURL;
    }*/
 /*   public String getPuthiyathalaimuraiImgURL(String responseBody) {
        System.out.println("pithiya thalaimurai image url");


        Document document= Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage!=null) {


            if(metaOgImage.isEmpty())
            {
                System.out.println("pithiya thalaimurai image url is empty");
                imageURL="http://www.puthiyathalaimurai.com/assets/puthiyathalaimurai-logo.webp";
            }
            else {

                imageURL = metaOgImage.attr("content");
                System.out.println("pithiya thalaimurai image url"+imageURL);
                System.out.println(imageURL);
            }
        }
        else
        {
            imageURL="http://www.puthiyathalaimurai.com/assets/puthiyathalaimurai-logo.webp";

        }
        return imageURL;
    }

    public String getDinakaranImgURL(String responseBody) {
        //System.out.println("dinakartan image url"+responseBody);


        Document document= Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage!=null) {


            if(metaOgImage.isEmpty())
            {

                imageURL="http://www.dinakaran.com/images/site-use/logo-dinakaran-news-paper.png";
            }
            else {

                imageURL = metaOgImage.attr("content");
                System.out.println(imageURL);
            }
        }
        else
        {
            imageURL="http://www.dinakaran.com/images/site-use/logo-dinakaran-news-paper.png";

        }
        return imageURL;
    }


    /* public String getTamilHinduImgURL(String responseBody) {

       //  System.out.println("tamil the hindu video"+responseBody);
         if (responseBody != null && responseBody.contains("hcenter")) {
             responseBody = responseBody.substring(responseBody.indexOf("hcenter"));
             responseBody = responseBody.substring(responseBody.indexOf("src=") + 5, responseBody.indexOf("class") - 2);
             imageURL = responseBody;
         } else if (responseBody != null && responseBody.contains("og:image")) {
             responseBody = responseBody.substring(responseBody.indexOf("og:image\""));
             responseBody = responseBody.substring(responseBody.indexOf("content=\"") + 9, responseBody.indexOf("/>") - 2);
             //  System.out.println("IMAGE URL OF HINDU ==========>" + responseBody);
             imageURL = responseBody;
         } else {

             imageURL = "http://i.imgur.com/jvGPMA2.png";
         }
         return imageURL;
     }*/
 /*   public String getTamilHinduImgURL(String responseBody) {

        // System.out.println("tamil the hindu video"+responseBody);
        Document document= Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage!=null) {
            imageURL= metaOgImage.attr("content");
        }
        else
        {
            imageURL="http://i.imgur.com/jvGPMA2.png";
        }

        return imageURL;
    }

    public String getDinamalarImgURL(String responseBody) {


        Document document= Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage!=null) {
            imageURL= metaOgImage.attr("content");
        }
        else
        {
            imageURL="http://www.creativemedia.co.in/images/2018/01/14/dinamalar-logo.jpg";
        }
        return imageURL;
    }

    public String getOneIndiaImgURL(String responseBody) {

        Document document= Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage!=null) {
            System.out.println("meta image---->"+metaOgImage);

            imageURL= metaOgImage.attr("content");
        }
        else
        {
            imageURL="https://images.oneindia.com/images/rss/oneindia-tamil-logo.png";
        }
        return imageURL;
    }

    public String getSamayamUrl(String responseBody) {

        Document document= Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage!=null) {
            imageURL= metaOgImage.attr("content");
        }
        else
        {
            imageURL="https://tamil.samayam.com/photo/48055721/logo.jpg";
        }



        return imageURL;
    }

    public String getFilmiBeatImgUrl(String responseBody) {

        Document document= Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage!=null) {
            imageURL= metaOgImage.attr("content");
        }
        else
        {
            imageURL="https://tamil.samayam.com/photo/48055721/logo.jpg";
        }



        return imageURL;
    }


    public String getWebduniaImageURL(String responseBody) {

        Document document= Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage!=null) {
            imageURL= metaOgImage.attr("content");
        }
        else
        {
            imageURL="https://i.ytimg.com/vi/t-mNF53_a2I/0.jpg";
        }



        return imageURL;
    }
    public String getVikatanImgUrl(String responseBody) {



        Document document= Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage!=null) {
            imageURL= metaOgImage.attr("content");
        }
        else
        {
            imageURL="https://content3.jdmagicbox.com/comp/chennai/03/044pf003003/catalogue/vikatan-media-services-pvt-ltd-mount-road-chennai-magazines-3bdrggn.jpg";
        }



        return imageURL;
    }


    public String getBBCTamilImgUrl(String responseBody) {


        Document document= Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage!=null) {
            imageURL= metaOgImage.attr("content");
        }
        else
        {
            imageURL=" https://ichef.bbci.co.uk/news/ws/640/amz/worldservice/live/assets/images/2016/11/15/161115113204_bbc_tamil_logo_640x360_bbc_nocredit.jpg";
        }



        return imageURL;
    }
    public String getDescriptionForVikatan(String responseBody) {


        Document document= Jsoup.parse(responseBody);

        Elements shortDesc = document.select("meta[property=og:description]");
        Elements shortDesc2 = document.select("meta[name=Description]");
        if(shortDesc2!=null)

        {
            desc=shortDesc2.attr("content");

        }


        return desc;
    }
    public String getDescriptionForDinamalar(String responseBody) {


        Document document= Jsoup.parse(responseBody);

        Elements shortDesc = document.select("meta[property=og:description]");
        if(shortDesc!=null)
        {
            desc=shortDesc.attr("content");
        }


        return desc;
    }
    public String getMalaimalarImgURL(String responseBody) {

        Document document= Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage!=null) {
            imageURL= metaOgImage.attr("content");
        }
        else
        {
            imageURL="https://stat.maalaimalar.com/MMNews/Content/images/MM-NewsLogo.png";
        }



        return imageURL;
    }
    public String getZeeNewsImgUrl(String responseBody) {


        Document document= Jsoup.parse(responseBody);
        Elements metaOgImage = document.select("meta[property=og:image]");
        if (metaOgImage!=null) {
            imageURL= metaOgImage.attr("content");
        }
        else
        {
            imageURL="https://pbs.twimg.com/profile_images/944366370782711809/wQtAVakc_400x400.jpg";
        }



        return imageURL;
    }

    public void clearCache()
    {



        InputStream input = null;
        Properties prop = new Properties();


        try {

            input = new FileInputStream("/var/lib/tomcat7/webapps/ROOT/WEB-INF/db.properties");

            // input = new FileInputStream("/Users/Sathya/Documents/_PROJCTS/SUDASUDA/sudasudascrapper/src/main/webapp/WEB-INF/db.properties" );

            prop.load(input);
            System.out.println("servername"+prop.getProperty("server"));


            String listOfServer=prop.getProperty("server");
            String[] server=listOfServer.split(",");//Separating the word using delimiter Comma and stored in an array
            for(int k=0;k<server.length;k++)
            {

                System.out.println(server[k]);

                String inputs=server[k];

                CloseableHttpClient client = HttpClients.createDefault();
                URIBuilder builder = new URIBuilder(inputs);

                HttpGet get = new HttpGet(builder.build());

                CloseableHttpResponse response = client.execute(get);

                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        response.getEntity().getContent()));

                String inputLine;
                StringBuffer HttpResponse = new StringBuffer();


                System.out.println("Clearing Cache.........");
                reader.close();
            }

        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
        catch (URISyntaxException ex)
        {
            ex.printStackTrace();
        }

    }



}





 */