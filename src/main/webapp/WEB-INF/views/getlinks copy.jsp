<%@page import="org.springframework.web.servlet.i18n.SessionLocaleResolver"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.List,com.sudasuda.app.domain.Link,com.sudasuda.app.domain.User,com.sudasuda.app.vo.NameCountVO" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="ChudaChuda.com">
	<meta name="keywords" content="ChudaChuda.com">
	<title>ChudaChuda.com</title>
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/style.css">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/jquery.mCustomScrollbar.css">
</head>	
<body>
<%String tab = (String) request.getAttribute("tab");
if (tab==null) tab="current";
if ( request.getSession().getAttribute("userInfo") != null ) { User user = (User) session.getAttribute("userInfo"); }%>
<script>
	window.fbAsyncInit = function() {
		FB.init({
			appId      : '909964385712734',
			xfbml      : true,
			version    : 'v2.3'
		});
	};

	(function(d, s, id){
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) {return;}
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>
	<div class="home-container">
		<div class="home-sidebar">
			<div class="logo"></div>
			<div class="left-arrow side-menu"></div>
			<div class="s-cat-box">
				<div class="s-topics">
					<a href="<%=request.getContextPath()%>/GetLinks?tab=new">
						<div class='s-topic <%=(tab.equalsIgnoreCase("new")?"s-topic-active":"") %>'>
							<div class="s-topic-icon">	
								<span class="new-icon"></span>
							</div>
							<h5>NEW</h5>
						</div>
					</a>
					<a href="<%=request.getContextPath()%>/GetLinks?tab=current">
						<div class='s-topic <%=(tab.equalsIgnoreCase("current")?"s-topic-active":"") %>'>
							<div class="s-topic-icon">
								<span class="trending-icon"></span>
							</div>
							<h5>TRENDING</h5>
						</div>
					</a>
					<a href="<%=request.getContextPath()%>/GetLinks?tab=expired">
						<div class='s-topic <%=(tab.equalsIgnoreCase("expired")?"s-topic-active":"") %>'>
							<div class="s-topic-icon">
								<span class="expired-icon"></span>
							</div>
							<h5>EXPIRED</h5>
						</div>
					</a>
					<a href="<%=request.getContextPath()%>/GetLinks?tab=mylinks">
						<div class='s-topic <%=(tab.equalsIgnoreCase("mylinks")?"s-topic-active":"") %>'>
							<div class="s-topic-icon">
								<span class="mylinks-icon"></span>
							</div>
							<h5>Likes</h5>
						</div>
					</a>
				</div>

				<div class="s-cat">
					<div class="s-cat-heading">
						<span class="cat-icon"></span>
						<h5>CATEGORIES</h5><a href="#"><span class="down-icon"></span></a>
					</div>
					<% String cat = (String)session.getAttribute("category");
					   cat = (cat==null?"All":cat); %>
					<div class="cat-items hidden">
						<ul class="cat-list">
							<a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=All"><li <%=(cat.equalsIgnoreCase("all")?"class='cat-list-active'":"")%>>All</li></a>
							<a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=Health"><li <%=(cat.equalsIgnoreCase("health")?"class='cat-list-active'":"")%>>HEALTH</li></a>
							<a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=Society"><li <%=(cat.equalsIgnoreCase("society")?"class='cat-list-active'":"")%>>SOCIETY</li></a>
							<a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=World"><li <%=(cat.equalsIgnoreCase("world")?"class='cat-list-active'":"")%>>WORLD</li></a>
							<a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=Politics"><li <%=(cat.equalsIgnoreCase("politics")?"class='cat-list-active'":"")%>>POLITICS</li></a>
							<a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=Sports"><li <%=(cat.equalsIgnoreCase("sports")?"class='cat-list-active'":"")%>>SPORTS</li></a>
							<a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=Spirituality"><li <%=(cat.equalsIgnoreCase("spirituality")?"class='cat-list-active'":"")%>>SPIRITUALITY</li></a>
							<a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&category=Technology"><li <%=(cat.equalsIgnoreCase("technology")?"class='cat-list-active'":"")%>>TECHNOLOGY</li></a>
						</ul>
					</div>
				</div>
				<div class="s-cat">
					<div class="s-cat-heading">
						<span class="country-icon"></span>
						<h5>COUNTRY</h5><a href="#"><span class="down-icon"></span></a>
					</div>
					<% String country=(String) session.getAttribute("country");
						if ( country == null ) country="all";%>
					<div class="cat-items hidden">
						<ul class="cat-list">
							<a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=All'><li <%=(country.equalsIgnoreCase("All")?"class='cat-list-active'":"")%>>ALL</li></a>
							<a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=India'><li <%=(country.equalsIgnoreCase("India")?"class='cat-list-active'":"")%>>INDIA</li></a>
							<a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=United States'><li <%=(country.equalsIgnoreCase("United States")?"class='cat-list-active'":"")%>>USA</li></a>
							<a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=Australia'><li <%=(country.equalsIgnoreCase("Australia")?"class='cat-list-active'":"")%>>AUSTRALIA</li></a>
							<a href='<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&country=United Kigdom'><li <%=(country.equalsIgnoreCase("United Kingdom")?"class='cat-list-active'":"")%>>UNITED KINGDOM</li></a>
						</ul>
					</div>
				</div>
				<div class="s-cat">
					<div class="s-cat-heading">
						<span class="lang-icon"></span>
						<h5>LANGUAGE</h5><a href="#"><span class="down-icon"></span></a>
					</div>
					<% String language=(String) session.getAttribute("language");
					   if ( language == null ) language="All";%>

					<div class="cat-items hidden">
						<ul class="cat-list">
							<a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&lang=All&ln=en"><li <%=(language.equalsIgnoreCase("All")?"class='cat-list-active'":"")%>>ALL</li></a>
							<a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&lang=English&ln=en"><li <%=(language.equalsIgnoreCase("English")?"class='cat-list-active'":"")%>>ENGLISH</li></a>
							<a href="<%=request.getContextPath()%>/GetLinks?tab=<%=request.getAttribute("tab")%>&lang=Tamil&ln=ta"><li <%=(language.equalsIgnoreCase("Tamil")?"class='cat-list-active'":"")%>>தமிழ்</li></a>
							<%--<a href="#"><li>HINDI</li></a>--%>
							<%--<a href="#"><li>TELUGU</li></a>--%>
							<%--<a href="#"><li>MALAYALAM</li></a>--%>
						</ul>
					</div>
				</div>
			</div>	
		</div>
		<div class="home-main" id="page-content-wrapper">
			<div class="mobile-header">
				<a href="#"><div class="mobile-menu side-menu"></div></a>
				<a href="#">
					<div class="mobile-logo"></div>
				</a>
				<div class="mobile-user">
					<div class="m-user-pic">	
						<img src="<%=request.getContextPath()%>/resources/images/user-avatar.png" alt="User Picture">
					</div>
					<a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<span class="m-user-name">Hi Hari</span>
						<span class="down-icon-grey"></span>
					</a>000000000
					<ul class="dropdown-menu m-dropdown-menu" role="menu" aria-labelledby="dLabel">
						<a href="#"><li>Edit Profile</li></a>
						<a href="#"><li>Change Password</li></a>
						<a href="#"><li>Logout</li></a>
					</ul>
				</div>
				<div class="mobile-login hide">
					<p><a href="#" id="user-signin"  data-toggle="modal" data-target="#myModal">SignIn</a></p>
					<p><a href="#" id="user-signup"  data-toggle="modal" data-target="#myModal">SignUp</a></p>
				</div>
			</div>
			<div class="home-header">
				<div class="input-group home-search">
					<input type="text" class="form-control home-search-control" placeholder="Enter your Keyword to search">
					<span class="input-group-btn">
						<button class="btn home-search-btn" type="button"></button>
					</span>
				</div>

				<% if ( session.getAttribute("userInfo") == null ) {%>
				<div class="home-login">
					<p><a href="#" id="user-signin"  data-toggle="modal" data-target="#myModal">LOGIN</a></p>
					<p><a href="#" id="user-signup"  data-toggle="modal" data-target="#myModal">SIGNUP</a></p>
				</div>
				<%}%>

				<% if (session.getAttribute("userInfo") != null) { %>

				<div class="home-user dropdown">
					<div class="h-user-pic">	
						<img src="<%=request.getContextPath()%>/resources/images/user-avatar.png" alt="User Picture">
					</div>
					<a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<span class="h-user-name"><%= ((User) session.getAttribute("userInfo")).getUserName()%></span>
						<span class="down-icon-grey"></span>
					</a>	
					<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
						<%-- <a href="#"><li>Edit Profile</li></a>
						<a href="#"><li>Change Password</li></a>
						--%>
						<a href="<%=request.getContextPath()%>/SignOut"><li>Logout</li></a>
					</ul>
				</div>
				<%}%>
				<div class="submit-post">
					<a href="<%=request.getContextPath()%>/AddLink"><span class="submit-text">Submit an item</span></a>
				</div>
			<div class="clearfix"></div>
			</div>
			<div class="home-content">
				
			<!-- Modal -->
				<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog signup-dialog">
						<div class="modal-content signup-content">
							<div class="modal-body signup-body">
								
								<div class="submit-content pad0">
									<div class="submit-item">
										<button type="button" class="close custom-close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<div class="sign-tab">
											<ul>
												<li class="active" id="signin">SIGNIN</li>
												<li id="signup">SIGNUP</li>
											</ul>
										</div>
										<div class="sign-body">
											<div class="login-left">
												<form id="signin-form" method="POST" action="<%=request.getContextPath()%>/SignIn?action=login">
													<div class="input-group">
														<span class="input-group-addon">
															<img src="<%=request.getContextPath()%>/resources/images/fn-icon.png" alt="M">
														</span>
														<input type="username" name="username" id="username" class="form-control" placeholder="Username">
													</div>
													<div class="input-group">
														<span class="input-group-addon">
															<img src="<%=request.getContextPath()%>/resources/images/lock-icon.png" alt="M">
														</span>
														<input type="password" name="password" id="password" class="form-control" placeholder="Password">
													</div>
													<div class="pwd-options">
														<div class="input-group remember-pwd">
															<label>
																<input type="checkbox" name="remember"> Remember Password
															</label>
														</div>
														<div class="input-group fgt-pwd">
															<a href="#"><span>Forgot Password?</span></a>
														</div>
													</div>
													<div class="clearfix"></div>
													<div class="signupBtn">
														<button class="btn btn-signup">SIGN IN</button>
													</div>
												</form>
												<form id="signup-form" class="hide" action="<%=request.getContextPath()%>/SignIn?action=signup"
													  method="post">
													<div class="input-group">
														<span class="input-group-addon">
															<img src="<%=request.getContextPath()%>/resources/images/mail-icon.png" alt="M">
														</span>
														<input type="email" class="form-control" name="email" placeholder="Email Id">
													</div>
													<div class="input-group">
														<span class="input-group-addon">
															<img src="<%=request.getContextPath()%>/resources/images/fn-icon.png" alt="N">
														</span>
														<input type="text" class="form-control" name="username" placeholder="User Name">
													</div>
													<div class="input-group">
														<span class="input-group-addon">
															<img src="<%=request.getContextPath()%>/resources/images/lock-icon.png" alt="M">
														</span>
														<input type="password" class="form-control" name="password" placeholder="Password">
													</div>

													<div class="signupBtn">
														<button class="btn btn-signup">SIGN UP</button>
													</div>
												</form>
											</div>
											<div class="login-center hr-signin">
												<hr>
												<span>OR</span>
											</div>
											<div class="login-right">
												<a href="">
													<img src="<%=request.getContextPath()%>/resources/images/fb-btn.png" alt="facebook">
												</a>
												<a href="">
													<img src="<%=request.getContextPath()%>/resources/images/gp-btn.png" alt="Google+">
												</a>
											</div>
										</div>
									</div>
									<!-- <div class="submit-item-shadow"></div> -->
								</div>
							</div>
						</div>
					</div>
				</div>	
				
				<div class="home-articles">
				
				<% 
	List <Link> links = (List<Link>) request.getAttribute("links"); 
    if ( links == null || links.size() == 0) { %>
				No articles found
				
				<%} 
    
    		for (int i = 0; links != null && i < links.size(); i++) {
    			
    			Link link = links.get(i);
    %>
					<div class="article-box">
						<div class="articles-img">
							<img src="<%=link.getImageUrl() %>" alt="Image">
						</div>
						<div class="article-box-content">
							<a href="<%=link.getUrl()%>"><h5><%=link.getTitle()%></h5></a>
							<h6>By <%=link.getSubmitedBy() %> <%=( link.getHoursElapsed()<=24?link.getHoursElapsed()+" hours ago ":link.getHoursElapsed()/24+" days ago ") %></h6>
							<a href="#"><span>#NarendraModi</span></a>
							<a href="#"><span>#MudraBank</span></a>
							<div class="article-shadow"></div>
							<div class="article-action">
								<div class="article-like">
									<%-- <span class='<%=link.isVoted()?"like-filled-icon":"like-icon"%>'></span> --%>
									<% if ( tab != null && !tab.equalsIgnoreCase("mylinks")) {%>
									<% if ( !link.isVoted() ) {%>
									<a href="<%=request.getContextPath()%>/VoteUp?linkId=<%=link.getLinkId()%>&tab=<%=request.getAttribute("tab")%>"><span class='<%=link.isVoted()?"like-filled-icon":"like-icon"%>'></span><h6>LIKE(<%=link.getVotes()%>)</h6></a>
									<%} else { %>
										<span class='<%=link.isVoted()?"like-filled-icon":"like-icon"%>'></span><h6>LIKE(<%=link.getVotes()%>)</h6>
									<%} }%>

								</div>
								<div class="article-comment">
									<span class="comment-icon"></span>
									<a href="#"><h6>COMMENT</h6></a>
								</div>
								<%-- <div class="article-later">
									<span class="later-icon"></span>
									<a href="#"><h6>READ LATER</h6></a>
								</div> --%>
							</div>
						</div>
						<div class="box-shadow"></div>
					</div>	
					<%} %>

				</div>
					</div>
				</div>
			</div>
			<script src='<%=request.getContextPath()%>/resources/js/jquery-1.11.2.min.js'></script>
			<script src="<%=request.getContextPath()%>/resources/js/bootstrap.min.js"></script>
			<script src="<%=request.getContextPath()%>/resources/js/script.js"></script>
			<script src="<%=request.getContextPath()%>/resources/js/jquery.mCustomScrollbar.concat.min.js"></script>
		</body>
		</html>